package com.taxiyefood.driver.databean;

public class Country {
    private int id;
    private String countryName;
    private String countryCode;

    public Country(int id, String countryName, String countryCode) {
        this.id = id;
        this.countryName = countryName;
        this.countryCode = countryCode;
    }

    public int getId() {
        return id;
    }

    public String getCountryName() {
        return countryName;
    }

    public String getCountryCode() {
        return countryCode;
    }
}
