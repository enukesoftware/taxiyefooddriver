package com.taxiyefood.driver.databean;


/**
 * Created by enuke on 18/5/16.
 */
public class AcceptedOrderBean {

    String pickUpRestName;

    String pickUpAddress;

    String deliveredTo;

    String deliveryAddress;

    String notificationDateTime;

    String orderId;

    String status;

    String deliveredAmount;

    String pickUpLat;

    String pickUpLong;

    String deliveryLat;

    String deliveryLong;

    public AcceptedOrderBean(String pickUpRestName, String pickUpAddress, String deliveredTo, String deliveryAddress, String notificationDateTime, String orderId, String status, String deliveredAmount, String pickUpLat, String pickUpLong, String deliveryLat, String deliveryLong) {
        this.pickUpRestName = pickUpRestName;
        this.pickUpAddress = pickUpAddress;
        this.deliveredTo = deliveredTo;
        this.deliveryAddress = deliveryAddress;
        this.notificationDateTime = notificationDateTime;
        this.orderId = orderId;
        this.status = status;
        this.deliveredAmount = deliveredAmount;
        this.pickUpLat = pickUpLat;
        this.pickUpLong = pickUpLong;
        this.deliveryLat = deliveryLat;
        this.deliveryLong = deliveryLong;
    }

    public String getPickUpRestName() {
        return pickUpRestName;
    }

    public void setPickUpRestName(String pickUpRestName) {
        this.pickUpRestName = pickUpRestName;
    }

    public String getPickUpAddress() {
        return pickUpAddress;
    }

    public void setPickUpAddress(String pickUpAddress) {
        this.pickUpAddress = pickUpAddress;
    }

    public String getDeliveredTo() {
        return deliveredTo;
    }

    public void setDeliveredTo(String deliveredTo) {
        this.deliveredTo = deliveredTo;
    }

    public String getDeliveryAddress() {
        return deliveryAddress;
    }

    public void setDeliveryAddress(String deliveryAddress) {
        this.deliveryAddress = deliveryAddress;
    }

    public String getNotificationDateTime() {
        return notificationDateTime;
    }

    public void setNotificationDateTime(String notificationDateTime) {
        this.notificationDateTime = notificationDateTime;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDeliveredAmount() {
        return deliveredAmount;
    }

    public void setDeliveredAmount(String deliveredAmount) {
        this.deliveredAmount = deliveredAmount;
    }

    public String getPickUpLat() {
        return pickUpLat;
    }

    public void setPickUpLat(String pickUpLat) {
        this.pickUpLat = pickUpLat;
    }

    public String getPickUpLong() {
        return pickUpLong;
    }

    public void setPickUpLong(String pickUpLong) {
        this.pickUpLong = pickUpLong;
    }

    public String getDeliveryLat() {
        return deliveryLat;
    }

    public void setDeliveryLat(String deliveryLat) {
        this.deliveryLat = deliveryLat;
    }

    public String getDeliveryLong() {
        return deliveryLong;
    }

    public void setDeliveryLong(String deliveryLong) {
        this.deliveryLong = deliveryLong;
    }
}
