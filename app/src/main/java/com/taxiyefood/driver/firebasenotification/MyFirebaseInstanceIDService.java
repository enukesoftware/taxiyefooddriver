package com.taxiyefood.driver.firebasenotification;


import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

/**
 * Created by amrit on 06/4/17.
 */
public class MyFirebaseInstanceIDService extends FirebaseInstanceIdService {
    private static final String TAG = MyFirebaseInstanceIDService.class.getSimpleName();

    @Override
    public void onTokenRefresh() {
        super.onTokenRefresh();
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();

        // Saving reg id to shared preferences
        storeRegIdInPref(refreshedToken);

        // sending reg id to your server
        sendRegistrationToServer(refreshedToken);

        // Notify UI that registration has completed, so the progress indicator can be hidden.
       // Intent registrationComplete = new Intent(Config.REGISTRATION_COMPLETE);
       // registrationComplete.putExtra("token", refreshedToken);
       // LocalBroadcastManager.getInstance(this).sendBroadcast(registrationComplete);
    }

    private void sendRegistrationToServer(final String token) {
        // sending gcm token to server
        Log.e(TAG, "sendRegistrationToServer: " + token);
        /*SessionManger session = new SessionManger(this);
        FirebaseTokenUpdateRequest request = new FirebaseTokenUpdateRequest();
        request.setDeviceToken(token);
        request.setUserId(session.getUserId());
        if(GeneralFunctions.isNetworkAvaliable(this)) {*/
        if (FirebaseInstanceId.getInstance().getToken() != null) {
            UpdateFirebaseToken.update(getApplicationContext(), token);
        }
        //}


    }

    private void storeRegIdInPref(String token) {
        /*SessionManger sessionManger = new SessionManger(this);
        sessionManger.setFirebaseToken(token);

        //token updated only if it is send to server
        sessionManger.setFirebaseTokenUpdated(false);*/
    }
}
