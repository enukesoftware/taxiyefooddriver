package com.taxiyefood.driver.firebasenotification;

import android.content.Context;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.taxiyefood.driver.data.AppController;
import com.taxiyefood.driver.serverrequest.UpdatefbseNotificationRequest;
import com.taxiyefood.driver.serverrequest.UpdatenotiTokenResponse;
import com.taxiyefood.driver.utils.Constant;
import com.taxiyefood.driver.utils.GsonRequest;
import com.taxiyefood.driver.utils.Utility;
import com.google.gson.Gson;

import com.taxiyefood.driver.utils.Preference;

/**
 * Created by enuke on 8/22/17.
 */

public class UpdateFirebaseToken {

    public static void update(Context context, String token) {
        if (Preference.getUserIdCommon() != null) {
            if (!Preference.getUserIdCommon() .equals("") && token != null) {
                try {
                    UpdatefbseNotificationRequest tokenrequest = new UpdatefbseNotificationRequest();
                    tokenrequest.setDeviceToken(token);
                    tokenrequest.setUserId(Preference.getUserIdCommon());
                    final Gson gsonlocal = new Gson();
                    final String request = gsonlocal.toJson(tokenrequest);
                    Log.i("devicetoken", "update: "+token);
                    sendDeviceToken(context, request, Constant.UPDATETOKEN_METHOD);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

        public static void sendDeviceToken(final Context context, final String request, final String urlMethod) {
            GsonRequest gsonRequest = new GsonRequest<>(Request.Method.POST, urlMethod,
                    UpdatenotiTokenResponse.class, request, new Response.Listener<UpdatenotiTokenResponse>() {
                @Override
                public void onResponse(UpdatenotiTokenResponse tokenupdateResponseData) {
                    String result = "" + tokenupdateResponseData.getSuccess();
                    Log.i("result", "update: "+result);
                    if (result.equalsIgnoreCase("true")) {
                        Preference.putBooleanNotifaicton(Constant.PREFERENCES_UPDATE_NOTIFICATION_STATUS,true);

                    } else {
                    }
                    AppController.getInstance().getRequestQueue().getCache().clear();
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError volleyError) {
                    if (volleyError != null) {
                        if (volleyError.getMessage() == null)
                            volleyError.printStackTrace();

                        else
                            volleyError.printStackTrace();
                    }
                    Utility.onVolleyErrorResponse(context, volleyError);

                }
            });
            AppController.getInstance().addToRequestQueue(gsonRequest);
        }

}


