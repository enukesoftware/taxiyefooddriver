package com.taxiyefood.driver.firebasenotification;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.text.SimpleDateFormat;


/**
 * Created by amrit on 06/4/17.
 */
public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = MyFirebaseMessagingService.class.getSimpleName();
    int[] appWidgetIds;
    String title = "", message = "",type,notificationID;
    Intent resultIntent;

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        if (remoteMessage == null)
            return;
        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {
            Log.e(TAG, "DATA_BODYData: " + remoteMessage.getData().toString());
            handleRemoteData(remoteMessage);
        }
    }

    @Override
    public void onNewToken(String s) {
        super.onNewToken(s);
        try{
            UpdateFirebaseToken.update(getApplicationContext(), s);
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    /**
     * Showing notification with text only
     */
    private void showNotificationMessage(Context context, String title, String message, String timeStamp, String  type,String notificationID) {
        //String timestamp = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss a").format(System.currentTimeMillis());
        NotificationUtils notificationUtils = new NotificationUtils(context);
        notificationUtils.playNotificationSound();
        notificationUtils.sendNotification(getApplicationContext(), title, message, timeStamp, type, notificationID);
    }

    private void handleRemoteData(RemoteMessage remoteMessage) {
        String timestamp = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss a").format(System.currentTimeMillis());
        resultIntent=new Intent();
        try {
           /* Log.e("notification_1", "Notification Message Body: " + remoteMessage.getData().get("message"));
            Log.e("notification_2", "Notification Message Tag: " + remoteMessage.getData().get("subtitle"));
            Log.e("notification_3", "Notification Message Title: " + remoteMessage.getData().get("title"));
            Log.e("notification_3", "Notification Message Title: " + remoteMessage.getData().get("type"));
            Log.e("notification_3", "Notification Message Title: " + remoteMessage.getData().get("notificationID"));
            Log.e("notificationremote", "Notification Message Title: " );*/
            title=remoteMessage.getData().get("title");
            message=remoteMessage.getData().get("body");
            type=remoteMessage.getData().get("type");
            notificationID=remoteMessage.getData().get("notificationID");
            showNotificationMessage(getApplicationContext(), title, message, timestamp, type,notificationID);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}