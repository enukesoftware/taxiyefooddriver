package com.taxiyefood.driver.adapter;


import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Vibrator;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.taxiyefood.driver.R;
import com.taxiyefood.driver.data.AppController;
import com.taxiyefood.driver.fragment.AcceptCancelFragment;
import com.taxiyefood.driver.serverrequest.AcceptedOrderRequest;
import com.taxiyefood.driver.serverrequest.AcceptedOrderResponse;
import com.taxiyefood.driver.serverrequest.LocationResponseData;
import com.taxiyefood.driver.utils.Constant;
import com.taxiyefood.driver.utils.GsonRequest;
import com.taxiyefood.driver.utils.MyCustomProgressDialog;
import com.taxiyefood.driver.utils.Preference;
import com.taxiyefood.driver.utils.Utility;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by enuke on 18/5/16.
 */
public class AcceptedOrderAdapter extends RecyclerView.Adapter<AcceptedOrderAdapter.ViewHolder> {

    private Context context;
    private ArrayList<LocationResponseData> locationResponseDatas;
    private List<ViewHolder> lstHolders;
    private Handler mHandler = new Handler();
    private ProgressDialog pDialog;

    public AcceptedOrderAdapter(Context context, ArrayList<LocationResponseData> locationResponseDatas) {
        this.context = context;
        this.locationResponseDatas = locationResponseDatas;
        lstHolders = new ArrayList<>();
        //  startUpdateTimer();
    }


    /* private Runnable updateRemainingTimeRunnable = new Runnable() {
         @Override
         public void run() {
             synchronized (lstHolders) {
                 long currentTime = System.currentTimeMillis();
                 for (ViewHolder holder : lstHolders) {
                     holder.updateTimeRemaining(currentTime);
                 }
             }
         }
     };

     private void startUpdateTimer() {
         Timer tmr = new Timer();
         tmr.schedule(new TimerTask() {
             @Override
             public void run() {
                 mHandler.post(updateRemainingTimeRunnable);
             }
         }, 1000, 1000);
     }
 */
    @Override
    public AcceptedOrderAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                              int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.accepted_order_item, parent, false);
        ViewHolder vhFS = new ViewHolder(v);
        return vhFS;

    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {

        final LocationResponseData locationResponseData = locationResponseDatas.get(position);

        //long duration = Long.parseLong(locationResponseData.getDeliveryLong());

        /*holder.restName.setText(acceptedOrderBean.getPickUpRestName());
        holder.deliveryAddr.setText(acceptedOrderBean.getPickUpAddress());
        holder.deliveryPersonName.setText(acceptedOrderBean.getDeliveredTo());
        holder.receiverAddr.setText(acceptedOrderBean.getDeliveryAddress());*/

        synchronized (lstHolders) {
            lstHolders.add(holder);
        }

        holder.setData(locationResponseData, position);

        holder.acceptingOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //   replaceFragment(locationResponseData);
                makeAcceptRequest(locationResponseData);
            }
        });

    }

    private void makeAcceptRequest(LocationResponseData locationResponseData) {
        AcceptedOrderRequest acceptedOrderRequest = new AcceptedOrderRequest(Preference.getUserIdCommon(), locationResponseData.getId(), "3", "");
        Gson gson = new GsonBuilder().create();
        String acceptJson = gson.toJson(acceptedOrderRequest);
        makeServerRequest(acceptJson, locationResponseData);

    }


    private void makeServerRequest(String loginJson, final LocationResponseData locationResponseData) {
        pDialog = new MyCustomProgressDialog(context, R.style.MyTheme);
        pDialog.setMessage("Please wait...");

        pDialog.setCancelable(false);
        pDialog.show();
        String url = Constant.ACCEPT_ORDER_METHOD;
        Utility.showingLog("url", "" + url + "request" + loginJson);
        GsonRequest gsonRequest = new GsonRequest<AcceptedOrderResponse>(Request.Method.POST, url,
                AcceptedOrderResponse.class, loginJson, new Response.Listener<AcceptedOrderResponse>() {

            @Override
            public void onResponse(AcceptedOrderResponse acceptedOrderResponse) {
                boolean status = acceptedOrderResponse.isSuccess();

                if (status) {
                    pDialog.dismiss();
                    Preference.setPickDeliveryStatus(locationResponseData.getId(), 3);

                    Gson gson = new GsonBuilder().create();
                    Preference.setAcceptedOrderStatusValue(gson.toJson(locationResponseData));
                    Utility.showingLog("location json", "" + gson.toJson(locationResponseData));
                    replaceFragment(locationResponseData);
                } else {
                    pDialog.dismiss();
                }

            }
        },

                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        pDialog.dismiss();
                        if (volleyError != null) {
                            if (volleyError instanceof NetworkError)
                                Utility.showToast(context, context.getResources().getString(R.string.slow_net));
                            else if (volleyError.getMessage() == null)
                                Utility.showToast(context, context.getResources().getString(R.string.network_error));
                            else
                                Utility.showToast(context, volleyError.getMessage());


                        }
                    }
                });
        DefaultRetryPolicy policy = new DefaultRetryPolicy(2 * 60 * 1000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        gsonRequest.setRetryPolicy(policy);
        AppController.getInstance().addToRequestQueue(gsonRequest);
    }

    private void replaceFragment(LocationResponseData locationResponseData) {

        AcceptCancelFragment acceptCancelFragment = new AcceptCancelFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable("order_id", locationResponseData);
        acceptCancelFragment.setArguments(bundle);

        ((FragmentActivity) context).getSupportFragmentManager().beginTransaction().replace(R.id.controllerFragment, acceptCancelFragment).addToBackStack(null).commit();
    }


    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return locationResponseDatas.size();
    }


    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public class ViewHolder extends RecyclerView.ViewHolder {

        // each data item is just a string in this case

        TextView restName, deliveryAddr, deliveryPersonName, receiverAddr, timerValue, orderId;
        LocationResponseData locationResponseData;
        Button acceptingOrder;


        public ViewHolder(View itemView) {
            super(itemView);

            restName = itemView.findViewById(R.id.pickUpRestName);
            deliveryAddr = itemView.findViewById(R.id.pickUpDeliveryAdd);
            deliveryPersonName = itemView.findViewById(R.id.deliverPersonName);
            receiverAddr = itemView.findViewById(R.id.reciverAddr);
            timerValue = itemView.findViewById(R.id.timerValue);
            acceptingOrder = itemView.findViewById(R.id.acceptingOrder);
            orderId = itemView.findViewById(R.id.orderId);


        }

        public void setData(LocationResponseData item, int position) {
            Vibrator v = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);
            // Vibrate for 500 milliseconds
            v.vibrate(500);
            locationResponseData = item;
            String floor = "";
            String houseNo = "";
            String street = "";
            String areaName = "";
            String city = "";
            restName.setText(item.getRestaurant_name());
            deliveryAddr.setText(item.getRestaurant_floor() + "," + item.getRestaurant_street() + "," + item.getRestaurant_area() + " " + item.getRestaurant_city());
            deliveryPersonName.setText(item.getFirst_name() + " " + (item.getLast_name() == null ? "" : item.getLast_name()));
            if (item.getShipJson() != null) {
                if (item.getShipJson().getAddressJson() != null) {
                    floor = (item.getShipJson().getAddressJson().getFloorUnit() == null ? "" : item.getShipJson().getAddressJson().getFloorUnit() + ", ");
                    houseNo = (item.getShipJson().getAddressJson().getHouseNo() == null ? "" : item.getShipJson().getAddressJson().getHouseNo() + ", ");
                    street = (item.getShipJson().getAddressJson().getStreet() == null ? "" : item.getShipJson().getAddressJson().getStreet() + ", ");
                    areaName = (item.getShipJson().getAddressJson().getAreaName() == null ? "" : item.getShipJson().getAddressJson().getAreaName() + ", ");
                    city = (item.getShipJson().getAddressJson().getCity() == null ? "" : item.getShipJson().getAddressJson().getCity());

                }
            }
            receiverAddr.setText(floor + houseNo + street + areaName + city);
            orderId.setText(item.getOrder_number());
            updateTimeRemaining(System.currentTimeMillis(), position, item);
        }

        public void updateTimeRemaining(long currentTime, final int position, final LocationResponseData item) {
     /*       long timeDiff = System.currentTimeMillis()+1;//Long.parseLong(acceptedOrderBeans.getDeliveryLong()) - currentTime;
            Utility.showingLog("time diff",""+timeDiff);
            if (timeDiff > 0) {
                int seconds = (int) (timeDiff / 1000) % 60;
                int minutes = (int) ((timeDiff / (1000 * 60)) % 60);
                int hours = (int) ((timeDiff / (1000 * 60 * 60)) % 24);
                timerValue.setText(hours + ":" + minutes + ":" + seconds);
                Utility.showingLog("time value",""+hours + " hrs " + minutes + " mins " + seconds + " sec");
            } else {
                timerValue.setText("Expired!!");
            }*/


            /*new CountDownTimer(Long.parseLong(locationResponseData.getDeliveryLong()), 1000) {

                public void onTick(long millisUntilFinished) {
                    timerValue.setText("" + millisUntilFinished / 1000);
                    //here you can have your logic to set text to edittext
                }

                public void onFinish() {
                    timerValue.setText("done!");
                    locationResponseDatas.remove(item);
                    notifyDataSetChanged();
                }

            }.start();*/

            new CountDownTimer(30000, 1000) {

                public void onTick(long millisUntilFinished) {
                    timerValue.setText("" + millisUntilFinished / 1000);
                    //here you can have your logic to set text to edittext
                }

                public void onFinish() {
                    timerValue.setText("done!");
                    locationResponseDatas.remove(item);
                    notifyDataSetChanged();
                    if (Preference.getAcceptedOrderStatusValue().equalsIgnoreCase(""))
                        AppController.getInstance().updatingTextView.updatingTextView();
                    else
                        Utility.showingLog("no updation", "oks");
                }

            }.start();
        }


    }


}


