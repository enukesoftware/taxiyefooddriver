package com.taxiyefood.driver.adapter;


import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.taxiyefood.driver.R;
import com.taxiyefood.driver.databean.Country;

import java.util.ArrayList;


/**
 * Created by enuke on 18/5/16.
 */
public class SearchCountryAdapter extends RecyclerView.Adapter<SearchCountryAdapter.ViewHolder> implements Filterable {

    private Context context;
    private ArrayList<Country> countryList;
    private ArrayList<Country> countryListFiltered;
    private String selectedCountry;
    private CountryAdapterListener listener;

    public SearchCountryAdapter(Context context, ArrayList<Country> countryList,String selectedCountry,CountryAdapterListener listener) {
        this.context = context;
        this.countryList = countryList;
        this.selectedCountry = selectedCountry;
        this.listener = listener;
        this.countryListFiltered = countryList;
    }

    @Override
    public SearchCountryAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                              int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.search_country_item, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;

    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {

        final Country currentItem = countryListFiltered.get(position);

        holder.tvCountryCode.setText(currentItem.getCountryCode());
        holder.tvCountryName.setText(currentItem.getCountryName());

        holder.rlSearchCountryItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onCountrySelected(currentItem);
            }
        });
    }


    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return countryListFiltered.size();
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                Log.e("FilterResults:",charString);
                if (charString.isEmpty()) {
                    countryListFiltered = countryList;
                } else {
                    ArrayList<Country> filteredList = new ArrayList<>();
                    for (Country row : countryList) {

                        // name match condition. this might differ depending on your requirement
                        // here we are looking for name or code match
                        if (row.getCountryName().toLowerCase().contains(charString.toLowerCase()) || row.getCountryCode().toLowerCase().contains(charString.toLowerCase())) {
                            filteredList.add(row);
                        }
                    }

                    countryListFiltered = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = countryListFiltered;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                countryListFiltered = (ArrayList<Country>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }


    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public class ViewHolder extends RecyclerView.ViewHolder {

        // each data item is just a string in this case

        RelativeLayout rlSearchCountryItem;
        TextView tvCountryName,tvCountryCode;


        public ViewHolder(View itemView) {
            super(itemView);

            rlSearchCountryItem = itemView.findViewById(R.id.rlCountryItem);
            tvCountryName = itemView.findViewById(R.id.tvCountryName);
            tvCountryCode = itemView.findViewById(R.id.tvCountryCode);

        }
    }

    public interface CountryAdapterListener {
        void onCountrySelected(Country country);
    }


}


