package com.taxiyefood.driver.adapter;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.taxiyefood.driver.R;
import com.taxiyefood.driver.serverrequest.CompletedOrderResponseData;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;


/**
 * Created by enuke on 18/5/16.
 */
public class CompletedOrderAdapter extends RecyclerView.Adapter<CompletedOrderAdapter.ViewHolder> {

    private Context context;
    private ArrayList<CompletedOrderResponseData> acceptedOrderBeen;

    public CompletedOrderAdapter(Context context, ArrayList<CompletedOrderResponseData> acceptedOrderBeen) {
        this.context = context;
        this.acceptedOrderBeen = acceptedOrderBeen;
    }

    @Override
    public CompletedOrderAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                               int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.completed_order_item, parent, false);
        ViewHolder vhFS = new ViewHolder(v);
        return vhFS;

    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {

        final CompletedOrderResponseData completedOrderResponseData = acceptedOrderBeen.get(position);


        holder.orderId.setText(completedOrderResponseData.getOrder_number());
        holder.deliverTo.setText(completedOrderResponseData.getFirst_name()+" "+(completedOrderResponseData.getLast_name() == null ? "" : completedOrderResponseData.getLast_name()));
        holder.amount.setText(context.getResources().getString(R.string.currency_symbol)+completedOrderResponseData.getAmount());
        holder.dateSet.setText(convertedDateWithSlash(completedOrderResponseData.getDate()+" "+completedOrderResponseData.getTime()));

    }

    public String convertedDateWithSlash(String dateValue) {
        SimpleDateFormat parseFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        SimpleDateFormat displayFormat = new SimpleDateFormat("dd MMM yyyy hh:mm a");
        Date date = null;
        try {
            date = parseFormat.parse(dateValue);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        String dateSending = displayFormat.format(date);
        return dateSending;
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return acceptedOrderBeen.size();
    }



    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public class ViewHolder extends RecyclerView.ViewHolder {

        // each data item is just a string in this case

        TextView orderId,deliverTo,amount,dateSet;


        public ViewHolder(View itemView) {
            super(itemView);

            orderId = itemView.findViewById(R.id.orderIdValue);
            deliverTo = itemView.findViewById(R.id.deliverToValue);
            amount = itemView.findViewById(R.id.amountValue);
            dateSet = itemView.findViewById(R.id.dateSettomg);


        }




    }


}


