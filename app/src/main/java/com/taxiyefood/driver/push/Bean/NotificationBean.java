package com.taxiyefood.driver.push.Bean;

/**
 * Created by enuke on 6/23/15.
 */
public class NotificationBean {

    String notificationId;
    String packet;
    String active;
    String isAccepted;

    public NotificationBean(String notificationId, String packet, String active, String isAccepted) {
        this.notificationId = notificationId;
        this.packet = packet;
        this.active = active;
        this.isAccepted = isAccepted;
    }

    public NotificationBean() {
    }

    public String getNotificationId() {
        return notificationId;
    }

    public void setNotificationId(String notificationId) {
        this.notificationId = notificationId;
    }

    public String getPacket() {
        return packet;
    }

    public void setPacket(String packet) {
        this.packet = packet;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public String getIsAccepted() {
        return isAccepted;
    }

    public void setIsAccepted(String isAccepted) {
        this.isAccepted = isAccepted;
    }
}
