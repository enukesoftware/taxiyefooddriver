package com.taxiyefood.driver.push.Bean;

import com.google.gson.annotations.SerializedName;


/**
 * Created by enuke on 6/8/15.
 */
public class NotificationResponseBean {

    @SerializedName("notification")
    NotificationResponseDetailBean notification;

    public NotificationResponseBean(NotificationResponseDetailBean notification) {
        this.notification = notification;
    }

    public NotificationResponseDetailBean getNotification() {
        return notification;
    }

    public void setNotification(NotificationResponseDetailBean notification) {
        this.notification = notification;
    }
}
