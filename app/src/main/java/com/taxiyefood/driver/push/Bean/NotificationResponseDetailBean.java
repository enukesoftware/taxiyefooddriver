package com.taxiyefood.driver.push.Bean;

import com.google.gson.annotations.SerializedName;


/**
 * Created by enuke on 6/8/15.
 */
public class NotificationResponseDetailBean {

    @SerializedName("status")
    String status;
    @SerializedName("msg")
    String msg;

    public NotificationResponseDetailBean(String status, String msg) {
        this.status = status;
        this.msg = msg;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
