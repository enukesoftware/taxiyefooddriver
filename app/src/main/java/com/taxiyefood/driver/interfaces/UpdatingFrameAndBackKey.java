package com.taxiyefood.driver.interfaces;


import com.taxiyefood.driver.fragment.AcceptCancelFragment;


/**
 * Created by enuke on 8/6/16.
 */
public interface UpdatingFrameAndBackKey {

    void selectedFragment(AcceptCancelFragment acceptCancelFragment);

}
