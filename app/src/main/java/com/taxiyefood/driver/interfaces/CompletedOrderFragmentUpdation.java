package com.taxiyefood.driver.interfaces;


import com.taxiyefood.driver.fragment.CompleteOrderFragment;


/**
 * Created by enuke on 8/6/16.
 */
public interface CompletedOrderFragmentUpdation {

    void selectedFragment(CompleteOrderFragment completeOrderFragment);

}
