package com.taxiyefood.driver;

import android.os.Parcel;
import android.os.Parcelable;

public class LocationObject implements Parcelable {

    private double mLattitude;
    private double mLongitude;
    private double pickUpLongitude;
    private double pickUpLatitude;
    private double deliverLongitude;
    private double deliverLatitude;
    private String orderId;
    private String orderIdValue;
    private String orderStatus;
    private String uId;
   // private float bearing;

    public LocationObject(double mLattitude, double mLongitude) {
        this.mLattitude = mLattitude;
        this.mLongitude = mLongitude;
    }

    public LocationObject() {

    }

    protected LocationObject(Parcel in) {
        mLattitude = in.readDouble();
        mLongitude = in.readDouble();
        pickUpLongitude = in.readDouble();
        pickUpLatitude = in.readDouble();
        deliverLongitude = in.readDouble();
        deliverLatitude = in.readDouble();
        orderId = in.readString();
        orderStatus = in.readString();
        uId = in.readString();
    }

    public static final Creator<LocationObject> CREATOR = new Creator<LocationObject>() {
        @Override
        public LocationObject createFromParcel(Parcel in) {
            return new LocationObject(in);
        }

        @Override
        public LocationObject[] newArray(int size) {
            return new LocationObject[size];
        }
    };

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getuId() {
        return uId;
    }

    public void setuId(String uId) {
        this.uId = uId;
    }

    public double getPickUpLongitude() {
        return pickUpLongitude;
    }

    public void setPickUpLongitude(double pickUpLongitude) {
        this.pickUpLongitude = pickUpLongitude;
    }

    public double getPickUpLatitude() {
        return pickUpLatitude;
    }

    public void setPickUpLatitude(double pickUpLatitude) {
        this.pickUpLatitude = pickUpLatitude;
    }

    public double getDeliverLongitude() {
        return deliverLongitude;
    }

    public void setDeliverLongitude(double deliverLongitude) {
        this.deliverLongitude = deliverLongitude;
    }

    public double getDeliverLatitude() {
        return deliverLatitude;
    }

    public void setDeliverLatitude(double deliverLatitude) {
        this.deliverLatitude = deliverLatitude;
    }

    public double getmLattitude() {
        return mLattitude;
    }

    public void setmLattitude(double mLattitude) {
        this.mLattitude = mLattitude;
    }

    public double getmLongitude() {
        return mLongitude;
    }

    public void setmLongitude(double mLongitude) {
        this.mLongitude = mLongitude;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeDouble(mLattitude);
        dest.writeDouble(mLongitude);
        dest.writeDouble(pickUpLongitude);
        dest.writeDouble(pickUpLatitude);
        dest.writeDouble(deliverLongitude);
        dest.writeDouble(deliverLatitude);
        dest.writeString(orderId);
        dest.writeString(orderStatus);
        dest.writeString(uId);
    }

    public String getOrderStatus() {
        return orderStatus;
    }

    public String getOrderIdValue() {
        return orderIdValue;
    }

    public void setOrderIdValue(String orderIdValue) {
        this.orderIdValue = orderIdValue;
    }

    public void setOrderStatus(String orderStatus) {
        this.orderStatus = orderStatus;
    }
}
