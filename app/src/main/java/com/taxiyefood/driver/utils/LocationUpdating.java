package com.taxiyefood.driver.utils;


import android.content.Context;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.taxiyefood.driver.R;
import com.taxiyefood.driver.data.AppController;
import com.taxiyefood.driver.serverrequest.LocationResponse;
import com.taxiyefood.driver.serverrequest.LocationResponseData;

import java.util.ArrayList;


/**
 * Created by enuke on 24/2/16.
 */
public class LocationUpdating {

    public void updateLocationValues(String locationJson, final Context context)
    {

        String url = Constant.LOCATION_METHOD;
        Utility.showingLog("url", "" + url + "request" + locationJson);
        GsonRequest gsonRequest = new GsonRequest<LocationResponse>(Request.Method.POST, url,
                LocationResponse.class, locationJson, new Response.Listener<LocationResponse>() {
            @Override
            public void onResponse(LocationResponse locationResponse) {
                ArrayList<LocationResponseData> locationResponseData = locationResponse.getData();
                boolean status = locationResponse.isSuccess();
                if (status) {
                    if(locationResponseData.size()>0)
                    {
                        Utility.showingLog("Location updating",""+locationResponseData);
                        Utility.locationResponseDatas  = locationResponseData;
                        if(AppController.getInstance().updateAcceptedOrder != null) {
                            if(Preference.getAcceptedOrderStatusValue().equalsIgnoreCase(""))
                            AppController.getInstance().updateAcceptedOrder.updateData();
                        }
                        else
                        {
                            Utility.showingLog("accepted recycler null",""+AppController.getInstance().updateAcceptedOrder);
                        }
                    }
                } else {

                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        if (volleyError != null) {
                            if (volleyError.getMessage() == null)
                                Utility.showingLog("location volley error",""+ context.getResources().getString(R.string.network_error));
                            else
                                Utility.showingLog("location volley error",""+ volleyError.getMessage());
                        }
                    }
                });
        DefaultRetryPolicy policy = new DefaultRetryPolicy(2 * 60 * 1000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        gsonRequest.setRetryPolicy(policy);
        AppController.getInstance().addToRequestQueue(gsonRequest);
    }

}
