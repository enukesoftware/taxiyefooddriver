package com.taxiyefood.driver.utils;


import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;

import androidx.annotation.Nullable;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.taxiyefood.driver.R;
import com.taxiyefood.driver.serverrequest.LocationData;

import java.util.Timer;
import java.util.TimerTask;


/**
 * Created by enuke on 17/5/16.
 */
public class LocUpdate extends Service {

    public Timer timer;
    TimerTask timerTask;
    final Handler handler = new Handler();
    private static final String TAG = "LocUpdate";
    TimerReceiver timerReceiver;
   public final static String ACTION = "com.taxiyefood.driver.intent.action.USER_ACTION";
    public final static int RQS_STOP_SERVICE = 1;
    IntentFilter intentFilter;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {

        super.onCreate();

    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Utility.showingErrorLog(TAG, "onStartCommand");
        super.onStartCommand(intent, flags, startId);

         intentFilter = new IntentFilter(ACTION);
        intentFilter.addCategory(Intent.CATEGORY_DEFAULT);
     //   intentFilter.addAction(ACTION);
        timerReceiver = new TimerReceiver();
//        registerReceiver(timerReceiver, intentFilter);
        LocalBroadcastManager.getInstance(this).registerReceiver(timerReceiver,intentFilter);
        Utility.showingLog("receiver check",""+timerReceiver    );

        startTimer();
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
//        this.unregisterReceiver(timerReceiver);
        try{
            LocalBroadcastManager.getInstance(this).unregisterReceiver(timerReceiver);
        }catch (Exception e){
            e.printStackTrace();
        }
        super.onDestroy();

    }

    public void startTimer() {
        //set a new Timer
        timer = new Timer();

        //initialize the TimerTask's job
        initializeTimerTask();
        //schedule the timer, after the first 5000ms the TimerTask will run every 10000ms
        timer.schedule(timerTask, 5000, 60000); //
    }


    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        return networkInfo != null && networkInfo.isConnected();
    }


    public void initializeTimerTask() {
        timerTask = new TimerTask() {
            public void run() {
                //use a handler to run a toast that shows the current timestamp
                handler.post(new Runnable() {
                    public void run() {
                        //get the current timeStamp
                        try {
                            if (isNetworkAvailable()) {
                                GPSTracker gpsTracker = new GPSTracker(getApplicationContext());
                                if(gpsTracker.getLocation() != null)
                                {
                                    String latitude = "" + gpsTracker.getLatitude();
                                    String longitude = "" + gpsTracker.getLongitude();
                                    Utility.showingLog("lati longi at 30 sec",""+latitude+""+longitude);
                                    if(latitude.equalsIgnoreCase("0.0") && longitude.equalsIgnoreCase("0.0"))
                                        Utility.showingLog(TAG,"request will not go");
                                    else {
                                        if(Preference.getUserIdCommon().equalsIgnoreCase(""))
                                            Utility.showingLog(TAG,"request will not go as user is not registered");
                                        else {
                                            if(Preference.getActiveDeactiveStatus())
                                            createGSONRequest(latitude, longitude, "2"); // Utility.showingLog(TAG,"request will  go");
                                            else
                                                Utility.showingLog(TAG,"request will not go as user is deactive");
                                        }
                                    }
// createGSONRequest(latitude,longitude);
                                }

                            } else {
                                Utility.showingLog("error", getString(R.string.no_internet));
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
        };
    }


    private void createGSONRequest(String latitude, String longitude,String status) {
        String user_id = Preference.getUserIdCommon();
        LocationData locationData = new LocationData(user_id,latitude, longitude,status);
      //  LocationRequest locationRequest = new LocationRequest(locationData);
        Gson gson = new GsonBuilder().create();
        String locationJson = gson.toJson(locationData);
        LocationUpdating locationUpdating = new LocationUpdating();
        locationUpdating.updateLocationValues(locationJson,LocUpdate.this);

    }

    public class TimerReceiver extends BroadcastReceiver
    {

        @Override
        public void onReceive(Context arg0, Intent arg1) {
            int rqs = arg1.getIntExtra("Loc", 0);
            Log.i("receiver call",""+rqs);
            if (rqs == RQS_STOP_SERVICE){
                stopSelf();
                timer.cancel();
            }
        }
    }
}
