package com.taxiyefood.driver.utils;


import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import com.android.volley.toolbox.ImageLoader;


/**
 * Created by enuke on 17/5/16.
 */
public class MyCache implements ImageLoader.ImageCache {

    @Override
    public Bitmap getBitmap(String key) {

        if (key.contains("drawable://")) {
            return BitmapFactory.decodeFile(key.substring(key.indexOf("drawable://") + 7));
            // return decodeSampledBitmapFromPath(key, 100, 100);
        } else {
            // Here you can add an actual cache
            return null;
        }
    }

    @Override
    public void putBitmap(String key, Bitmap bitmap) {
        // Here you can add an actual cache
    }

    public static Bitmap decodeSampledBitmapFromPath(String path, int reqWidth,
                                                     int reqHeight) {

        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(path.substring(path.indexOf("file://") + 7), options);

        options.inSampleSize = calculateInSampleSize(options, reqWidth,
                reqHeight);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        Bitmap bmp = BitmapFactory.decodeFile(path.substring(path.indexOf("file://") + 7), options);
        return bmp;
    }

    public static int calculateInSampleSize(BitmapFactory.Options options,
                                            int reqWidth, int reqHeight) {

        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {
            if (width > height) {
                inSampleSize = Math.round((float) height / (float) reqHeight);
            } else {
                inSampleSize = Math.round((float) width / (float) reqWidth);
            }
        }
        return inSampleSize;
    }
}