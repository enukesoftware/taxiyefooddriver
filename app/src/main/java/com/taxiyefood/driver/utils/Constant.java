package com.taxiyefood.driver.utils;


/**
 * Created by enuke on 5/2/16.
 */
public class Constant {


  //  public static final String BASE_URL="http://devfb.yiipro.com";//"http://foodbox.yiipro.com";
  //  public static final String BASE_URL ="http://devfbinternal.yiipro.com";
  //  public static final String BASE_URL ="http://safarimeals.yiipro.com";
  //  public static final String BASE_URL ="http://sfstaging.yiipro.com";

    public static final String HOME_URL = "http://safarimeals.com";
//    public static final String STAGING_URL = "http://sfstaging.yiipro.com";
    public static final String STAGING_URL = "http://fd.yiipro.com";

    public static final String BASE_URL = STAGING_URL;
    public static final String BASE_URL_ENGLISH_ADDITION="/en/api/v1";
    public static final String BASE_URL_FRENCH_ADDITION="/Fr/api/v1";


    public static final String URL_TYPE = BASE_URL_ENGLISH_ADDITION;

    public static final String LOGIN_METHOD = BASE_URL + URL_TYPE + "/driver/signin";
    public static final String UPDATETOKEN_METHOD = BASE_URL + URL_TYPE + "/update-device-token";

    public static final String LOCATION_METHOD = BASE_URL + URL_TYPE + "/driver/OrderList";


    public static final String UPDATE_DEVICE_ID = BASE_URL + URL_TYPE + "/update-deviceId";

    public static final String ACTIVATION_METHOD = BASE_URL + URL_TYPE + "/driver/Update";

    public static final String ACCEPT_ORDER_METHOD = BASE_URL + URL_TYPE + "/driver/OrderUpdate";

    public static final String COMPLETED_ORDERS = BASE_URL + URL_TYPE + "/driver/OrderByDriverId";
    public static final String UPDATE_PROFILE = BASE_URL + URL_TYPE + "/driver/update";
    public static final String SIGNUP = BASE_URL + URL_TYPE + "/driver/signup";
    public static final String LOGOUT = BASE_URL + URL_TYPE + "/driver/signout";
    public static final String CHANGE_PASSWORD = BASE_URL + URL_TYPE + "/driver/changepassword";

    public static final String SENDER_ID = "851658446642";
    public static final String PUSH_DATA = "PUSH_DATA";


    public static final String USERNAME = "username";
    public static final String PASSWORD = "password";
    public static final String USER_ID_COMMON = "user_id";
    public static final String ISLOGIN = "is_login";


    public static final String PUSHTOKEN = "PUSHTOKEN";

    public static final String DEVICE_ID = "device_id";

    public static final String NOTIFICATION_COUNT = "noti_count";

    public static final String PUSH_RECEIVE = "PUSH_Receive";
    public static final String NOTIFICATION_ID = "Notification_id";

    // Directory name to store captured images and videos
    public static final String IMAGE_DIRECTORY_NAME = "TruckerChamp";

    public static final String BADGE_COUNT = "badge_count";

    public static final String ACTIVE_DEACTIVE = "active_deactive";

    public static final String FRAME_VISIBILITY = "frame_visibility";

    public static final String ACCEPTED_ORDER_VALUE = "accepted_order_Value";
    public static final String PREFERENCES_UPDATE_NOTIFICATION_STATUS = "noti_status";
    public static final String USER_DATA = "USER_DATA";
    public static final String DEFAULT_COUNTRY_CODE = "251";



}
