package com.taxiyefood.driver.utils;


import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.taxiyefood.driver.R;
import com.taxiyefood.driver.data.AppController;
import com.taxiyefood.driver.serverrequest.LocationResponseData;
import com.taxiyefood.driver.serverrequest.UpdateDeviceData;
import com.taxiyefood.driver.serverrequest.UpdateDeviceId;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;

/**
 * Created by enuke on 14/10/15.
 */
public class Utility {


    public static ArrayList<LocationResponseData> locationResponseDatas = new ArrayList<>();

    public static boolean isInternetConnected(Context mContext) {


        final ConnectivityManager connectivityManager = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        final NetworkInfo ni = connectivityManager.getActiveNetworkInfo();

        return ni != null && ni.isConnectedOrConnecting();
    }

    public static void showToast(Context context, String message) {
        Toast.makeText(context, message, Toast.LENGTH_LONG).show();
    }

    public static void showingLog(String TAG, String message) {
        Log.i(TAG,message);
    }

    public static void showingErrorLog(String TAG, String message) {
        Log.e(TAG,message);
    }



    public static void updateDeviceID(final Context mContext, String GCMid, String userid) {
        String url = Constant.UPDATE_DEVICE_ID;

        JSONObject update_user = new JSONObject();
        JSONObject user = new JSONObject();

        try {
            user.put("id", userid);
            user.put("gcm_id", GCMid);

            update_user.put("user", user);

        } catch (Exception e) {
            e.printStackTrace();
        }

        GsonRequest gsonRequest = new GsonRequest<UpdateDeviceId>(Request.Method.POST, url,
                UpdateDeviceId.class, update_user.toString(), new Response.Listener<UpdateDeviceId>() {

            @Override
            public void onResponse(UpdateDeviceId updateDeviceId) {

                UpdateDeviceData updateDeviceData = updateDeviceId.getUpdateDeviceData();

                String status = updateDeviceData.getStatus();

                if (status.equalsIgnoreCase("success")) {
                    status = "Msg: " + updateDeviceData.getMsg() + "\n";
                    Utility.showingLog(" GCM id sent", "" + status);
                } else {
                    String textResult = "Message:GCM id not sent " + updateDeviceData.getMsg() + "\n";
                    Utility.showingLog("Reslt", textResult);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                if (volleyError != null) {
                    if(volleyError instanceof NetworkError)
                        showingLog("Utility",mContext.getString(R.string.network_error));
                    else
                        showingLog("Utility",volleyError.getMessage());
                }
 /*Utility.showToast(mContext, "MSG" + volleyError.getMessage());
                    System.out.println("ERROR:" + volleyError.getLocalizedMessage());*/

            }
        });

        DefaultRetryPolicy policy = new DefaultRetryPolicy(2 * 60 * 1000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        gsonRequest.setRetryPolicy(policy);

        AppController.getInstance().addToRequestQueue(gsonRequest);
    }


//    public static String getDeviceKey(Context mContext) {
//        String deviceId;
//        if (Preference.getDeviceId().length() > 0) {
//            deviceId = Preference.getDeviceId();
//
//        } else {
//            deviceId = ((TelephonyManager) mContext.getSystemService(Context.TELEPHONY_SERVICE)).getDeviceId();
//            if (deviceId == null || deviceId == "" || deviceId == " " || deviceId == "0") {
//
//                try {
//                    deviceId = Settings.Secure.getString(mContext.getContentResolver(), Settings.Secure.ANDROID_ID);
//                } catch (Exception e) {
//                    deviceId = String.valueOf(Calendar.getInstance().getTimeInMillis());
//                }
//            }
//        }
//        return deviceId;
//
//    }


    private static ProgressDialog progressDialog = null;

    public static void showProgressDialog(Context context, String title, String message) {
        progressDialog = new ProgressDialog(context);
        Utility.showingLog("progress dialog value", "pdialog" + progressDialog);
        progressDialog.setTitle(title);
        progressDialog.setMessage(message);
        progressDialog.setCancelable(false);
        progressDialog.show();

    }

    public static void dismissProgressDialog() {
        if (progressDialog != null)
            progressDialog.dismiss();
        progressDialog = null;
    }

    public static void showConfirmationDialogWithoutTitle(final Activity context, String message) {
        AlertDialog.Builder builder =
                new AlertDialog.Builder(context, R.style.AppCompatAlertDialogStyle);
        builder.setMessage(message);
        builder.setCancelable(false);
        builder.setPositiveButton ( "OK", new DialogInterface.OnClickListener () {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.show();
    }
    public static void onVolleyErrorResponse(Context context, VolleyError volleyError) {
        NetworkResponse networkResponse = volleyError.networkResponse;
        Log.e("Volley", "Error. HTTP Status Code:" + networkResponse);
        if (volleyError instanceof NoConnectionError) {
            showToast(context, context.getResources().getString(R.string.no_connection_error));
        } else if (volleyError instanceof TimeoutError) {
            showToast(context, context.getResources().getString(R.string.timeoutMessage));
        } else if (volleyError instanceof NetworkError) {
            showToast(context, context.getResources().getString(R.string.network_error1));
        } else if (volleyError instanceof ServerError) {
            showToast(context, context.getResources().getString(R.string.server_error));
        } else if (volleyError instanceof AuthFailureError) {
            showToast(context, context.getResources().getString(R.string.auth_error));
        } else if (volleyError instanceof ParseError) {
            showToast(context, context.getResources().getString(R.string.other_error));
        }
        //  pDialog.dismiss();
    }

    public static void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = activity.getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    public static void RequestPermission(Activity activity, String[] permission, int requsetcode) {
        int version = Build.VERSION.SDK_INT;
        if (version >= Build.VERSION_CODES.M) {
            activity.requestPermissions(permission, requsetcode);
        }
    }

    /**
     * Check array of permission if version is grater than 23.
     * if less than 23 then will return true.
     *
     * @param activity
     * @param permission
     * @return
     */
    public static boolean isPermissionGranted(Activity activity, String[] permission) {
        boolean isGranted = true;
        int version = Build.VERSION.SDK_INT;
        if (version >= Build.VERSION_CODES.M) {
            for (String permi : permission) {
                if (activity.checkSelfPermission(permi) == PackageManager.PERMISSION_DENIED) {
                    isGranted = false;
                }
            }
        }
        return isGranted;
    }
}
