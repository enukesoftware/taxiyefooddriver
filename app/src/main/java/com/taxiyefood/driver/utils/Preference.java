package com.taxiyefood.driver.utils;

import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.taxiyefood.driver.data.AppController;
import com.taxiyefood.driver.serverrequest.LoginResponseDataValue;

/**
 * Created by enuke on 10/9/15.
 */
public class Preference {
    final static String PREFS_NAME = "TaxiyeFoodDriver";



    public static void setUserIdCommon(String value) {
        final SharedPreferences appData = AppController.getInstance().getSharedPreferences(
                PREFS_NAME, 0);
        SharedPreferences.Editor editor = appData.edit();
        editor.putString(Constant.USER_ID_COMMON, value);
        editor.commit();
    }


    public static String getUserIdCommon() {
        final SharedPreferences appData = AppController.getInstance().getSharedPreferences(
                PREFS_NAME, 0);
        final String preData = appData.getString(Constant.USER_ID_COMMON, "").trim();
        return preData;
    }

    public static void setUserData(LoginResponseDataValue value) {
        final SharedPreferences appData = AppController.getInstance().getSharedPreferences(
                PREFS_NAME, 0);
        SharedPreferences.Editor editor = appData.edit();
        editor.putString(Constant.USER_DATA, new Gson().toJson(value));
        editor.commit();
    }


    public static LoginResponseDataValue getUserData() {
        final SharedPreferences appData = AppController.getInstance().getSharedPreferences(
                PREFS_NAME, 0);
        final String preData = appData.getString(Constant.USER_DATA, "").trim();

        if (preData != null && preData.length()>0){
            return new Gson().fromJson(preData, LoginResponseDataValue.class);
        }else{
            return null;
        }
    }

    public static void setIsLogin(boolean value) {
        final SharedPreferences appData = AppController.getInstance().getSharedPreferences(
                PREFS_NAME, 0);
        SharedPreferences.Editor editor = appData.edit();
        editor.putBoolean(Constant.ISLOGIN, value);
        editor.commit();

    }

    public static boolean getIsLogin() {
        final SharedPreferences appData = AppController.getInstance().getSharedPreferences(
                PREFS_NAME, 0);
        final boolean preData = appData.getBoolean(Constant.ISLOGIN, false);
        return preData;

    }


    public static void setPushToken(String value) {
        final SharedPreferences appData = AppController.getInstance().getSharedPreferences(
                PREFS_NAME, 0);
        SharedPreferences.Editor editor = appData.edit();
        editor.putString(Constant.PUSHTOKEN, value);
        editor.commit();

    }

    public static String getPushToken() {
        final SharedPreferences appData = AppController.getInstance().getSharedPreferences(
                PREFS_NAME, 0);
        final String preData = appData.getString(Constant.PUSHTOKEN, "");
        return preData;

    }


    public static void setDeviceId(String value) {
        final SharedPreferences appData = AppController.getInstance().getSharedPreferences(
                PREFS_NAME, 0);
        SharedPreferences.Editor editor = appData.edit();
        editor.putString(Constant.DEVICE_ID, value);
        editor.commit();

    }

    public static String getDeviceId() {
        final SharedPreferences appData = AppController.getInstance().getSharedPreferences(
                PREFS_NAME, 0);
        final String preData = appData.getString(Constant.DEVICE_ID, "");
        return preData;

    }

    public static void setNotificationCount(int value) {
        final SharedPreferences appData = AppController.getInstance().getSharedPreferences(
                PREFS_NAME, 0);
        SharedPreferences.Editor editor = appData.edit();
        editor.putInt(Constant.NOTIFICATION_COUNT, value);
        editor.commit();

    }

    public static int getNotificationCount() {
        final SharedPreferences appData = AppController.getInstance().getSharedPreferences(
                PREFS_NAME, 0);
        final int preData = appData.getInt(Constant.NOTIFICATION_COUNT, 0);
        return preData;

    }


    public static void setPushData(String value) {
        final SharedPreferences appData = AppController.getInstance().getSharedPreferences(
                PREFS_NAME, 0);
        SharedPreferences.Editor editor = appData.edit();
        editor.putString(Constant.PUSH_DATA, value);
        editor.commit();

    }

    public static String getPushData() {
        final SharedPreferences appData = AppController.getInstance().getSharedPreferences(
                PREFS_NAME, 0);
        final String preData = appData.getString(Constant.PUSH_DATA, "");
        return preData;

    }

    public static void setBadgeCount(int value) {
        final SharedPreferences appData = AppController.getInstance().getSharedPreferences(
                PREFS_NAME, 0);
        SharedPreferences.Editor editor = appData.edit();
        editor.putInt(Constant.BADGE_COUNT, value);
        editor.commit();

    }

    public static int getBadgeCount() {
        final SharedPreferences appData = AppController.getInstance().getSharedPreferences(
                PREFS_NAME, 0);
        final int preData = appData.getInt(Constant.BADGE_COUNT, 0);
        return preData;

    }

    public static void setNotificationId(String value) {
        final SharedPreferences appData = AppController.getInstance().getSharedPreferences(
                PREFS_NAME, 0);
        SharedPreferences.Editor editor = appData.edit();
        editor.putString(Constant.NOTIFICATION_ID, value);
        editor.commit();

    }

    public static String getNotificationId() {
        final SharedPreferences appData = AppController.getInstance().getSharedPreferences(
                PREFS_NAME, 0);
        final String preData = appData.getString(Constant.NOTIFICATION_ID, "");
        return preData;

    }


    public static void setPushReceive(boolean value) {
        final SharedPreferences appData = AppController.getInstance().getSharedPreferences(
                PREFS_NAME, 0);
        SharedPreferences.Editor editor = appData.edit();
        editor.putBoolean(Constant.PUSH_RECEIVE, value);
        editor.commit();

    }

    public static boolean isPushReceive() {
        final SharedPreferences appData = AppController.getInstance().getSharedPreferences(
                PREFS_NAME, 0);
        final boolean preData = appData.getBoolean(Constant.ISLOGIN, false);
        return preData;

    }


   /* public static void setPickDeliveryStatus(String key,boolean value) {
        final SharedPreferences appData = AppController.getInstance().getSharedPreferences(
                PREFS_NAME, 0);
        SharedPreferences.Editor editor = appData.edit();
        editor.putBoolean(key, value);
        editor.commit();

    }

    public static boolean getPickDeliveryStatus(String key) {
        final SharedPreferences appData = AppController.getInstance().getSharedPreferences(
                PREFS_NAME, 0);
        final boolean preData = appData.getBoolean(key, false);
        return preData;

    }
*/

    public static void setActiveDeactiveStatus(boolean value) {
        final SharedPreferences appData = AppController.getInstance().getSharedPreferences(
                PREFS_NAME, 0);
        SharedPreferences.Editor editor = appData.edit();
        editor.putBoolean(Constant.ACTIVE_DEACTIVE, value);
        editor.commit();

    }

    public static boolean getActiveDeactiveStatus() {
        final SharedPreferences appData = AppController.getInstance().getSharedPreferences(
                PREFS_NAME, 0);
        final boolean preData = appData.getBoolean(Constant.ACTIVE_DEACTIVE, true);
        return preData;

    }


    public static void setPickDeliveryStatus(String key,int value) {
        final SharedPreferences appData = AppController.getInstance().getSharedPreferences(
                PREFS_NAME, 0);
        SharedPreferences.Editor editor = appData.edit();
        editor.putInt(key, value);
        editor.commit();

    }

    public static int getPickDeliveryStatus(String key) {
        final SharedPreferences appData = AppController.getInstance().getSharedPreferences(
                PREFS_NAME, 0);
        final int preData = appData.getInt(key, 0);
        return preData;

    }

    public static void setStatus(String key,int value) {
        final SharedPreferences appData = AppController.getInstance().getSharedPreferences(
                PREFS_NAME, 0);
        SharedPreferences.Editor editor = appData.edit();
        editor.putInt(key, value);
        editor.commit();

    }

    public static int getStatus(String key) {
        final SharedPreferences appData = AppController.getInstance().getSharedPreferences(
                PREFS_NAME, 0);
        final int preData = appData.getInt(key, 0);
        return preData;

    }


    public static void setAcceptedOrderStatusValue(String value) {
        final SharedPreferences appData = AppController.getInstance().getSharedPreferences(
                PREFS_NAME, 0);
        SharedPreferences.Editor editor = appData.edit();
        editor.putString(Constant.ACCEPTED_ORDER_VALUE, value);
        editor.commit();

    }

    public static String getAcceptedOrderStatusValue() {
        final SharedPreferences appData = AppController.getInstance().getSharedPreferences(
                PREFS_NAME, 0);
        final String preData = appData.getString(Constant.ACCEPTED_ORDER_VALUE, "");
        return preData;

    }

    public static void setFrameVisibilityStatus(boolean value) {
        final SharedPreferences appData = AppController.getInstance().getSharedPreferences(
                PREFS_NAME, 0);
        SharedPreferences.Editor editor = appData.edit();
        editor.putBoolean(Constant.FRAME_VISIBILITY, value);
        editor.commit();

    }

    public static boolean getFrameVisibilityStatus() {
        final SharedPreferences appData = AppController.getInstance().getSharedPreferences(
                PREFS_NAME, 0);
        final boolean preData = appData.getBoolean(Constant.FRAME_VISIBILITY, true);
        return preData;

    }



    public static void clearPreferences() {
    final SharedPreferences appData = AppController.getInstance().getSharedPreferences(
            PREFS_NAME, 0);
    SharedPreferences.Editor editor = appData.edit();
    editor.clear();
    editor.commit();

    }

    public static void putBooleanNotifaicton(String key, boolean value) {
        final SharedPreferences appData = AppController.getInstance().getSharedPreferences(
                PREFS_NAME, 0);
        SharedPreferences.Editor editor = appData.edit();
        editor.putBoolean(key, value);
        editor.commit();

    }

    public static boolean getBooleanNotification(String key) {
        final SharedPreferences appData = AppController.getInstance().getSharedPreferences(
                PREFS_NAME, 0);

        return appData.getBoolean(key, false);
    }


}
