package com.taxiyefood.driver.utils;


/**
 * Created by enuke on 13/6/16.
 */

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.util.Log;
import android.widget.DatePicker;

import androidx.fragment.app.DialogFragment;

import java.util.Calendar;


/**
 * Created by enuke on 8/3/16.
 */
public class NewSelectDateFragment extends DialogFragment {

    // EditText editText;

    int datePassing=0;
    int monthPassing =0;
    int yearPassing = 0;

    String editTextValue;

    int yy,mm,dd;
    DatePickerDialog.OnDateSetListener ondateSet;
   /* public NewSelectDateFragment(EditText editText, int datePassing, int monthPassing, int yearPassing)
    {
        this.editText = editText;
        this.datePassing = datePassing;
        this.monthPassing = monthPassing;
        this.yearPassing = yearPassing;
    }*/

    public NewSelectDateFragment() {
    }

    @Override
    public void setArguments(Bundle args) {
        super.setArguments(args);
        yearPassing = args.getInt("year");
        monthPassing = args.getInt("month");
        datePassing = args.getInt("day");
        editTextValue = args.getString("edit_text_value");
    }

    public void setCallBack(DatePickerDialog.OnDateSetListener ondate) {
        ondateSet = ondate;
    }

    public void setCallBack1(DatePickerDialog.OnDateSetListener ondate) {
        ondateSet = ondate;
    }

    public void setCallBack2(DatePickerDialog.OnDateSetListener ondate) {
        ondateSet = ondate;
    }

    /*
    public NewSelectDateFragment(EditText editText) {
        this.editText = editText;
    }
*/

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final Calendar calendar = Calendar.getInstance();
        int yy = calendar.get(Calendar.YEAR);
        int mm = calendar.get(Calendar.MONTH);
        int dd = calendar.get(Calendar.DAY_OF_MONTH);
        //  return new DatePickerDialog(getActivity(), this, yy, mm, dd);

        Log.i("date coming",""+yy+"month"+mm+"day"+dd+"edittext value"+editTextValue);

        DatePickerDialog d = new DatePickerDialog(getActivity(), ondateSet, yy, mm, dd);
        DatePicker dp = d.getDatePicker();
       // dp.setMinDate(calendar.getTimeInMillis());
        if(editTextValue.equalsIgnoreCase(""))
            Log.i("date fragment","nothing done");
        else
            dp.updateDate(yearPassing,monthPassing,datePassing);
        return d;
    }

    /*public void onDateSet(DatePicker view, int yy, int mm, int dd) {
        Log.i("date setting year",""+yy+"month"+mm+"day"+dd);
        populateSetDate(yy, mm+1, dd);
    }
    public void populateSetDate(int year, int month, int day) {
       // editText.setText(year+"-"+month+"-"+day);
        editText.setText(month+"/"+day+"/"+year);
    }*/

}

