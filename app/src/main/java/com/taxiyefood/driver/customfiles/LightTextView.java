package com.taxiyefood.driver.customfiles;


import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;


/**
 * Created by enuke on 17/5/16.
 */
public class LightTextView extends TextView {

    public LightTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.setTypeface(Typeface.createFromAsset(context.getAssets(),"fonts/montserrat_light.ttf"));
    }
}
