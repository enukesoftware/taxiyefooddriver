package com.taxiyefood.driver.ui;


import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.view.ContextThemeWrapper;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.material.navigation.NavigationView;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.taxiyefood.driver.FloatingViewService;
import com.taxiyefood.driver.R;
import com.taxiyefood.driver.data.AppController;
import com.taxiyefood.driver.data.DatabaseManager;
import com.taxiyefood.driver.data.NotificationTable;
import com.taxiyefood.driver.databean.SignupBean;
import com.taxiyefood.driver.fragment.AcceptCancelFragment;
import com.taxiyefood.driver.fragment.AcceptedOrderFragment;
import com.taxiyefood.driver.fragment.CompleteOrderFragment;
import com.taxiyefood.driver.interfaces.CompletedOrderFragmentUpdation;
import com.taxiyefood.driver.interfaces.UpdatingFrameAndBackKey;
import com.taxiyefood.driver.serverrequest.DriverActivationRequest;
import com.taxiyefood.driver.serverrequest.DriverActivationResponse;
import com.taxiyefood.driver.serverrequest.DriverActivationResponseData;
import com.taxiyefood.driver.serverrequest.LoginResponseDataValue;
import com.taxiyefood.driver.serverrequest.LogoutRequest;
import com.taxiyefood.driver.serverrequest.LogoutResponse;
import com.taxiyefood.driver.utils.Constant;
import com.taxiyefood.driver.utils.GsonRequest;
import com.taxiyefood.driver.utils.LocUpdate;
import com.taxiyefood.driver.utils.MyCustomProgressDialog;
import com.taxiyefood.driver.utils.Preference;
import com.taxiyefood.driver.utils.Res;
import com.taxiyefood.driver.utils.Utility;

import de.hdodenhof.circleimageview.CircleImageView;


public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, UpdatingFrameAndBackKey, CompletedOrderFragmentUpdation, View.OnClickListener {

    public static MainActivity mainActivity;
    public MyReceiver1 broadcastReceiver;
    public Button activeDeactive;
    public ImageView filtering;
    public TextView title;
    Toolbar toolbar;
    SignupBean signupBean;
    NavigationView navigationView;
    private ProgressDialog pDialog;
    private String firebaseToken;
    private Res res;
    private AcceptCancelFragment acceptCancelFragment;
    private CompleteOrderFragment completeOrderFragment;
    RelativeLayout navHeader;
    LoginResponseDataValue userData;
    RequestOptions requestOptions;

    TextView username;
    TextView emailId;
    CircleImageView civProfileImage;
    DrawerLayout drawer;

    public static MainActivity getInstance() {
        return mainActivity;
    }
  /*  @Override
    public Resources getResources() {
        if (res == null) {
            res = new Res(super.getResources());
            Utility.showingLog("changed if","color");

        }
        return res;
    }*/

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Log.e("on create", "create");
        mainActivity = this;
        toolbar = findViewById(R.id.main_toolbar);
        activeDeactive = toolbar.findViewById(R.id.active_deactive);
        filtering = toolbar.findViewById(R.id.filtering);
        title = toolbar.findViewById(R.id.toolbar_title);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        requestOptions = new RequestOptions()
                .placeholder(R.drawable.default_avatar)
                .error(R.drawable.default_avatar);

        if (Preference.getUserIdCommon().equals("") || Preference.getUserIdCommon() == null) {
            Toast.makeText(this, getResources().getString(R.string.message_user_login), Toast.LENGTH_LONG).show();
            Intent i = new Intent(this, Login.class);
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(i);
            finish();
        }

        if (Preference.getAcceptedOrderStatusValue().equalsIgnoreCase(""))
            acceptFragment();
        else
            acceptCancelFragmentAdd();

        settingNotification();


        drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();
        navigationView = findViewById(R.id.nav_view);

        navigationView.setNavigationItemSelectedListener(this);
        navHeader = (RelativeLayout) navigationView.getHeaderView(0);
        username = navHeader.findViewById(R.id.home_user_name);
        emailId = navHeader.findViewById(R.id.home_user_mail_id);
        civProfileImage = navHeader.findViewById(R.id.home_user_pic);
        civProfileImage.setOnClickListener(this);
        navHeader.setOnClickListener(this);



        activeDeactive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Utility.isInternetConnected(MainActivity.this)) {

                    if (Preference.getAcceptedOrderStatusValue().equalsIgnoreCase(""))
                        makeHitForUserStatus(Preference.getActiveDeactiveStatus());
                    else
                        Utility.showToast(MainActivity.this, getString(R.string.act_de_msg));
                } else
                    Utility.showToast(MainActivity.this, getString(R.string.no_internet));
            }
        });
        filtering.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                completeOrderFragment.openingDrawer();
            }
        });

        //     startingTimer();
    }

    private void makeHitForUserStatus(boolean status) {
        DriverActivationRequest driverActivationRequest = new DriverActivationRequest(Preference.getUserIdCommon(), Preference.getActiveDeactiveStatus() ? "0" : "1");
        Gson gson = new GsonBuilder().create();
        String jsonForActivation = gson.toJson(driverActivationRequest);
        // Utility.showToast(MainActivity.this,jsonForActivation);
        makeServerRequest(jsonForActivation, status);
    }

    private void makeServerRequest(String jsonForActivation, final boolean statusCome) {
        pDialog = new MyCustomProgressDialog(MainActivity.this, R.style.MyTheme);
        //pDialog.setMessage("Please wait...");

        pDialog.setCancelable(false);
        pDialog.show();
        String url = Constant.ACTIVATION_METHOD;
        Utility.showingLog("url", "" + url + "request" + jsonForActivation);
        GsonRequest gsonRequest = new GsonRequest<DriverActivationResponse>(Request.Method.POST, url,
                DriverActivationResponse.class, jsonForActivation, new Response.Listener<DriverActivationResponse>() {

            @Override
            public void onResponse(DriverActivationResponse driverActivationResponse) {
                boolean status = driverActivationResponse.isSuccess();
                DriverActivationResponseData driverActivationResponseData = driverActivationResponse.getData();

                if (status) {
                    pDialog.dismiss();
                    if (statusCome) {
                        //   stopService(new Intent(MainActivity.this, LocUpdate.class));
                        Preference.setActiveDeactiveStatus(false);
                        activeDeactive.setBackgroundResource(R.drawable.active);
                        Utility.showingLog("if", "service stopped");
                        Intent intent = new Intent();
                        intent.setAction(LocUpdate.ACTION);
                        intent.putExtra("Loc", LocUpdate.RQS_STOP_SERVICE);
                        sendBroadcast(intent);

                        AcceptedOrderFragment.getInstance().locationResponseDatas.clear();

                        /*if(AcceptedOrderFragment.myRecyclerViewAdapter != null) {
                            if (AcceptedOrderFragment.myRecyclerViewAdapter.getItemCount() > 0) {
                                Utility.showingLog("Mainactivity", "if no update in text view");
                            } else {*/
                        AcceptedOrderFragment.getInstance().noData.setVisibility(View.VISIBLE);
                        AcceptedOrderFragment.getInstance().noData.setText(getString(R.string.deactive));
                            /*}
                        }*/

                    } else {
                        startService(new Intent(MainActivity.this, LocUpdate.class));
                        Utility.showingLog("else", "service started");
                        Preference.setActiveDeactiveStatus(true);
                        activeDeactive.setBackgroundResource(R.drawable.deactive);

                        if (AcceptedOrderFragment.myRecyclerViewAdapter != null) {
                            if (AcceptedOrderFragment.myRecyclerViewAdapter.getItemCount() > 0) {
                                Utility.showingLog("Mainactivity", "else no update in text view");
                            } else {
                                AcceptedOrderFragment.getInstance().noData.setText(getString(R.string.no_data));
                            }
                        }
                    }

                } else {
                    pDialog.dismiss();
//                    Utility.showToast(MainActivity.this,driverActivationResponseData.getStatus());
                }

            }
        },

                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        pDialog.dismiss();
                        if (volleyError != null) {
                            if (volleyError instanceof NetworkError)
                                Utility.showToast(MainActivity.this, getResources().getString(R.string.slow_net));
                            else if (volleyError.getMessage() == null)
                                Utility.showToast(MainActivity.this, getResources().getString(R.string.network_error));
                            else
                                Utility.showToast(MainActivity.this, volleyError.getMessage());


                        }
                    }
                });
        DefaultRetryPolicy policy = new DefaultRetryPolicy(2 * 60 * 1000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        gsonRequest.setRetryPolicy(policy);
        AppController.getInstance().addToRequestQueue(gsonRequest);
    }

    private void acceptFragment() {
        AcceptedOrderFragment acceptedOrderFragment = new AcceptedOrderFragment();
        getSupportFragmentManager().beginTransaction().add(R.id.controllerFragment, acceptedOrderFragment).commit();
    }

    private void acceptFragmentStart() {
        AcceptedOrderFragment acceptedOrderFragment = new AcceptedOrderFragment();
        getSupportFragmentManager().beginTransaction().replace(R.id.controllerFragment, acceptedOrderFragment).commit();
    }

    private void acceptCancelFragmentStart() {
        AcceptCancelFragment acceptCancelFragment = new AcceptCancelFragment();
        getSupportFragmentManager().beginTransaction().replace(R.id.controllerFragment, acceptCancelFragment).commit();
    }

    private void acceptCancelFragmentAdd() {
        AcceptCancelFragment acceptCancelFragment = new AcceptCancelFragment();
        getSupportFragmentManager().beginTransaction().add(R.id.controllerFragment, acceptCancelFragment).commit();
    }

    private void completedFragment() {
        Utility.showingLog("Completed", "fragment");
        CompleteOrderFragment completeOrderFragment = new CompleteOrderFragment();
        getSupportFragmentManager().beginTransaction().replace(R.id.controllerFragment, completeOrderFragment).commit();
    }
    /*private void startingTimer() {
        final Handler mainHandler = new Handler(Looper.getMainLooper());
        ScheduledFuture<?> updateFuture = Executors.newSingleThreadScheduledExecutor().scheduleAtFixedRate(new Runnable() {
            @Override
            public void run() {
                final int firstVisible = linearLayoutManager.findFirstVisibleItemPosition();
                final int lastVisible = linearLayoutManager.findLastVisibleItemPosition();
                mainHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        myRecyclerViewAdapter.notifyItemRangeChanged(firstVisible, lastVisible - firstVisible + 1);
                    }
                });
            }
        }, 0, 1000, TimeUnit.MILLISECONDS);
    }*/


    private void settingNotification() {

        broadcastReceiver = new MyReceiver1();


        if (Utility.isInternetConnected(getApplicationContext()))
            new ABC().execute();

        try {
//            Utility.showingLog("USERID_LOGIN r", Utility.getDeviceKey(MainActivity.this) + "hello" + Preference.getUserIdCommon());
            Utility.showingLog("REGISTRATION ID..", Preference.getPushToken());
            Utility.showingLog("DEVIDE ID..", Preference.getDeviceId());
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onBackPressed() {
//        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            if (Preference.getAcceptedOrderStatusValue().equalsIgnoreCase(""))
                super.onBackPressed();
            else {
                if (Preference.getFrameVisibilityStatus()) {
                    acceptCancelFragment.frameLayout.setVisibility(View.GONE);
                    Preference.setFrameVisibilityStatus(false);
                    acceptCancelFragment.enableDisableItems(true);
                } else
                    finish();
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main2, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        //  int id = item.getItemId();
        //noinspection SimplifiableIfStatement
       /* if (id == R.id.action_settings) {
            return true;
        }*/
        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        if (id == R.id.accept_order) {
            if (Preference.getAcceptedOrderStatusValue().equalsIgnoreCase("")) {
                title.setText(getString(R.string.accept_order));
                activeDeactive.setVisibility(View.VISIBLE);
                filtering.setVisibility(View.GONE);
                acceptFragmentStart();
            } else {
                title.setText(getString(R.string.accept_order));
                activeDeactive.setVisibility(View.VISIBLE);
                filtering.setVisibility(View.GONE);
                acceptCancelFragmentStart();
            }
// Utility.showToast(MainActivity.this,"Order Already in progress, you can't see any non accepted orders");
           /* if(Preference.getAcceptedOrderStatusValue().equalsIgnoreCase(""))
                Utility.showingLog("main activity","no need to open");
            else
                acceptCancelFragmentStart();*/
        } else if (id == R.id.complete_order) {
/*
            if(Preference.getAcceptedOrderStatusValue().equalsIgnoreCase("")) {
*/
            title.setText(getString(R.string.completed_orders));
            activeDeactive.setVisibility(View.GONE);
            filtering.setVisibility(View.VISIBLE);
            /*}
            else
            Utility.showingLog("MainActivity","Order already in progress");*/
            completedFragment();
        }/* else if (id == R.id.setting) {
            startActivity(new Intent(getApplicationContext(), SettingActivity.class));
        }*/
        else if(id == R.id.change_password){
            startActivity(new Intent(MainActivity.this,ChangePasswordActivity.class));
        }else if (id == R.id.logout) {
            showLogoutAlert();
        }
//        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void showLogoutAlert() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(this, R.style.AppCompatAlertDialogStyle));

        // set dialog message
        alertDialogBuilder
                .setTitle("Logout")
                .setMessage("Are you sure, you want to logout ?")
                .setCancelable(true)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                        makeLogoutRequest();
                    }
                });
        alertDialogBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();

            }
        });
        AlertDialog alert = alertDialogBuilder.create();
        alert.show();
        Button pbutton = alert.getButton(DialogInterface.BUTTON_POSITIVE);
        pbutton.setTextColor(getResources().getColor(R.color.blue));
        Button nbutton = alert.getButton(DialogInterface.BUTTON_NEGATIVE);
        nbutton.setTextColor(getResources().getColor(R.color.blue));
    }

    public void clearDatabase() {
        SQLiteDatabase db = DatabaseManager.getInstance().getWritableDatabase();

        try {
            for (String table : getResources().getStringArray(R.array.tablesCome)) {
                db.delete(table, null, null);
            }


        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void selectedFragment(AcceptCancelFragment acceptCancelFragment) {
        this.acceptCancelFragment = acceptCancelFragment;
    }

    @Override
    public void selectedFragment(CompleteOrderFragment completeOrderFragment) {
        this.completeOrderFragment = completeOrderFragment;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.e("on createmsg", "destroy");
        try {
            unregisterReceiver(broadcastReceiver);
            Intent intent = new Intent(this, FloatingViewService.class);
            stopService(intent);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.e("on create", "resume");
        try {
            int count = NotificationTable.getInstance().getActivecount();
            // System.out.println(Notification_Table.getInstance().getActivecount() + "=totalactivecount");
            Preference.setBadgeCount(count);
        } catch (Exception e) {
            e.printStackTrace();
        }


        try {
//            signupBean = SignUpTable.getInstance().getSignUpDetail(Preference.getUserIdCommon());
            userData = Preference.getUserData();
            username.setText(userData.getFirst_name());
            emailId.setText(userData.getEmail());
            Glide.with(MainActivity.this).
                    setDefaultRequestOptions(requestOptions).
                    load(userData.getProfileImage()).
                    into(civProfileImage);

//            navigationView.addOnLayoutChangeListener(new View.OnLayoutChangeListener() {
//                @Override
//                public void onLayoutChange(View view, int i, int i1, int i2, int i3, int i4, int i5, int i6, int i7) {
//
//                    navigationView.removeOnLayoutChangeListener(this);
//
//                    TextView username = navigationView.findViewById(R.id.home_user_name);
//                    TextView emailId = navigationView.findViewById(R.id.home_user_mail_id);
//                    CircleImageView civProfileImage = navigationView.findViewById(R.id.home_user_pic);
//
//                    username.setText(userData.getFirst_name());
//                    emailId.setText(userData.getEmail());
//
//                    Glide.with(MainActivity.this).
//                            setDefaultRequestOptions(requestOptions).
//                            load(userData.getProfileImage()).
//                            into(civProfileImage);
//
//                    navHeader = navigationView.findViewById(R.id.navHeader);
//
//                    navHeader.setOnClickListener(new View.OnClickListener() {
//                        @Override
//                        public void onClick(View v) {
//                            startActivity(new Intent(MainActivity.this, ProfileActivity.class));
//
//                        }
//                    });
//
//                }
//            });
        } catch (Exception e) {
            e.printStackTrace();
        }
        registerReceiver(broadcastReceiver, new IntentFilter(MyReceiver1.ACTION_RESP12));

        if (Preference.getActiveDeactiveStatus())
            activeDeactive.setBackgroundResource(R.drawable.deactive);
        else
            activeDeactive.setBackgroundResource(R.drawable.active);


    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.e("on create", "start");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.e("on create", "restart");

    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.e("on create", "pause");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.e("on create", "stop");
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.navHeader:
            case R.id.home_user_pic:
                startActivity(new Intent(MainActivity.this, ProfileActivity.class));
                drawer.closeDrawer(GravityCompat.START);
                break;
        }
    }

    public class ABC extends AsyncTask<String, String, String> {


        @Override
        protected String doInBackground(String... params) {
            // TODO Auto-generated method stub
          /*  try {
                GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(MainActivity.this);
                String regID = gcm.register("851658446642");
                Log.d("REGISTRATION ID...12...", regID);
                Preference.setPushToken(regID);


            } catch (Exception e) {
                e.printStackTrace();
            }
*/
            return null;
        }
    }

    public class MyReceiver1 extends BroadcastReceiver {
        public static final String ACTION_RESP12 =
                "com.truckerchamp.intent.action.MESSAGE_count";

        @Override
        public void onReceive(Context context, Intent intent) {
            Toast.makeText(context, "Intent Detected.", Toast.LENGTH_LONG).show();

            System.out.println("trip search fragment");
        }
    }

    private void makeLogoutRequest() {
        LogoutRequest request = new LogoutRequest(Preference.getUserIdCommon());
        Gson gson = new GsonBuilder().create();
        String requestJson = gson.toJson(request);
        callLogutApi(requestJson);

    }

    private void callLogutApi(String requestJson) {
        pDialog = new MyCustomProgressDialog(MainActivity.this, R.style.MyTheme);
        //   pDialog.setMessage("Please wait...");
        pDialog.setIndeterminate(true);
        pDialog.setCancelable(false);
        pDialog.show();
        String url = Constant.LOGOUT;
        Utility.showingLog("url", "" + url + "request" + requestJson);
        GsonRequest gsonRequest = new GsonRequest<LogoutResponse>(Request.Method.POST, url,
                LogoutResponse.class, requestJson, new Response.Listener<LogoutResponse>() {

            @Override
            public void onResponse(LogoutResponse loginResponseData) {
                boolean status = loginResponseData.getSuccess();

                if (status) {
                    pDialog.dismiss();

                    Preference.clearPreferences();
                    clearDatabase();
                    Intent intent = new Intent(getApplicationContext(), Login.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    finish();
                } else {
                    Utility.showToast(MainActivity.this, loginResponseData.getMessage());
                    Preference.setIsLogin(false);
                    pDialog.dismiss();
                }

            }
        },

                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        pDialog.dismiss();
                        if (volleyError != null) {
                            if (volleyError instanceof NetworkError)
                                Utility.showToast(MainActivity.this, getResources().getString(R.string.slow_net));
                            else if (volleyError.getMessage() == null)
                                Utility.showToast(MainActivity.this, getResources().getString(R.string.network_error));
                            else
                                Utility.showToast(MainActivity.this, volleyError.getMessage());
                            Preference.setIsLogin(false);

                        }
                    }
                });
        DefaultRetryPolicy policy = new DefaultRetryPolicy(2 * 60 * 1000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        gsonRequest.setRetryPolicy(policy);
        AppController.getInstance().addToRequestQueue(gsonRequest);
    }


}
