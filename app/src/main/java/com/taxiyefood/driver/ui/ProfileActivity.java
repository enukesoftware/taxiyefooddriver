package com.taxiyefood.driver.ui;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.Settings;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.view.ContextThemeWrapper;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.FileProvider;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.material.textfield.TextInputLayout;
import com.google.gson.Gson;
import com.taxiyefood.driver.BuildConfig;
import com.taxiyefood.driver.R;
import com.taxiyefood.driver.cropper.CropImage;
import com.taxiyefood.driver.data.AppController;
import com.taxiyefood.driver.data.SignUpTable;
import com.taxiyefood.driver.databean.SignupBean;
import com.taxiyefood.driver.multipart.VolleyMultipartRequest;
import com.taxiyefood.driver.serverrequest.LoginResponseData;
import com.taxiyefood.driver.serverrequest.LoginResponseDataValue;
import com.taxiyefood.driver.utils.Constant;
import com.taxiyefood.driver.utils.ImageUtils;
import com.taxiyefood.driver.utils.MyCustomProgressDialog;
import com.taxiyefood.driver.utils.Preference;
import com.taxiyefood.driver.utils.Utility;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import de.hdodenhof.circleimageview.CircleImageView;


public class ProfileActivity extends AppCompatActivity implements View.OnClickListener {

    String readExternalPermission = Manifest.permission.WRITE_EXTERNAL_STORAGE;
    String writeExternalPermission = Manifest.permission.READ_EXTERNAL_STORAGE;
    String Camera_permission = Manifest.permission.CAMERA;
    public static final int COUNTRY_REQUREST_CODE = 101;
    final static int OPEN_CAMERA = 102;
    final static int ACCESS_CAMERA = 104;
    final static int ACCESS_GALLERY = 105;
    final static int OPEN_GALLERY = 103;

    Toolbar toolbar;
    Menu menu;
    LoginResponseDataValue userData;
    LoginResponseDataValue userDataUpdated;
    CircleImageView civProfileImage;
    LinearLayout llUploadImage;
    TextInputLayout tilFirstName;
    TextInputLayout tilLastName;
    TextInputLayout tilEmail;
    TextInputLayout tilContactNumber;
    TextInputLayout tilCountryCode;
    EditText etCountryCode;
    EditText etFirstName;
    EditText etLastName;
    EditText etEmail;
    EditText etContactNumber;
    Button btnUpdateProfile;
    String TMP_FILE;
    AlertDialog alertDialog;
    File file;
    String imagePath = null;
    Uri imageUri;
    String profileImage = "";
    Uri profileUri;
    RequestOptions requestOptions;
    private MyCustomProgressDialog pDialog;


    //    private RequestQueue rQueue;
    private ArrayList<HashMap<String, String>> arraylist;
    Bitmap imageBitmap = null;
    boolean isEditable = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        settingToolbar();
        getUserData();
        initializeView();
    }

    private void getUserData() {
        userData = Preference.getUserData();
        userDataUpdated = Preference.getUserData();

        Log.e("USER_DATA:", new Gson().toJson(userData));
    }

    private void settingToolbar() {
        toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle(getString(R.string.profile));
        toolbar.setTitleTextColor(getResources().getColor(R.color.white));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        final Drawable upArrow = getResources().getDrawable(R.drawable.abc_ic_ab_back_mtrl_am_alpha);
        upArrow.setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.SRC_ATOP);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
        if (isEditable) {
            editProfileCancel();
        } else {
            finish();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        this.menu = menu;
        getMenuInflater().inflate(R.menu.profile_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == R.id.action_edit) {
            editProfile();
            return true;
        }

        if (id == R.id.action_cancel) {
            editProfileCancel();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void editProfile() {
        if (menu != null) {
            menu.findItem(R.id.action_edit).setVisible(false);
            menu.findItem(R.id.action_cancel).setVisible(true);
        }

        isEditable = true;
        btnUpdateProfile.setVisibility((View.VISIBLE));
        llUploadImage.setVisibility(View.VISIBLE);
        etFirstName.setEnabled(true);
        etLastName.setEnabled(true);
        etEmail.setEnabled(true);
        etCountryCode.setEnabled(true);
        etContactNumber.setEnabled(true);
    }

    private void editProfileCancel() {
        if (menu != null) {
            menu.findItem(R.id.action_cancel).setVisible(false);
            menu.findItem(R.id.action_edit).setVisible(true);
        }

        isEditable = false;
        btnUpdateProfile.setVisibility((View.GONE));
        llUploadImage.setVisibility(View.GONE);
        etFirstName.setEnabled(false);
        etLastName.setEnabled(false);
        etEmail.setEnabled(false);
        etCountryCode.setEnabled(false);
        etContactNumber.setEnabled(false);

        etFirstName.setText(userData.getFirst_name());
        etLastName.setText(userData.getLast_name());
        etEmail.setText(userData.getEmail());
        etCountryCode.setText(userData.getCountryCode());
        etContactNumber.setText(userData.getContact());

        Glide.with(this).
                setDefaultRequestOptions(requestOptions).
                load(userData.getProfileImage()).
                into(civProfileImage);

        clearImageDataOnCancel();
    }

    private void profileUpdated(LoginResponseData loginResponseData) {
        userData = loginResponseData.getLoginResponseDataValue();
        userDataUpdated = loginResponseData.getLoginResponseDataValue();
        editProfileCancel();
    }

    private void clearImageDataOnCancel() {
        file = null;
        imagePath = null;
        imageUri = null;
        profileImage = "";
        profileUri = null;
        imageBitmap = null;
    }

    private void initializeView() {
        civProfileImage = findViewById(R.id.civProfileImage);
        llUploadImage = findViewById(R.id.llUploadImage);
        tilFirstName = findViewById(R.id.tilFirstName);
        tilLastName = findViewById(R.id.tilLastName);
        tilEmail = findViewById(R.id.tilEmail);
        tilContactNumber = findViewById(R.id.tilContactNumber);
        etFirstName = findViewById(R.id.etFistName);
        etLastName = findViewById(R.id.etLastName);
        etEmail = findViewById(R.id.etEmail);
        etContactNumber = findViewById(R.id.etContactNumber);
        btnUpdateProfile = findViewById(R.id.btnUpdateProfile);
        tilCountryCode = findViewById(R.id.tilCountryCode);
        etCountryCode = findViewById(R.id.etCountryCode);
        etCountryCode.setText(Constant.DEFAULT_COUNTRY_CODE);
        etCountryCode.setFocusable(false);
        etCountryCode.setOnClickListener(this);
        tilCountryCode.setOnClickListener(this);
        btnUpdateProfile.setOnClickListener(this);
        llUploadImage.setOnClickListener(this);

        removeError();

        requestOptions = new RequestOptions()
                .placeholder(R.drawable.default_avatar)
                .error(R.drawable.default_avatar);

        editProfileCancel();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.llUploadImage:
                showAttachmentsDialog();
                break;
            case R.id.etCountryCode:
            case R.id.tilCountryCode:
                if (isEditable) {
                    Utility.hideKeyboard(this);
                    startActivityForResult(new Intent(ProfileActivity.this, SearchCountryActivity.class), COUNTRY_REQUREST_CODE);

                }
                break;
            case R.id.btnUpdateProfile:
                if (isValid()) {
                    if (Utility.isInternetConnected(ProfileActivity.this)) {
                        callProfileUpdateApi();
                    } else {
                        Utility.showToast(ProfileActivity.this, getString(R.string.no_internet));
                    }
                }
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);


        switch (requestCode) {
            case COUNTRY_REQUREST_CODE:
                if (resultCode == RESULT_OK) {
                    String countryCode = data.getExtras().getString("countryCode");
                    etCountryCode.setText(countryCode);
                }
                break;
            case OPEN_CAMERA:
                if (resultCode == RESULT_OK) {
                    final File file = new File(Environment.getExternalStorageDirectory(), TMP_FILE + ".jpg");
                    Uri uri = FileProvider.getUriForFile(ProfileActivity.this, ProfileActivity.this.getApplicationContext().getPackageName() + ".provider", file);
                    CropImage.activity(uri).start(ProfileActivity.this);
                }
                break;
            case OPEN_GALLERY:
                if (resultCode == RESULT_OK) {
                    if (data == null) {
                        Toast.makeText(ProfileActivity.this, "Something went wrong!", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    Uri uri = data.getData();
                    CropImage.activity(uri).start(ProfileActivity.this);
                }
                break;
            case CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE:
                CropImage.ActivityResult result = CropImage.getActivityResult(data);
                if (resultCode == RESULT_OK) {
                    Uri resultUri = result.getUri();
                    imagePath = ImageUtils.getFilePath(ProfileActivity.this, resultUri);
                    this.imagePath = ImageUtils.saveImageToFile(imagePath);
                    this.imageUri = resultUri;
                    profileImage = imagePath;
                    profileUri = resultUri;

                    Glide.with(this).
                            setDefaultRequestOptions(requestOptions).
                            load(profileImage).
                            into(civProfileImage);

                    imageBitmap = BitmapFactory.decodeFile(profileImage);


                } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                    Exception error = result.getError();
                }

                break;
        }
    }

    /**
     * Open Dialog for image option
     */
    void showAttachmentsDialog() {
        Utility.hideKeyboard(this);
        final AlertDialog.Builder builder = new AlertDialog.Builder(new ContextThemeWrapper(this, R.style.ProfileDialogTheme));
        builder.setTitle(getString(R.string.select_option));
        ArrayList<String> options = new ArrayList<>();
        options.add(getString(R.string.from_camera));
        options.add(getString(R.string.from_gallery));
//        if (!isProfileImage)
//            options.add("Select Text File");
        options.add(getString(R.string.cancel));
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, R.layout.custom_simple_list_item_for_dialog, options);
        builder.setAdapter(adapter, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                switch (i) {
                    case 0:
                        alertDialog.dismiss();
                        if (!Utility.isPermissionGranted(ProfileActivity.this, new String[]{Camera_permission, writeExternalPermission})) {
                            Utility.RequestPermission(ProfileActivity.this, new String[]{Camera_permission, writeExternalPermission}, ACCESS_CAMERA);
                        } else {
                            openCamera();
                        }
                        break;
                    case 1:
                        alertDialog.dismiss();
                        if (!Utility.isPermissionGranted(ProfileActivity.this, new String[]{readExternalPermission})) {
                            Utility.RequestPermission(ProfileActivity.this, new String[]{readExternalPermission}, ACCESS_GALLERY);
                        } else {
                            openGallery();
                        }
                        break;
                    case 2:
                        alertDialog.dismiss();
                        break;
                }
            }
        });

        alertDialog = builder.create();
        alertDialog.show();
        //builder.show();
    }

    /**
     * Open camera to click a picture
     */
    void openCamera() {

        UUID idOne = UUID.randomUUID();
        TMP_FILE = idOne.toString();
        file = new File(Environment
                .getExternalStorageDirectory(), TMP_FILE + ".jpg");

        Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
        intent.putExtra(MediaStore.EXTRA_OUTPUT, FileProvider.getUriForFile(this,
                BuildConfig.APPLICATION_ID + ".provider",
                file));
//        intent.putExtra("return-data", true);
        startActivityForResult(intent, OPEN_CAMERA);

    }

    /**
     * Open file manger to choose a file.
     */
    void openGallery() {
        Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(intent, OPEN_GALLERY);
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {

        for (String permission : permissions) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(ProfileActivity.this, permission)) {
                //denied
                Log.e("denied", permission);
            } else {
                if (ActivityCompat.checkSelfPermission(ProfileActivity.this, permission) == PackageManager.PERMISSION_GRANTED) {
                    //allowed
                    Log.e("allowed", permission);
                    if (requestCode == ACCESS_CAMERA) {
                        if (permission.equalsIgnoreCase(Camera_permission)) {
                            onSelectImageClick(requestCode, permissions, grantResults);
                            break;
                        }
                    } else {
                        onSelectImageClick(requestCode, permissions, grantResults);
                    }
                } else {
                    //set to never ask again
                    Log.e("set to never ask again", permission);
                    showAlertForCameraStorage();                    //do something here.
                }
            }
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }


    private void onSelectImageClick(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case ACCESS_CAMERA:
                for (int i = 0; i < permissions.length; i++) {
                    if (permissions[i].equalsIgnoreCase(Camera_permission)) {
                        if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {
                            openCamera();
                        }
                    }
                }
                break;
            case ACCESS_GALLERY:
                for (int i = 0; i < permissions.length; i++) {
                    if (permissions[i].equalsIgnoreCase(readExternalPermission)) {
                        if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {
                            openGallery();
                        }
                    }
                }
                break;
        }
    }

    /**
     * alert dialog when camera and storage is permanently denied
     */
    private void showAlertForCameraStorage() {
        final AlertDialog.Builder dialog = new AlertDialog.Builder(ProfileActivity.this);
        dialog.setTitle("Enable Permission")
                .setMessage("Please provide Camera and storage permission in permission section of app info for uploading image")
                .setPositiveButton("Provide Permission", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        openSettings();
                    }
                });
        dialog.show();
    }

    /**
     * Opens settings screen for providing permission
     */
    private void openSettings() {
        Intent intent = new Intent();
        intent.setAction(
                Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts("package",
                BuildConfig.APPLICATION_ID, null);
        intent.setData(uri);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    private boolean isValid() {

        if (TextUtils.isEmpty(etFirstName.getText().toString().trim())) {
            etFirstName.requestFocus();
            tilFirstName.setErrorEnabled(true);
            tilFirstName.setError(getString(R.string.error_first_name));
            return false;
        } else {
            tilFirstName.setError(null);
        }

        if (TextUtils.isEmpty(etLastName.getText().toString().trim())) {
            etLastName.requestFocus();
            tilLastName.setErrorEnabled(true);
            tilLastName.setError(getString(R.string.error_last_name));
            return false;
        } else {
            tilLastName.setError(null);
        }

        if (!validateEmail()) {
            return false;
        }

        if (TextUtils.isEmpty(etContactNumber.getText().toString().trim()) || etContactNumber.getText().toString().length() < 10) {
            etContactNumber.requestFocus();
            tilContactNumber.setErrorEnabled(true);
            tilContactNumber.setError(getString(R.string.error_contact_number));
            return false;
        } else {
            tilContactNumber.setError(null);
        }

        return true;
    }

    private boolean validateEmail() {
        if (TextUtils.isEmpty(etEmail.getText().toString().trim())) {
            tilEmail.setErrorEnabled(true);
            tilEmail.setError(getString(R.string.error_email_empty));
            etEmail.requestFocus();
            return false;
        } else {
            tilEmail.setErrorEnabled(false);
        }

        if (!isValidEmail(etEmail.getText().toString().trim())) {
            tilEmail.setErrorEnabled(true);
            tilEmail.setError(getString(R.string.error_email_incorrect));
            etEmail.requestFocus();
            return false;
        } else {
            tilEmail.setErrorEnabled(false);
        }
        return true;
    }

    // validating email id
    private boolean isValidEmail(String email) {
        String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

        Pattern pattern = Pattern.compile(EMAIL_PATTERN);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }

    private void callProfileUpdateApi() {
        pDialog = new MyCustomProgressDialog(ProfileActivity.this, R.style.MyTheme);
        //   pDialog.setMessage("Please wait...");
        pDialog.setIndeterminate(true);
        pDialog.setCancelable(false);
        pDialog.show();
        VolleyMultipartRequest volleyMultipartRequest = new VolleyMultipartRequest(Request.Method.POST, Constant.UPDATE_PROFILE,
                new Response.Listener<NetworkResponse>() {
                    @Override
                    public void onResponse(NetworkResponse response) {
//                        Log.d("ressssssoo", new String(response.data));
//                        rQueue.getCache().clear();
                        try {
                            pDialog.dismiss();
                            LoginResponseData loginResponseData = new Gson().fromJson(new String(response.data), LoginResponseData.class);

                            Log.e("PROFILE_RES:", new Gson().toJson(loginResponseData));
                            if (loginResponseData.isStatus()) {
                                String userid = String.valueOf(loginResponseData.getLoginResponseDataValue().getUser_id());
                                Preference.setUserIdCommon(userid);
                                pDialog.dismiss();
                                try {

                                    String mobcont = loginResponseData.getLoginResponseDataValue().getContact();
                                    String email = loginResponseData.getLoginResponseDataValue().getEmail();
                                    String firstName = loginResponseData.getLoginResponseDataValue().getFirst_name();
                                    String lastName = loginResponseData.getLoginResponseDataValue().getLast_name();

                                    long insertRow = SignUpTable.getInstance().add(new SignupBean(userid, email, mobcont, firstName + " " + lastName));

                                    Utility.showingLog("log inserted", "" + insertRow);

                                    Preference.setUserData(loginResponseData.getLoginResponseDataValue());
                                    Utility.showToast(ProfileActivity.this, loginResponseData.getMessage());
                                    profileUpdated(loginResponseData);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            } else {
                                Utility.showToast(ProfileActivity.this, loginResponseData.getMessage());
                                pDialog.dismiss();
                            }
//                            JSONObject jsonObject = new JSONObject(new String(response.data));

//                            Toast.makeText(getApplicationContext(), jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
//
//                            jsonObject.toString().replace("\\\\", "");
//
//                            if (jsonObject.getString("status").equals("true")) {
//
//                                arraylist = new ArrayList<HashMap<String, String>>();
//                                JSONArray dataArray = jsonObject.getJSONArray("data");
//
//                                String url = "";
//                                for (int i = 0; i < dataArray.length(); i++) {
//                                    JSONObject dataobj = dataArray.getJSONObject(i);
//                                    url = dataobj.optString("pathToFile");
//                                }
////                                Picasso.get().load(url).into(imageView);
//                            }
                        } catch (Exception e) {
                            pDialog.dismiss();
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        pDialog.dismiss();
                        if (volleyError != null) {
                            if (volleyError instanceof NetworkError)
                                Utility.showToast(ProfileActivity.this, getResources().getString(R.string.slow_net));
                            else if (volleyError.getMessage() == null)
                                Utility.showToast(ProfileActivity.this, getResources().getString(R.string.network_error));
                            else
                                Utility.showToast(ProfileActivity.this, volleyError.getMessage());

                        }
                    }
                }) {

            /*
             * If you want to add more parameters with the image
             * you can do it here
             * here we have only one parameter with the image
             * which is tags
             * */
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("id", userData.getUser_id());
                params.put("first_name", etFirstName.getText().toString());
                params.put("last_name", etLastName.getText().toString());
                params.put("contact_number", etContactNumber.getText().toString());
                params.put("email", etEmail.getText().toString());
                params.put("countrycode", etCountryCode.getText().toString());
                return params;

            }

            /*
             *pass files using below method
             * */
            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();
                long imagename = System.currentTimeMillis();
                if (imageBitmap != null) {
                    params.put("image", new DataPart(userData.getUser_id() + "_" + imagename + ".png", getFileDataFromDrawable(imageBitmap)));
                }
                return params;
            }
        };


        volleyMultipartRequest.setRetryPolicy(new DefaultRetryPolicy(
                0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
//        rQueue = Volley.newRequestQueue(ProfileActivity.this);
//        rQueue.add(volleyMultipartRequest);

        DefaultRetryPolicy policy = new DefaultRetryPolicy(2 * 60 * 1000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        volleyMultipartRequest.setRetryPolicy(policy);
        AppController.getInstance().addToRequestQueue(volleyMultipartRequest);
    }

    public byte[] getFileDataFromDrawable(Bitmap bitmap) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 80, byteArrayOutputStream);
        return byteArrayOutputStream.toByteArray();
    }

    private void removeError() {

        etFirstName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
                if (!TextUtils.isEmpty(charSequence.toString()))
                    tilFirstName.setErrorEnabled(false);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        etLastName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
                if (!TextUtils.isEmpty(charSequence.toString()))
                    tilLastName.setErrorEnabled(false);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        etEmail.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
                if (!TextUtils.isEmpty(charSequence.toString()))
                    tilEmail.setErrorEnabled(false);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        etContactNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
                if (!TextUtils.isEmpty(charSequence.toString()))
                    tilContactNumber.setErrorEnabled(false);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }
}
