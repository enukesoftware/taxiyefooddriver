package com.taxiyefood.driver.ui;


import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.ToggleButton;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.taxiyefood.driver.R;
import com.taxiyefood.driver.utils.Utility;


/**
 * Created by enuke on 19/5/16.
 */
public class SettingActivity extends AppCompatActivity {

    Toolbar toolbar;
    ToggleButton settingToggle;
    Spinner langDetails;
    String langs[] = {"Select Language","English","French"};
    ArrayAdapter<String> langAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        settingToolbar();

        gettingLayoutId();
    }

    private void gettingLayoutId() {
        settingToggle = findViewById(R.id.settingToggle);
        langDetails = findViewById(R.id.langDetails);

        langAdapter = new ArrayAdapter<String>(getApplicationContext(), R.layout.custom_layout, R.id.customtextView, langs);
        langDetails.setAdapter(langAdapter);
        langDetails.setSelection(0);

        langDetails.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent,
                                       View view, int position, long id) {
                // TODO Auto-generated method stub
               Utility.showingLog("Selected Item",""+langs[position]);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub

            }
        });


    }

    private void settingToolbar() {
        toolbar = findViewById(R.id.settings_toolbar);
        toolbar.setTitle("Settings");
        toolbar.setTitleTextColor(getResources().getColor(R.color.white));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        final Drawable upArrow = getResources().getDrawable(R.drawable.abc_ic_ab_back_mtrl_am_alpha);
        upArrow.setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.SRC_ATOP);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}