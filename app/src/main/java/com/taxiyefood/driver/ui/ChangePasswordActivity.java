package com.taxiyefood.driver.ui;

import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.android.material.textfield.TextInputLayout;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.taxiyefood.driver.R;
import com.taxiyefood.driver.data.AppController;
import com.taxiyefood.driver.serverrequest.ChangePasswordRequest;
import com.taxiyefood.driver.serverrequest.LoginResponseDataValue;
import com.taxiyefood.driver.serverrequest.LogoutResponse;
import com.taxiyefood.driver.utils.Constant;
import com.taxiyefood.driver.utils.GsonRequest;
import com.taxiyefood.driver.utils.MyCustomProgressDialog;
import com.taxiyefood.driver.utils.Preference;
import com.taxiyefood.driver.utils.Utility;

public class ChangePasswordActivity extends AppCompatActivity implements View.OnClickListener {

    Toolbar toolbar;
    TextInputLayout tilOldPassword;
    TextInputLayout tilNewPassword;
    TextInputLayout tilConfirmPassword;
    EditText etOldPassword;
    EditText etNewPassword;
    EditText etConfirmPassword;
    Button btnChangePassword;

    LoginResponseDataValue userData;
    private MyCustomProgressDialog pDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);
        settingToolbar();
        getUserData();
        initializeView();
        removeError();
    }

    private void settingToolbar() {
        toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle(getString(R.string.change_password));
        toolbar.setTitleTextColor(getResources().getColor(R.color.white));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        final Drawable upArrow = getResources().getDrawable(R.drawable.abc_ic_ab_back_mtrl_am_alpha);
        upArrow.setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.SRC_ATOP);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    private void getUserData() {
        userData = Preference.getUserData();

        Log.e("USER_DATA:", new Gson().toJson(userData));
    }

    private void initializeView() {
        tilOldPassword = findViewById(R.id.tilOldPassword);
        tilNewPassword = findViewById(R.id.tilNewPassword);
        tilConfirmPassword = findViewById(R.id.tilConfirmPassword);
        etOldPassword = findViewById(R.id.etOldPassword);
        etNewPassword = findViewById(R.id.etNewPassword);
        etConfirmPassword = findViewById(R.id.etConfirmPassword);
        btnChangePassword = findViewById(R.id.btnChangePassword);

        btnChangePassword.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnChangePassword:
                if (isValid()) {
                    if (Utility.isInternetConnected(ChangePasswordActivity.this)) {
                        makeRequest();
                    } else {
                        Utility.showToast(ChangePasswordActivity.this, getString(R.string.no_internet));
                    }
                }
                break;
        }
    }

    private boolean isValid() {
        if (TextUtils.isEmpty(etOldPassword.getText().toString().trim()) || etOldPassword.getText().toString().length() < 6) {
            etOldPassword.requestFocus();
            tilOldPassword.setErrorEnabled(true);
            tilOldPassword.setError(getString(R.string.error_password));
            return false;
        } else {
            tilOldPassword.setError(null);
        }

        if (TextUtils.isEmpty(etNewPassword.getText().toString().trim()) || etNewPassword.getText().toString().length() < 6) {
            etNewPassword.requestFocus();
            tilNewPassword.setErrorEnabled(true);
            tilNewPassword.setError(getString(R.string.error_password));
            return false;
        } else {
            tilNewPassword.setError(null);
        }

        if (TextUtils.isEmpty(etConfirmPassword.getText().toString().trim()) || !etNewPassword.getText().toString().trim().equals(etConfirmPassword.getText().toString().trim())) {
            etConfirmPassword.requestFocus();
            tilConfirmPassword.setErrorEnabled(true);
            tilConfirmPassword.setError(getString(R.string.error_new_password_not_match));
            return false;
        } else {
            tilConfirmPassword.setError(null);
        }

        return true;
    }

    private void removeError() {


        etOldPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
                if (!TextUtils.isEmpty(charSequence.toString()))
                    tilOldPassword.setErrorEnabled(false);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        etNewPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
                if (!TextUtils.isEmpty(charSequence.toString()))
                    tilNewPassword.setErrorEnabled(false);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        etConfirmPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
                if (!TextUtils.isEmpty(charSequence.toString()))
                    tilConfirmPassword.setErrorEnabled(false);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    private void makeRequest() {
        String userId = userData.getUser_id();
        String oldPassword = etOldPassword.getText().toString().trim();
        String newPassword = etNewPassword.getText().toString().trim();

        ChangePasswordRequest request = new ChangePasswordRequest(userId, oldPassword, newPassword);
        Gson gson = new GsonBuilder().create();
        String requestJson = gson.toJson(request);
        callChangePasswordApi(requestJson);

    }

    private void callChangePasswordApi(String requestJson) {
        pDialog = new MyCustomProgressDialog(ChangePasswordActivity.this, R.style.MyTheme);
        //   pDialog.setMessage("Please wait...");
        pDialog.setIndeterminate(true);
        pDialog.setCancelable(false);
        pDialog.show();
        String url = Constant.CHANGE_PASSWORD;
        Utility.showingLog("url", "" + url + "request" + requestJson);
        GsonRequest gsonRequest = new GsonRequest<LogoutResponse>(Request.Method.POST, url,
                LogoutResponse.class, requestJson, new Response.Listener<LogoutResponse>() {

            @Override
            public void onResponse(LogoutResponse response) {
                boolean status = response.getSuccess();

                if (status) {
                    pDialog.dismiss();
                    if (response.getMessage() != null) {
                        Utility.showToast(ChangePasswordActivity.this, response.getMessage());
                    }
                    finish();

                } else {
                    if (response.getMessage() != null) {
                        Utility.showToast(ChangePasswordActivity.this, response.getMessage());
                    }
                    pDialog.dismiss();
                }

            }
        },

                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        pDialog.dismiss();
                        if (volleyError != null) {
                            if (volleyError instanceof NetworkError)
                                Utility.showToast(ChangePasswordActivity.this, getResources().getString(R.string.slow_net));
                            else if (volleyError.getMessage() == null)
                                Utility.showToast(ChangePasswordActivity.this, getResources().getString(R.string.network_error));
                            else
                                Utility.showToast(ChangePasswordActivity.this, volleyError.getMessage());
                            Preference.setIsLogin(false);

                        }
                    }
                });
        DefaultRetryPolicy policy = new DefaultRetryPolicy(2 * 60 * 1000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        gsonRequest.setRetryPolicy(policy);
        AppController.getInstance().addToRequestQueue(gsonRequest);
    }
}
