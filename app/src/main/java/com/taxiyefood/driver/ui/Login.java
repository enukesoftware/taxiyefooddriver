package com.taxiyefood.driver.ui;


import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.material.textfield.TextInputLayout;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.taxiyefood.driver.R;
import com.taxiyefood.driver.data.AppController;
import com.taxiyefood.driver.data.SignUpTable;
import com.taxiyefood.driver.databean.SignupBean;
import com.taxiyefood.driver.serverrequest.LoginData;
import com.taxiyefood.driver.serverrequest.LoginResponseData;
import com.taxiyefood.driver.utils.Constant;
import com.taxiyefood.driver.utils.GsonRequest;
import com.taxiyefood.driver.utils.LocUpdate;
import com.taxiyefood.driver.utils.MyCustomProgressDialog;
import com.taxiyefood.driver.utils.Preference;
import com.taxiyefood.driver.utils.Utility;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * Created by enuke on 16/5/16.
 */
public class Login extends AppCompatActivity implements View.OnClickListener, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener {

    EditText emailId, password;
    RelativeLayout loginSubmit;
    private TextInputLayout emailLayout, passwordLayout;
    private MyCustomProgressDialog pDialog;
    private TextView tvRegister;
    private static final int PERMISSION_REQUEST_CODE = 1;
    public final static int REQUEST_CHECK_SETTINGS = 2;
    private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 1000;

    private Location mLastLocation;
    // Google client to interact with Google API
    private GoogleApiClient mGoogleApiClient;
    // boolean flag to toggle periodic location updates
    private boolean mRequestingLocationUpdates = true;
    private LocationRequest mLocationRequest;
    // Location updates intervals in sec
    private static int UPDATE_INTERVAL = 10000; // 10 sec
    private static int FATEST_INTERVAL = 5000; // 5 sec
    private static int DISPLACEMENT = 10; // 10 meters

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Log.i("devietoken", "onCreate: "+ FirebaseInstanceId.getInstance().getToken());
        if (Preference.getIsLogin()) {
            startActivity(new Intent(getApplicationContext(), MainActivity.class));
            finish();
        } else {
            setContentView(R.layout.login);
            gettingLayoutId();
            removeError();

            if (checkPlayServices()) {
                // Building the GoogleApi client
                buildGoogleApiClient();
                createLocationRequest();
            }

            checkBasicPermissions();


            if (mGoogleApiClient != null) {
                LocationSettingsRequest.Builder locationSettingsRequestBuilder = new LocationSettingsRequest.Builder()
                        .addLocationRequest(mLocationRequest);
                PendingResult<LocationSettingsResult> result =
                        LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, locationSettingsRequestBuilder.build());
                result.setResultCallback(mResultCallbackFromSettings);
                Utility.showingLog("builder", "" + locationSettingsRequestBuilder + "location request" + mLocationRequest);
                Utility.showingLog("result", "" + result);
            }

        }
    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(Login.this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API).build();
    }


    /**
     * Creating location request object
     */
    protected void createLocationRequest() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(UPDATE_INTERVAL);
        mLocationRequest.setFastestInterval(FATEST_INTERVAL);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setSmallestDisplacement(DISPLACEMENT);
    }


    // The callback for the management of the user settings regarding location
    private ResultCallback<LocationSettingsResult> mResultCallbackFromSettings = new ResultCallback<LocationSettingsResult>() {
        @Override
        public void onResult(LocationSettingsResult result) {
            final Status status = result.getStatus();
            //final LocationSettingsStates locationSettingsStates = result.getLocationSettingsStates();
            switch (status.getStatusCode()) {
                case LocationSettingsStatusCodes.SUCCESS:
                    // All location settings are satisfied. The client can initialize location
                    // requests here.
                    break;
                case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                    // Location settings are not satisfied. But could be fixed by showing the user
                    // a dialog.
                    try {
                        // Show the dialog by calling startResolutionForResult(),
                        // and check the result in onActivityResult().
                        status.startResolutionForResult(
                                Login.this,
                                REQUEST_CHECK_SETTINGS);
                    } catch (IntentSender.SendIntentException e) {
                        // Ignore the error.
                    }
                    break;
                case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                    Utility.showingErrorLog("Check In Timer", "Settings change unavailable. We have no way to fix the settings so we won't show the dialog.");
                    break;
            }
        }
    };

    private boolean checkPlayServices() {
        int resultCode = GooglePlayServicesUtil
                .isGooglePlayServicesAvailable(Login.this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                GooglePlayServicesUtil.getErrorDialog(resultCode, Login.this,
                        PLAY_SERVICES_RESOLUTION_REQUEST).show();
            } else {
                Utility.showToast(Login.this,
                        "This device is not supported.");
            }
            return false;
        }
        return true;
    }

    private void checkBasicPermissions() {
        int result1 = ContextCompat.checkSelfPermission(Login.this, Manifest.permission.ACCESS_FINE_LOCATION);
        int result2 = ContextCompat.checkSelfPermission(Login.this, Manifest.permission.READ_PHONE_STATE);

        List<String> permissions = new ArrayList<String>();

        if (result1 != PackageManager.PERMISSION_GRANTED) {
            permissions.add(Manifest.permission.ACCESS_FINE_LOCATION);
            permissions.add(Manifest.permission.ACCESS_COARSE_LOCATION);
        }

        if (result2 != PackageManager.PERMISSION_GRANTED) {
            permissions.add(Manifest.permission.READ_PHONE_STATE);
        }


        if (!permissions.isEmpty()) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(permissions.toArray(new String[permissions.size()]), PERMISSION_REQUEST_CODE);
            }
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:

                for (int i = 0; i < permissions.length; i++) {
                    if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {
                        Utility.showingLog("Permissions", "Permission Granted: " + permissions[i]);
                        if (permissions[i].equalsIgnoreCase("android.permission.ACCESS_COARSE_LOCATION")) {
                            try {
                                startService(new Intent(getApplicationContext(), LocUpdate.class));
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }

                    } else if (grantResults[i] == PackageManager.PERMISSION_DENIED) {
                        Utility.showingLog("Permissions", "Permission Denied: " + permissions[i]);

                    }
                }

                break;
            default: {
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
            }

        }
    }


    private void gettingLayoutId() {

        emailId = findViewById(R.id.loginEmail);
        password = findViewById(R.id.loginPassword);

        loginSubmit = findViewById(R.id.loginSubmit);

        emailLayout = findViewById(R.id.loginEmaillayout);
        passwordLayout = findViewById(R.id.loginpasswordlayout);

        Typeface type = Typeface.createFromAsset(getAssets(), "fonts/montserrat_light.ttf");
        emailId.setTypeface(type);
        password.setTypeface(type);

        tvRegister = findViewById(R.id.tvRegister);

        tvRegister.setOnClickListener(this);
        loginSubmit.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.loginSubmit:

                if (mGoogleApiClient != null) {
                    LocationSettingsRequest.Builder locationSettingsRequestBuilder = new LocationSettingsRequest.Builder()
                            .addLocationRequest(mLocationRequest);
                    PendingResult<LocationSettingsResult> result =
                            LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, locationSettingsRequestBuilder.build());
                    result.setResultCallback(mResultCallbackFromSettings);
                    Utility.showingLog("builder", "" + locationSettingsRequestBuilder + "location request" + mLocationRequest);
                    Utility.showingLog("result", "" + result);
                }

                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(password.getWindowToken(), 0);

                Utility.showingLog("login", "login click");
                if (Utility.isInternetConnected(Login.this)) {
                    Utility.showingLog("net connected", "login");
                    if (isValidRegistration())
                        makeLoginRequest(emailId.getText().toString(), password.getText().toString());
                } else {
                    Utility.showingLog("net not connected", "login");
                    Utility.showToast(Login.this, getString(R.string.no_internet));
                }
                break;

            case R.id.tvRegister:
                startActivity(new Intent(Login.this, RegisterActivity.class));
                finish();
                break;
            default:
                break;
        }
    }

    private boolean isValidRegistration() {
        if (!validateEmail())
            return false;
        return validatePassword();
    }


    private boolean validateEmail() {
        if (TextUtils.isEmpty(emailId.getText().toString())) {

            emailLayout.setErrorEnabled(true);
            emailLayout.setError("Please enter your Email");
            requestFocus(emailId);
            return false;
        } else {
            emailLayout.setErrorEnabled(false);
        }

        if (!isValidEmail(emailId.getText().toString())) {

            emailLayout.setErrorEnabled(true);
            emailLayout.setError("Please enter valid Email Id");
            requestFocus(emailId);
            return false;
        } else {
            emailLayout.setErrorEnabled(false);
        }
        return true;
    }

    // validating email id
    private boolean isValidEmail(String email) {
        String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

        Pattern pattern = Pattern.compile(EMAIL_PATTERN);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }

    private boolean validatePassword() {
        if (!isValidPassword(password.getText().toString())) {
            passwordLayout.setErrorEnabled(true);
            passwordLayout.setError("Password should contain atleast 6 Digits");
            requestFocus(password);
            return false;
        } else {
            passwordLayout.setErrorEnabled(false);
        }
        return true;
    }


    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }


    private void makeLoginRequest(String userName, String password) {
        LoginData loginData = new LoginData(userName, password);
        Gson gson = new GsonBuilder().create();
        String loginJson = gson.toJson(loginData);
        makeServerRequest(loginJson);

    }


    @Override
    public void onStart() {
        super.onStart();
        if (mGoogleApiClient != null) {
            mGoogleApiClient.connect();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mGoogleApiClient != null) {
            if (mGoogleApiClient.isConnected()) {
                mGoogleApiClient.disconnect();
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Utility.showingLog("in on activity result", "come" + requestCode);


        if (requestCode == REQUEST_CHECK_SETTINGS && resultCode == RESULT_OK) {
            if (mGoogleApiClient.isConnected()) {
                startLocationUpdates();
            }
        }
    }

    /**
     * Starting the location updates
     */
    protected void startLocationUpdates() {
        if (mLocationRequest != null) {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        checkPlayServices();
        // Resuming the periodic location updates
        if (mGoogleApiClient != null) {
            if (mGoogleApiClient.isConnected() && mRequestingLocationUpdates) {
                startLocationUpdates();
            }
        }
    }

    private void makeServerRequest(String loginJson) {
        pDialog = new MyCustomProgressDialog(Login.this, R.style.MyTheme);
        //   pDialog.setMessage("Please wait...");
        pDialog.setIndeterminate(true);
        pDialog.setCancelable(false);
        pDialog.show();
        String url = Constant.LOGIN_METHOD;
        Utility.showingLog("url", "" + url + "request" + loginJson);
        GsonRequest gsonRequest = new GsonRequest<LoginResponseData>(Request.Method.POST, url,
                LoginResponseData.class, loginJson, new Response.Listener<LoginResponseData>() {

            @Override
            public void onResponse(LoginResponseData loginResponseData) {
                boolean status = loginResponseData.isStatus();

                if (status) {
                    String userid = String.valueOf(loginResponseData.getLoginResponseDataValue().getUser_id());
                    Utility.showingLog("user id", "" + userid);
                    Preference.setUserIdCommon(userid);
                    pDialog.dismiss();
                    try {

                        String mobcont = loginResponseData.getLoginResponseDataValue().getContact();
                        String email = loginResponseData.getLoginResponseDataValue().getEmail();
                        String firstName = loginResponseData.getLoginResponseDataValue().getFirst_name();
                        String lastName = loginResponseData.getLoginResponseDataValue().getLast_name();

                        long insertRow = SignUpTable.getInstance().add(new SignupBean(userid, email, mobcont, firstName + " " + lastName));

                        Utility.showingLog("log inserted", "" + insertRow);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                    Preference.setUserData(loginResponseData.getLoginResponseDataValue());
                    Utility.showToast(Login.this, loginResponseData.getMessage());
                    Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                    startActivity(intent);
                    Preference.setIsLogin(true);
                    finish();

                } else {
                    Utility.showToast(Login.this, loginResponseData.getMessage());
                    Preference.setIsLogin(false);
                    pDialog.dismiss();
                }

            }
        },

                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        pDialog.dismiss();
                        if (volleyError != null) {
                            if (volleyError instanceof NetworkError)
                                Utility.showToast(Login.this, getResources().getString(R.string.slow_net));
                            else if (volleyError.getMessage() == null)
                                Utility.showToast(Login.this, getResources().getString(R.string.network_error));
                            else
                                Utility.showToast(Login.this, volleyError.getMessage());
                            Preference.setIsLogin(false);

                        }
                    }
                });
        DefaultRetryPolicy policy = new DefaultRetryPolicy(2 * 60 * 1000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        gsonRequest.setRetryPolicy(policy);
        AppController.getInstance().addToRequestQueue(gsonRequest);
    }

    // validating password with  password length
    private boolean isValidPassword(String pass) {
        return pass.length() >= 6;
    }

    private void removeError() {

        emailLayout.getEditText().addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
                if (!TextUtils.isEmpty(charSequence.toString()))
                    emailLayout.setErrorEnabled(false);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        passwordLayout.getEditText().addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
                if (!TextUtils.isEmpty(charSequence.toString()))
                    passwordLayout.setErrorEnabled(false);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        displayLocation();
        if (mRequestingLocationUpdates) {
            startLocationUpdates();
        }
    }

    @Override
    public void onConnectionSuspended(int i) {
        mGoogleApiClient.connect();
    }

    @Override
    public void onLocationChanged(Location location) {
        mLastLocation = location;
        // Displaying the new location on UI
        displayLocation();
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Utility.showingLog("Check In Timer", "Connection failed: ConnectionResult.getErrorCode() = "
                + connectionResult.getErrorCode());
    }

    /**
     * Method to display the location on UI
     */
    private void displayLocation() {
        if (mLastLocation != null && mGoogleApiClient != null) {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }
            mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
            double latitudeHere, longitudeHere;
            if (mLastLocation == null) {
                latitudeHere = 0.0;
                longitudeHere = 0.0;
                Utility.showingLog("lait here", "" + latitudeHere + "longi here" + longitudeHere);
            } else {
                latitudeHere = mLastLocation.getLatitude();
                longitudeHere = mLastLocation.getLongitude();
                Utility.showingLog("lait else", "" + latitudeHere + "longi else" + longitudeHere);

            }

            /*fusionLatitude =latitudeHere ;
            fusionLongitude = longitudeHere;*/


        } else {
//            SystemUtils.showToast(PersonalDetail.this, "Couldn't get the location. Make sure location is enabled on the device");
        }
    }
}
