package com.taxiyefood.driver.ui;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.android.material.textfield.TextInputLayout;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.taxiyefood.driver.R;
import com.taxiyefood.driver.data.AppController;
import com.taxiyefood.driver.serverrequest.LoginResponseData;
import com.taxiyefood.driver.serverrequest.SignUpRequest;
import com.taxiyefood.driver.utils.Constant;
import com.taxiyefood.driver.utils.GsonRequest;
import com.taxiyefood.driver.utils.MyCustomProgressDialog;
import com.taxiyefood.driver.utils.Preference;
import com.taxiyefood.driver.utils.Utility;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegisterActivity extends AppCompatActivity implements View.OnClickListener {
    public static final int COUNTRY_REQUREST_CODE = 101;

    TextInputLayout tilFirstName;
    TextInputLayout tilLastName;
    TextInputLayout tilEmail;
    TextInputLayout tilContactNumber;
    TextInputLayout tilCountryCode;
    TextInputLayout tilPassword;
    TextInputLayout tilConfirmPassword;
    EditText etCountryCode;
    EditText etFirstName;
    EditText etLastName;
    EditText etEmail;
    EditText etContactNumber;
    EditText etPassword;
    EditText etConfirmPassword;
    RelativeLayout rlSubmit;
    TextView tvLogin;
    private MyCustomProgressDialog pDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        initializeView();
        removeError();
    }

    private void initializeView() {
        tilFirstName = findViewById(R.id.tilFirstName);
        tilLastName = findViewById(R.id.tilLastName);
        tilEmail = findViewById(R.id.tilEmail);
        tilContactNumber = findViewById(R.id.tilContactNumber);
        tilPassword = findViewById(R.id.tilPassword);
        tilConfirmPassword = findViewById(R.id.tilConfirmPassword);
        etFirstName = findViewById(R.id.etFistName);
        etLastName = findViewById(R.id.etLastName);
        etEmail = findViewById(R.id.etEmail);
        etContactNumber = findViewById(R.id.etContactNumber);
        etPassword = findViewById(R.id.etPassword);
        etConfirmPassword = findViewById(R.id.etConfirmPassword);
        rlSubmit = findViewById(R.id.rlSubmit);
        tilCountryCode = findViewById(R.id.tilCountryCode);
        etCountryCode = findViewById(R.id.etCountryCode);
        tvLogin = findViewById(R.id.tvLogin);

        etCountryCode.setText(Constant.DEFAULT_COUNTRY_CODE);
        etCountryCode.setFocusable(false);
        etCountryCode.setOnClickListener(this);
        tilCountryCode.setOnClickListener(this);
        rlSubmit.setOnClickListener(this);
        tvLogin.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.etCountryCode:
            case R.id.tilCountryCode:
                Utility.hideKeyboard(this);
                startActivityForResult(new Intent(RegisterActivity.this, SearchCountryActivity.class), COUNTRY_REQUREST_CODE);

                break;
            case R.id.rlSubmit:
                if (isValid()) {
                    if (Utility.isInternetConnected(RegisterActivity.this)) {
                        makeLoginRequest();
                    } else {
                        Utility.showToast(RegisterActivity.this, getString(R.string.no_internet));
                    }
                }
                break;

            case R.id.tvLogin:
                startActivity(new Intent(RegisterActivity.this, Login.class));
                finish();
                break;

        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);


        switch (requestCode) {
            case COUNTRY_REQUREST_CODE:
                if (resultCode == RESULT_OK) {
                    String countryCode = data.getExtras().getString("countryCode");
                    etCountryCode.setText(countryCode);
                }
                break;
        }
    }

    private boolean isValid() {

        if (TextUtils.isEmpty(etFirstName.getText().toString().trim())) {
            etFirstName.requestFocus();
            tilFirstName.setErrorEnabled(true);
            tilFirstName.setError(getString(R.string.error_first_name));
            return false;
        } else {
            tilFirstName.setError(null);
        }

        if (TextUtils.isEmpty(etLastName.getText().toString().trim())) {
            etLastName.requestFocus();
            tilLastName.setErrorEnabled(true);
            tilLastName.setError(getString(R.string.error_last_name));
            return false;
        } else {
            tilLastName.setError(null);
        }

        if (!validateEmail()) {
            return false;
        }

        if (TextUtils.isEmpty(etContactNumber.getText().toString().trim()) || etContactNumber.getText().toString().length() < 10) {
            etContactNumber.requestFocus();
            tilContactNumber.setErrorEnabled(true);
            tilContactNumber.setError(getString(R.string.error_contact_number));
            return false;
        } else {
            tilContactNumber.setError(null);
        }

        if (TextUtils.isEmpty(etPassword.getText().toString().trim()) || etPassword.getText().toString().length() < 6) {
            etPassword.requestFocus();
            tilPassword.setErrorEnabled(true);
            tilPassword.setError(getString(R.string.error_password));
            return false;
        } else {
            tilPassword.setError(null);
        }

        if (TextUtils.isEmpty(etConfirmPassword.getText().toString().trim()) || !etPassword.getText().toString().trim().equals(etConfirmPassword.getText().toString().trim())) {
            etConfirmPassword.requestFocus();
            tilConfirmPassword.setErrorEnabled(true);
            tilConfirmPassword.setError(getString(R.string.error_password_not_match));
            return false;
        } else {
            tilConfirmPassword.setError(null);
        }

        return true;
    }

    private boolean validateEmail() {
        if (TextUtils.isEmpty(etEmail.getText().toString().trim())) {
            tilEmail.setErrorEnabled(true);
            tilEmail.setError(getString(R.string.error_email_empty));
            etEmail.requestFocus();
            return false;
        } else {
            tilEmail.setErrorEnabled(false);
        }

        if (!isValidEmail(etEmail.getText().toString().trim())) {
            tilEmail.setErrorEnabled(true);
            tilEmail.setError(getString(R.string.error_email_incorrect));
            etEmail.requestFocus();
            return false;
        } else {
            tilEmail.setErrorEnabled(false);
        }
        return true;
    }

    // validating email id
    private boolean isValidEmail(String email) {
        String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

        Pattern pattern = Pattern.compile(EMAIL_PATTERN);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }

    private void makeLoginRequest() {
        String firstName = etFirstName.getText().toString().trim();
        String lastName = etLastName.getText().toString().trim();
        String email = etEmail.getText().toString().trim();
        String conutryCode = etCountryCode.getText().toString().trim();
        String contactNumber = etContactNumber.getText().toString().trim();
        String password = etPassword.getText().toString().trim();
        SignUpRequest request = new SignUpRequest(email, password, contactNumber, firstName, lastName, conutryCode);
        Gson gson = new GsonBuilder().create();
        String requestJson = gson.toJson(request);
        callRegisterApi(requestJson);

    }

    private void callRegisterApi(String requestJson) {
        pDialog = new MyCustomProgressDialog(RegisterActivity.this, R.style.MyTheme);
        //   pDialog.setMessage("Please wait...");
        pDialog.setIndeterminate(true);
        pDialog.setCancelable(false);
        pDialog.show();
        String url = Constant.SIGNUP;
        Utility.showingLog("url", "" + url + "request" + requestJson);
        GsonRequest gsonRequest = new GsonRequest<LoginResponseData>(Request.Method.POST, url,
                LoginResponseData.class, requestJson, new Response.Listener<LoginResponseData>() {

            @Override
            public void onResponse(LoginResponseData loginResponseData) {
                boolean status = loginResponseData.isStatus();

                if (status) {
                    pDialog.dismiss();
                    Utility.showToast(RegisterActivity.this, getString(R.string.wait_for_varification));
                    startActivity(new Intent(RegisterActivity.this, Login.class));
                    finish();
                } else {
                    Utility.showToast(RegisterActivity.this, loginResponseData.getMessage());
                    Preference.setIsLogin(false);
                    pDialog.dismiss();
                }

            }
        },

                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        pDialog.dismiss();
                        if (volleyError != null) {
                            if (volleyError instanceof NetworkError)
                                Utility.showToast(RegisterActivity.this, getResources().getString(R.string.slow_net));
                            else if (volleyError.getMessage() == null)
                                Utility.showToast(RegisterActivity.this, getResources().getString(R.string.network_error));
                            else
                                Utility.showToast(RegisterActivity.this, volleyError.getMessage());
                            Preference.setIsLogin(false);

                        }
                    }
                });
        DefaultRetryPolicy policy = new DefaultRetryPolicy(2 * 60 * 1000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        gsonRequest.setRetryPolicy(policy);
        AppController.getInstance().addToRequestQueue(gsonRequest);
    }

    private void removeError() {

        etFirstName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
                if (!TextUtils.isEmpty(charSequence.toString()))
                    tilFirstName.setErrorEnabled(false);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        etLastName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
                if (!TextUtils.isEmpty(charSequence.toString()))
                    tilLastName.setErrorEnabled(false);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        etEmail.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
                if (!TextUtils.isEmpty(charSequence.toString()))
                    tilEmail.setErrorEnabled(false);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        etContactNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
                if (!TextUtils.isEmpty(charSequence.toString()))
                    tilContactNumber.setErrorEnabled(false);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        etPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
                if (!TextUtils.isEmpty(charSequence.toString()))
                    tilPassword.setErrorEnabled(false);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        etConfirmPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
                if (!TextUtils.isEmpty(charSequence.toString()))
                    tilConfirmPassword.setErrorEnabled(false);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }
}
