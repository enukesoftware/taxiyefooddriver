package com.taxiyefood.driver;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.IBinder;
import android.util.Log;

import androidx.core.app.NotificationCompat;
import androidx.core.content.ContextCompat;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.gson.Gson;
import com.taxiyefood.driver.utils.Preference;

public class TrackerService extends Service {
    private static final String TAG = TrackerService.class.getSimpleName();
    public static final String CHANNEL_ID = "ForegroundServiceChannel";
    Context mContext;
    protected BroadcastReceiver stopReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.d(TAG, "received stop broadcast");
            mContext = context;
            //    orderId = intent.getStringExtra("orderId");
            // Stop the service when the notification is tapped
            unregisterReceiver(stopReceiver);
            stopSelf();
        }
    };
    LocationObject locationObject;
    Location prevLoc = null;
    String orderId;

    @Override
    public IBinder onBind(Intent intent) {

        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        try {
            String data = intent.getStringExtra("data");
            locationObject = new Gson().fromJson(data, LocationObject.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onCreate() {
        super.onCreate();

        buildNotification();
        requestLocationUpdates();
    }

    private void buildNotification() {
        String stop = "stop";
        // registerReceiver(stopReceiver, new IntentFilter(stop));
        PendingIntent broadcastIntent = PendingIntent.getBroadcast(
                this, 0, new Intent(stop), PendingIntent.FLAG_UPDATE_CURRENT);

        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        String channelId = getString(R.string.app_name);
        NotificationChannel notificationChannel = null;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            notificationChannel = new NotificationChannel(channelId, channelId, NotificationManager.IMPORTANCE_DEFAULT);
            notificationChannel.setDescription(channelId);
            notificationChannel.setSound(null, null);
            notificationManager.createNotificationChannel(notificationChannel);
        }


        // Create the persistent notification
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this, channelId)
                .setContentTitle(getString(R.string.app_name))
                .setContentText(getString(R.string.notification_text))
                .setOngoing(true)
                //.setContentIntent(broadcastIntent)
                .setSmallIcon(R.drawable.logo);
        startForeground(1, builder.build());
    }

    private void requestLocationUpdates() {
        LocationRequest request = new LocationRequest();
        request.setInterval(1000);
        request.setFastestInterval(1000);
        request.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        FusedLocationProviderClient client = LocationServices.getFusedLocationProviderClient(this);
        final String path = getString(R.string.firebase_path) + "/" + Preference.getUserIdCommon();
        int permission = ContextCompat.checkSelfPermission(this,
                "android.permission.ACCESS_FINE_LOCATION");
        if (permission == PackageManager.PERMISSION_GRANTED) {
            // Request location updates and when an update is
            // received, store the location in Firebase
            client.requestLocationUpdates(request, new LocationCallback() {
                @Override
                public void onLocationResult(LocationResult locationResult) {

                    DatabaseReference ref = FirebaseDatabase.getInstance().getReference(path);
                    Location location = locationResult.getLastLocation();
                    try {
                        if (location != null) {

                            orderId = locationObject.getOrderId();
                            LocationObject note = new LocationObject();
                            // note.setuId(ref.child(orderId).push().getKey());
                            note.setmLattitude(location.getLatitude());
                            note.setmLongitude(location.getLongitude());
                            note.setPickUpLatitude(locationObject.getPickUpLatitude());
                            note.setPickUpLongitude(locationObject.getPickUpLongitude());
                            note.setDeliverLatitude(locationObject.getDeliverLatitude());
                            note.setDeliverLongitude(locationObject.getDeliverLongitude());
                            note.setOrderStatus(String.valueOf(Preference.getStatus(locationObject.getOrderIdValue())));

                            if (prevLoc == null)
                                prevLoc = location;

                            ref.child(orderId).child("123").setValue(note);
                            prevLoc = location;

                        }
                        if (Preference.getStatus(locationObject.getOrderIdValue()) == 5) {
                            stopSelf();
                            //MainActivity.getInstance().stopService(new Intent(mContext, TrackerService.class));
                            //  deleteNode(orderId);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }, null);
        }
    }

    public void deleteNode(String orderId) {
        final String path = getString(R.string.firebase_path) + "/" + Preference.getUserIdCommon();

        // FirebaseDatabase.getInstance().getReference(path).child(orderId).removeValue();
    }
}