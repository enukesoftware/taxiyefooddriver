
package com.taxiyefood.driver.data;


import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.provider.BaseColumns;


/**
 * Table with account related information.
 *
 * @author
 */
public abstract class AbstractAccountTable extends AbstractTable {

    public interface Fields extends BaseColumns {

        String ACCOUNT = "account";

    }

    /**
     * Remove records with specified account.
     *
     * @param account
     */
    public void removeAccount(String account) {
        SQLiteDatabase db = DatabaseManager.getInstance().getWritableDatabase();
        db.delete(getTableName(), Fields.ACCOUNT + " = ?",
                new String[]{account});
    }

    public static String getAccount(Cursor cursor) {
        return cursor.getString(cursor.getColumnIndex(Fields.ACCOUNT));
    }

}
