
package com.taxiyefood.driver.data;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;

import com.taxiyefood.driver.push.Bean.NotificationBean;
import com.taxiyefood.driver.utils.Utility;

import java.util.ArrayList;


/**
 * Storage with User Details.
 *
 * @author Parmanand
 */
@SuppressWarnings("ALL")
public class NotificationTable extends AbstractEntityTable {


    private static final class Fields implements AbstractEntityTable.Fields {


        private Fields() {
        }


        public static final String ID = "id";
        public static final String NOTI_ID = "noti_id";
        public static final String PACKET = "packet";
        public static final String ACTIVE = "active";
        public static final String ISACCEPTED = "isaccepted";


    }

    private static final String NAME = "notification";
    private static final String[] PROJECTION = new String[]{Fields._ID, Fields.NOTI_ID,
            Fields.PACKET, Fields.ACTIVE, Fields.ISACCEPTED};

    private final DatabaseManager databaseManager;
    private SQLiteStatement insertNewLoginStatement;
    private final Object insertNewLoginLock;

    private final static NotificationTable instance;

    static {
        instance = new NotificationTable(DatabaseManager.getInstance());
        DatabaseManager.getInstance().addTable(instance);
    }

    public static NotificationTable getInstance() {
        return instance;
    }

    private NotificationTable(DatabaseManager databaseManager) {
        this.databaseManager = databaseManager;
        insertNewLoginStatement = null;
        insertNewLoginLock = new Object();
    }

    @Override
    public void create(SQLiteDatabase db) {
        String sql;
        sql = "CREATE TABLE " + NAME + " (" + Fields._ID
                + " INTEGER PRIMARY KEY," + Fields.NOTI_ID + " TEXT,"
                + Fields.PACKET + " TEXT," + Fields.ACTIVE + " TEXT ," + Fields.ISACCEPTED + " TEXT);";
        DatabaseManager.execSQL(db, sql);

        sql = "CREATE INDEX " + NAME + "_list ON " + NAME + " ("
                + Fields.NOTI_ID
                + " ASC)";
        DatabaseManager.execSQL(db, sql);

    }

    @Override
    protected String getTableName() {
        return NAME;
    }

    @Override
    protected String[] getProjection() {
        return PROJECTION;
    }

    @Override
    public void migrate(SQLiteDatabase db, int toVersion) {
        super.migrate(db, toVersion);
        String sql;
        switch (toVersion) {

            default:
                break;
        }
    }

    /**
     * Save new user to the database.
     *
     * @return Assigned id.
     */
    public long add(NotificationBean notificationBean) {

        String Noti_id = notificationBean.getNotificationId();
        String Packet = notificationBean.getPacket();
        String Active = notificationBean.getActive();
        String Isaccepted = notificationBean.getIsAccepted();
        //Insert if new user
        if (!hasPacket(Noti_id)) {

            synchronized (insertNewLoginLock) {
                if (insertNewLoginStatement == null) {
                    SQLiteDatabase db = databaseManager.getWritableDatabase();
                    /*insertNewLoginStatement = db.compileStatement("INSERT INTO "
                            + NAME + " (" + Fields.NOTI_ID + ", "+ Fields.PACKET + ", " + Fields.ACTIVE + ") VALUES "
                            + "(?,?,?);");
                    */
                    insertNewLoginStatement = db.compileStatement("INSERT INTO "
                            + NAME + " (" + Fields.NOTI_ID + ", " + Fields.PACKET + ", " + Fields.ACTIVE + ", " + Fields.ISACCEPTED + ") VALUES "
                            + "(?, ?, ?,?);");

                    Utility.showingLog("insert statement", "" + insertNewLoginStatement);

                }
                insertNewLoginStatement.bindString(1, Noti_id);
                insertNewLoginStatement.bindString(2, Packet);
                insertNewLoginStatement.bindString(3, Active);
                insertNewLoginStatement.bindString(4, Isaccepted);

                return insertNewLoginStatement.executeInsert();
            }
        } else {
            //update if old user
            return UpdateActive(Noti_id, Packet, Active, Isaccepted);

        }
    }

    //return true if there is any Notification present
    boolean hasPacket(String Noti_id) {
        Cursor cr;

        SQLiteDatabase db = databaseManager.getWritableDatabase();
        cr = db.query(NAME, PROJECTION, Fields.NOTI_ID + " = ? ", new String[]{Noti_id},
                null, null, null);
        return cr.getCount() > 0;


    }


    //Update the Active if its already in database
    public long UpdateActive(String noti_id, String packet, String active, String IsAccepted) {
        SQLiteDatabase db = databaseManager.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(Fields.ACTIVE, active);
        values.put(Fields.PACKET, packet);
        values.put(Fields.ISACCEPTED, IsAccepted);

        return db.update(NAME, values, Fields.NOTI_ID + " = ?",
                new String[]{noti_id});
    }


    public ArrayList<NotificationBean> getGroupDetails(String Noti_id) {

        SQLiteDatabase db = databaseManager.getWritableDatabase();

        ArrayList<NotificationBean> arrayList = new ArrayList<>();

        Cursor res = db.query(NAME, PROJECTION, Fields.NOTI_ID + " = ? ", new String[]{Noti_id},
                null, null, null);


        res.moveToFirst();

        while (!res.isAfterLast()) {


            NotificationBean enitiy = new NotificationBean();

            enitiy.setNotificationId(res.getString(res.getColumnIndex(Fields.NOTI_ID)));
            enitiy.setPacket(res.getString(res.getColumnIndex(Fields.PACKET)));
            enitiy.setActive(res.getString(res.getColumnIndex(Fields.ACTIVE)));
            enitiy.setIsAccepted(res.getString(res.getColumnIndex(Fields.ISACCEPTED)));


            arrayList.add(enitiy);

            res.moveToNext();
        }
        res.close();
        //  db.close();

        return arrayList;
    }


    public ArrayList<NotificationBean> getGroupDetailsall() {

        SQLiteDatabase db = databaseManager.getWritableDatabase();

        ArrayList<NotificationBean> arrayList = new ArrayList<>();
     /*   Cursor res = db.query(true,NAME, PROJECTION, null,null,
                Fields.NOTI_ID, null, null,null);*/
        Cursor res = db.query(NAME, PROJECTION, null, null,
                null, null, null);


        res.moveToFirst();

        while (!res.isAfterLast()) {


            NotificationBean enitiy = new NotificationBean();

            enitiy.setNotificationId(res.getString(res.getColumnIndex(Fields.NOTI_ID)));
            enitiy.setPacket(res.getString(res.getColumnIndex(Fields.PACKET)));
            enitiy.setActive(res.getString(res.getColumnIndex(Fields.ACTIVE)));
            enitiy.setIsAccepted(res.getString(res.getColumnIndex(Fields.ISACCEPTED)));


            arrayList.add(enitiy);

            res.moveToNext();
        }
        res.close();
        //  db.close();

        return arrayList;
    }


    final String[] PROJECTION12 = new String[]{
            Fields.ACTIVE};

    public int getActivecount() {

        SQLiteDatabase db = databaseManager.getWritableDatabase();
        String result = null;
        ArrayList<NotificationBean> arrayList = new ArrayList<>();

        String value = "1";
        Cursor res = db.query(NAME, PROJECTION, Fields.ACTIVE + " = ? ", new String[]{value},
                null, null, null);


        res.moveToFirst();

        while (!res.isAfterLast()) {


            NotificationBean enitiy = new NotificationBean();

            enitiy.setNotificationId(res.getString(res.getColumnIndex(Fields.NOTI_ID)));
            enitiy.setPacket(res.getString(res.getColumnIndex(Fields.PACKET)));
            enitiy.setActive(res.getString(res.getColumnIndex(Fields.ACTIVE)));
            enitiy.setIsAccepted(res.getString(res.getColumnIndex(Fields.ISACCEPTED)));


            arrayList.add(enitiy);

            res.moveToNext();
        }
        res.close();
        //  db.close();

        return res.getCount();
    }


    final String[] PROJECTION1 = new String[]{
            Fields.ISACCEPTED};


    public String getisaccepted(String Noti_id) {

        SQLiteDatabase db = databaseManager.getWritableDatabase();
        String result = null;
        ArrayList<NotificationBean> arrayList = new ArrayList<>();
     /*   Cursor res = db.query(true,NAME, PROJECTION, null,null,
                Fields.NOTI_ID, null, null,null);*/
        Cursor res = db.query(NAME, PROJECTION1, Fields.NOTI_ID + " = ? ", new String[]{Noti_id},
                null, null, null);

        res.moveToFirst();
        result = res.getString(res.getColumnIndex(Fields.ISACCEPTED));

        res.close();
        //  db.close();

        return result;
    }


}