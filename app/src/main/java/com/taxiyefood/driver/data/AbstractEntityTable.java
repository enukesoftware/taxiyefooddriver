package com.taxiyefood.driver.data;

import android.database.Cursor;


/**
 * Table with entity related information.
 *
 * @author
 */
public abstract class AbstractEntityTable extends AbstractAccountTable {

    public interface Fields extends AbstractAccountTable.Fields {

        String USER = "user";

    }

    public static String getUser(Cursor cursor) {
        return cursor.getString(cursor.getColumnIndex(Fields.USER));
    }

}