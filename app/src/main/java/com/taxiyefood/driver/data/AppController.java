package com.taxiyefood.driver.data;


import android.Manifest;
import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.TypedArray;
import android.os.Build;
import android.text.TextUtils;

import androidx.core.content.ContextCompat;
import androidx.multidex.MultiDex;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;
import com.taxiyefood.driver.R;
import com.taxiyefood.driver.interfaces.UpdateAcceptedOrder;
import com.taxiyefood.driver.interfaces.UpdatingTextView;
import com.taxiyefood.driver.utils.LocUpdate;
import com.taxiyefood.driver.utils.MyCache;
import com.taxiyefood.driver.utils.Utility;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;


public class AppController extends Application {


    private ImageLoader mImageLoader;
    private static AppController instance;
    private final ArrayList<Object> registeredManagers;
    private final HashMap<Class<? extends BaseManagerInterface>, Collection<? extends BaseManagerInterface>> managerInterfaces;
    private boolean closed;
    public static final String TAG = AppController.class
            .getSimpleName();
    private RequestQueue mRequestQueue;
    public  UpdateAcceptedOrder updateAcceptedOrder;
    public UpdatingTextView updatingTextView;


    public static AppController getInstance() {
        if (instance == null)
            throw new IllegalStateException();
        return instance;
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    public AppController() {
        managerInterfaces = new HashMap<Class<? extends BaseManagerInterface>, Collection<? extends BaseManagerInterface>>();
        registeredManagers = new ArrayList<Object>();
    }

    @Override
    public void onCreate() {
        super.onCreate();

        instance = this;
        //LocaleHelper.onCreate(this, "en");
        Thread.currentThread().setPriority(Thread.MAX_PRIORITY);

      //  startTimer();


        TypedArray managerClasses = getResources().obtainTypedArray(
                R.array.managers);
        for (int index = 0; index < managerClasses.length(); index++)
            try {
                Class.forName(managerClasses.getString(index));
            } catch (ClassNotFoundException e) {
                throw new RuntimeException(e);
            }
        managerClasses.recycle();
        TypedArray tableClasses = getResources().obtainTypedArray(
                R.array.tables);
        for (int index = 0; index < tableClasses.length(); index++)
            try {
                Class.forName(tableClasses.getString(index));
            } catch (ClassNotFoundException e) {
                throw new RuntimeException(e);
            }
        tableClasses.recycle();



        int result1 = ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION);

        List<String> permissions = new ArrayList<String>();

        if( result1 != PackageManager.PERMISSION_GRANTED ) {
            permissions.add( Manifest.permission.ACCESS_FINE_LOCATION );
            permissions.add(Manifest.permission.ACCESS_COARSE_LOCATION);
        }
        try {
            if (Build.VERSION.SDK_INT >= 23) {
             //   Log.i("service will not run", "ok");
                if( !permissions.isEmpty() ) {
                }
                else
                    startService(new Intent(getApplicationContext(), LocUpdate.class));
            }
            else
                startService(new Intent(getApplicationContext(), LocUpdate.class));

        } catch (Exception e) {
            e.printStackTrace();
        }



    /*    Thread.setDefaultUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
            @Override
            public void uncaughtException(Thread thread, Throwable e) {

                errorHandlerApiResponse1(e);
            }
        });*/
    }





    public ImageLoader getImageLoader1() {
        getRequestQueue();
        if (mImageLoader == null) {
            mImageLoader = new ImageLoader(this.mRequestQueue,
                    new LruBitmapCache());
        }
        return this.mImageLoader;
    }

    /**
     * Register new manager.
     *
     * @param manager
     */
    public void addManager(Object manager) {
        registeredManagers.add(manager);
    }

    /**
     * @param cls Requested class of managers.
     * @return List of registered manager.
     */
    @SuppressWarnings("unchecked")
    public <T extends BaseManagerInterface> Collection<T> getManagers(
            Class<T> cls) {
        if (closed)
            return Collections.emptyList();
        Collection<T> collection = (Collection<T>) managerInterfaces.get(cls);
        if (collection == null) {
            collection = new ArrayList<T>();
            for (Object manager : registeredManagers)
                if (cls.isInstance(manager))
                    collection.add((T) manager);
            collection = Collections.unmodifiableCollection(collection);
            managerInterfaces.put(cls, collection);
        }
        return collection;
    }

    private void onClose() {
        Utility.showingLog("Application", "onClose");
        for (Object manager : registeredManagers)
            if (manager instanceof OnCloseListener)
                ((OnCloseListener) manager).onClose();
        closed = true;
    }

    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(getApplicationContext());
        }
        return mRequestQueue;
    }

    public ImageLoader getImageLoader() {
        getRequestQueue();
        if (mImageLoader == null) {
            mImageLoader = new ImageLoader(this.mRequestQueue,
                    new MyCache());
        }
        return this.mImageLoader;
    }

    public <T> void addToRequestQueue(Request<T> req, String tag) {
        // set the default tag if tag is empty
        req.setTag(TextUtils.isEmpty(tag) ? TAG : tag);
        getRequestQueue().add(req);
    }

    public <T> void addToRequestQueue(Request<T> req) {
        req.setTag(TAG);
        getRequestQueue().add(req);
    }

    public void cancelPendingRequests(Object tag) {
        if (mRequestQueue != null) {
            mRequestQueue.cancelAll(tag);
        }
    }
}
