package com.taxiyefood.driver.data;


import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;

import com.taxiyefood.driver.databean.SignupBean;


/**
 * Created by enuke on 1/3/16.
 */
public class SignUpTable extends AbstractEntityTable{



    private static final class Fields implements AbstractEntityTable.Fields {


        public static final String USER_NAME = "user_name";
        public static final String EMAIL_ID = "emailId";
        public static final String MOBILE_NUMBER = "mobileNumber";
        public static final String FIRST_NAME = "fName";

        private Fields() {
        }


    }

    private static final String NAME = "SignUp";
    private final static SignUpTable instance;
    static {
        instance = new SignUpTable(DatabaseManager.getInstance());
        DatabaseManager.getInstance().addTable(instance);
    }    private static final String[] PROJECTION = new String[]{Fields._ID,
            Fields.USER_NAME, Fields.EMAIL_ID, Fields.MOBILE_NUMBER,Fields.FIRST_NAME};

    private final DatabaseManager databaseManager;
    private final Object insertNewLoginLock;
    private SQLiteStatement insertNewLoginStatement;

    private SignUpTable(DatabaseManager databaseManager) {
        this.databaseManager = databaseManager;
        insertNewLoginStatement = null;
        insertNewLoginLock = new Object();
    }

    public static SignUpTable getInstance() {
        return instance;
    }

    @Override
    public void create(SQLiteDatabase db) {
        String sql;
        sql = "CREATE TABLE " + NAME + " (" + Fields._ID
                + " INTEGER PRIMARY KEY,"
                + Fields.USER_NAME + " TEXT, "
                + Fields.EMAIL_ID + " TEXT,"
                + Fields.MOBILE_NUMBER + " TEXT,"
                + Fields.FIRST_NAME + " TEXT)";

        DatabaseManager.execSQL(db, sql);
        sql = "CREATE INDEX " + NAME + "_list ON " + NAME + " ("
                + Fields.USER_NAME
                + " ASC)";
        DatabaseManager.execSQL(db, sql);

    }

    @Override
    protected String getTableName() {
        return NAME;
    }

    @Override
    protected String[] getProjection() {
        return PROJECTION;
    }

    @Override
    public void migrate(SQLiteDatabase db, int toVersion) {
        super.migrate(db, toVersion);
        String sql;
        switch (toVersion) {

            default:
                break;
        }
    }

    /**
     * Save new user to the database.
     *
     * @return Assigned id.
     */
    public long add(SignupBean signupBean) {


        String userName = signupBean.getUserName();
        String emailId = signupBean.getEmailId();
        String mobileNumber = signupBean.getMobileNumber();
        String firstName = signupBean.getFirstName();


        if(!hasUser(userName)) {
            synchronized (insertNewLoginLock) {
                if (insertNewLoginStatement == null) {
                    SQLiteDatabase db = databaseManager.getWritableDatabase();
                    insertNewLoginStatement = db.compileStatement("INSERT INTO "
                            + NAME + " (" + Fields.USER_NAME + ", " + Fields.EMAIL_ID + ", "
                            + Fields.MOBILE_NUMBER + ", "
                            + Fields.FIRST_NAME + ") VALUES "
                            + "(?,?,?,?);");
                }
                insertNewLoginStatement.bindString(1, userName);
                insertNewLoginStatement.bindString(2, emailId);
                insertNewLoginStatement.bindString(3, mobileNumber);
                insertNewLoginStatement.bindString(4, firstName);

                return insertNewLoginStatement.executeInsert();
            }
        }
        else
        {
            return updateUser(userName,emailId,mobileNumber,firstName);
        }

    }




    //return true if there is any user present
    public boolean hasUser(String user) {
        SQLiteDatabase db = databaseManager.getWritableDatabase();
        Cursor cr = db.query(NAME, null, Fields.USER_NAME + " = ? ", new String[]{user},
                null, null, null);

        return cr.getCount() > 0;
    }

    //Update the Other Details if its already in database
    public long updateUser(String userName, String emailId, String mobileNumber,String firstName) {
        SQLiteDatabase db = databaseManager.getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put(Fields.USER_NAME, userName);
        values.put(Fields.EMAIL_ID, emailId);
        values.put(Fields.MOBILE_NUMBER, mobileNumber);
        values.put(Fields.FIRST_NAME, firstName);




        return db.update(NAME, values, Fields.USER_NAME + " = ?",
                new String[]{userName});
    }

    public SignupBean getSignUpDetail(String userId) {

        SQLiteDatabase db = databaseManager.getWritableDatabase();
        SignupBean signupBean = null;

        Cursor cursor = db.query(NAME, PROJECTION, Fields.USER_NAME +" = ? ", new String[]{userId}, null, null, null);

        if(cursor.getCount() >0) {
            cursor.moveToFirst();
            signupBean = new SignupBean(cursor.getString(cursor.getColumnIndex(Fields.USER_NAME)),
                    cursor.getString(cursor.getColumnIndex(Fields.EMAIL_ID)),
                    cursor.getString(cursor.getColumnIndex(Fields.MOBILE_NUMBER)),
                            cursor.getString(cursor.getColumnIndex(Fields.FIRST_NAME))
                   );

        }
        cursor.close();
        return signupBean;
    }



}
