package com.taxiyefood.driver.fragment;


import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.iid.FirebaseInstanceId;
import com.taxiyefood.driver.R;
import com.taxiyefood.driver.adapter.AcceptedOrderAdapter;
import com.taxiyefood.driver.data.AppController;
import com.taxiyefood.driver.firebasenotification.UpdateFirebaseToken;
import com.taxiyefood.driver.interfaces.UpdateAcceptedOrder;
import com.taxiyefood.driver.interfaces.UpdatingTextView;
import com.taxiyefood.driver.serverrequest.LocationResponseData;
import com.taxiyefood.driver.utils.Constant;
import com.taxiyefood.driver.utils.Preference;
import com.taxiyefood.driver.utils.Utility;

import java.util.ArrayList;


/**
 * Created by enuke on 19/5/16.
 */
@SuppressWarnings("ALL")
public class AcceptedOrderFragment extends Fragment implements UpdateAcceptedOrder,UpdatingTextView{

   public ArrayList<LocationResponseData> locationResponseDatas;

    private RecyclerView myRecyclerView;
    public static AcceptedOrderAdapter myRecyclerViewAdapter;

   public TextView noData;
    public static AcceptedOrderFragment acceptedOrderFragment;

    public static AcceptedOrderFragment getInstance()
    {
        return acceptedOrderFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.e("on fragment","creat");
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Log.e("on fragment","activity created");
    }

    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
        Log.e("on fragment","view restored");
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        Log.e("on fragment","attach");
    }

    @Override
    public void onStart() {
        super.onStart();
        Log.e("on fragment","start");
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        Log.e("on fragment","deview");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.e("on fragment","des");
    }

    @Override
    public void onDetach() {
        super.onDetach();
        Log.e("on fragment","detach");
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.e("on fragment","pause");
    }

    @Override
    public void onStop() {
        super.onStop();
        Log.e("on fragment","stop");
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
         super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.accepted_orders,container,false);
        Log.e("on fragment","creatview");
        acceptedOrderFragment = this;
        settingUI();
        settingLayout(view);
        updateFirebaseNotification();
        myRecyclerViewAdapter = new AcceptedOrderAdapter(getActivity(),locationResponseDatas);
        myRecyclerView.setAdapter(myRecyclerViewAdapter);

        AppController.getInstance().updateAcceptedOrder = acceptedOrderFragment;
        AppController.getInstance().updatingTextView = acceptedOrderFragment;


        return view;
    }

    private void settingUI() {
        locationResponseDatas = new ArrayList<>();
        Utility.showingLog("current milli",""+System.currentTimeMillis());
        locationResponseDatas = Utility.locationResponseDatas;
        /*acceptedOrderBeen.add(new AcceptedOrderBean("Subway Japanese Resturant","Sector 18,Udyog Vihar Gurgaon","Mr. Raj Kapoor","Sector 18,Udyog Vihar Gurgaon","18-3-2016","2322","Ready","300","","","",""+30000));
        acceptedOrderBeen.add(new AcceptedOrderBean("Rajdhani Rajsthali Resturant","MG Road, Iffco Chowk Gurgaon","Mr. Shyam Taneja","MG Road, Iffco Chowk Gurgaon","18-3-2016","2324","Ready","300","","","",""+60000));//1463570598700*/
    }

    private void settingLayout(View view) {
        noData = view.findViewById(R.id.no_data_there);

        myRecyclerView = view.findViewById(R.id.acceptedOrderRecycler);
        myRecyclerView.setHasFixedSize(true);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);


        myRecyclerView.setLayoutManager(linearLayoutManager);
    }


    @Override
    public void onResume() {
        super.onResume();

        Log.e("on fragment","resume");
        if(myRecyclerViewAdapter.getItemCount() >0)
            noData.setVisibility(View.GONE);
        else {
            noData.setVisibility(View.VISIBLE);
            if (Preference.getActiveDeactiveStatus())
                noData.setText(getString(R.string.no_data));
            else
                noData.setText(getString(R.string.deactive));
        }
    }

    @Override
    public void updateData() {
        Utility.showingLog("in fragment","data"+Utility.locationResponseDatas);
        locationResponseDatas = Utility.locationResponseDatas;
        myRecyclerViewAdapter = new AcceptedOrderAdapter(getActivity(),locationResponseDatas);
        myRecyclerView.setAdapter(myRecyclerViewAdapter);

        myRecyclerViewAdapter.notifyDataSetChanged();

        if(myRecyclerViewAdapter.getItemCount() >0)
            noData.setVisibility(View.GONE);
        else
            noData.setVisibility(View.VISIBLE);
    }

    @Override
    public void updatingTextView() {
        if(myRecyclerViewAdapter.getItemCount() >0)
            noData.setVisibility(View.GONE);
        else {
            noData.setVisibility(View.VISIBLE);
            if(Preference.getActiveDeactiveStatus()) {
                Activity activity = getActivity();
                if(activity != null && isAdded())
                noData.setText(getString(R.string.no_data));
            }
            else
                noData.setText(getString(R.string.deactive));
        }
    }
    /**
     * update device token to server for firebase
     */
    private void updateFirebaseNotification() {
        if (Preference.getUserIdCommon() != null) {
            if (FirebaseInstanceId.getInstance().getToken() != null && Preference.getUserIdCommon() != null) {
               String firebaseToken = FirebaseInstanceId.getInstance().getToken();
                if (!Preference.getBooleanNotification(Constant.PREFERENCES_UPDATE_NOTIFICATION_STATUS)) {
                    UpdateFirebaseToken.update(getActivity(), firebaseToken);
                }
            }
        }
    }
}
