package com.taxiyefood.driver.fragment;


import android.Manifest;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.PorterDuff;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.taxiyefood.driver.FloatingViewService;
import com.taxiyefood.driver.LocationObject;
import com.taxiyefood.driver.R;
import com.taxiyefood.driver.TrackerService;
import com.taxiyefood.driver.data.AppController;
import com.taxiyefood.driver.firebasenotification.UpdateFirebaseToken;
import com.taxiyefood.driver.interfaces.UpdatingFrameAndBackKey;
import com.taxiyefood.driver.serverrequest.AcceptedOrderRequest;
import com.taxiyefood.driver.serverrequest.AcceptedOrderResponse;
import com.taxiyefood.driver.serverrequest.LocationResponseData;
import com.taxiyefood.driver.ui.MainActivity;
import com.taxiyefood.driver.utils.Constant;
import com.taxiyefood.driver.utils.GsonRequest;
import com.taxiyefood.driver.utils.MyCustomProgressDialog;
import com.taxiyefood.driver.utils.Preference;
import com.taxiyefood.driver.utils.Utility;

import de.hdodenhof.circleimageview.CircleImageView;


/**
 * Created by enuke on 20/5/16.
 */
public class AcceptCancelFragment extends Fragment implements View.OnClickListener, CompoundButton.OnCheckedChangeListener {

    private static final int DRAW_OVER_OTHER_APP_PERMISSION = 123;
    public FrameLayout frameLayout;
    TextView restName, deliveryAddr, deliveryToName, receiverAddr, orderVal, amountVal, statusVal;
    ImageView restImage, deliverToImage, pickUpLocation, deliveryLocation;
    RelativeLayout markingStatus, popupShowing;
    String orderId, amount, status, restNameGet, restAddress, deliverToPersonName, reciverAdd, orderIdValue, myId;
    LocationResponseData locationResponseData;
    Bundle bundle;
    CheckBox pickUpCheck, deliveryUncheck;
    double restLatitude = 0.0, restLongitude = 0.0, deliveryLat = 0.0, deliveryLong = 0.0;
    int orderStatus;
    Button cancellingOrder;
    UpdatingFrameAndBackKey updatingFrameAndBackKey;
    boolean deliveryFlag = false;
    private ProgressDialog pDialog;
    CircleImageView civCustomer;
    TextView tvCustomerName;
    TextView tvCustomerPhone;
    CardView cardCall;
    String customerName, customerNumber = "NA", profileImage;
    RequestOptions requestOptions;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (!(getActivity() instanceof UpdatingFrameAndBackKey)) {
            throw new ClassCastException("Hosting activity must implement BackHandlerInterface");
        } else {
            updatingFrameAndBackKey = (UpdatingFrameAndBackKey) getActivity();
        }
        askForSystemOverlayPermission();
    }

    private void askForSystemOverlayPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && !Settings.canDrawOverlays(getActivity())) {

            //If the draw over permission is not available open the settings screen
            //to grant the permission.
            Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION,
                    Uri.parse("package:" + getContext().getPackageName()));
            startActivityForResult(intent, DRAW_OVER_OTHER_APP_PERMISSION);
        }
    }


    @Override
    public void onPause() {
        super.onPause();
        // To prevent starting the service if the required permission is NOT granted.
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M || Settings.canDrawOverlays(getActivity())) {
            // getContext().startService(new Intent(getActivity(), FloatingViewService.class).putExtra("activity_background", true));
            // getActivity().finish();
        } else {
            errorToast();
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == DRAW_OVER_OTHER_APP_PERMISSION) {

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (!Settings.canDrawOverlays(getActivity())) {
                    //Permission is not available. Display error text.
                    errorToast();
                    getActivity().finish();
                }
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    private void errorToast() {
        Toast.makeText(getActivity(), "Draw over other app permission not available. Can't start the application without the permission.", Toast.LENGTH_LONG).show();
    }


    @Override
    public void onStart() {
        super.onStart();

        // Mark this fragment as the selected Fragment.
        updatingFrameAndBackKey.selectedFragment(this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.accept_cancel_fragment, container, false);
        String floor = "";
        String houseNo = "";
        String street = "";
        String areaName = "";
        String city = "";
        updateFirebaseNotification();
        if (Preference.getAcceptedOrderStatusValue().equalsIgnoreCase("")) {
            bundle = getArguments();
            locationResponseData = (LocationResponseData) bundle.getSerializable("order_id");
            orderId = locationResponseData.getId();
            orderIdValue = locationResponseData.getOrder_number();
            amount = locationResponseData.getAmount();
            status = locationResponseData.getStatus();
            restNameGet = locationResponseData.getRestaurant_name();
            restAddress = locationResponseData.getRestaurant_floor() + "," + locationResponseData.getRestaurant_street() + "," + locationResponseData.getRestaurant_area() + " " + locationResponseData.getRestaurant_city();
            deliverToPersonName = locationResponseData.getFirst_name() + " " + (locationResponseData.getLast_name() == null ? "" : locationResponseData.getLast_name());
            if (locationResponseData.getShipJson() != null) {
                if (locationResponseData.getShipJson().getAddressJson() != null) {
                    floor = (locationResponseData.getShipJson().getAddressJson().getFloorUnit() == null ? "" : locationResponseData.getShipJson().getAddressJson().getFloorUnit() + ", ");
                    houseNo = (locationResponseData.getShipJson().getAddressJson().getHouseNo() == null ? "" : locationResponseData.getShipJson().getAddressJson().getHouseNo() + ", ");
                    street = (locationResponseData.getShipJson().getAddressJson().getStreet() == null ? "" : locationResponseData.getShipJson().getAddressJson().getStreet() + ", ");
                    areaName = (locationResponseData.getShipJson().getAddressJson().getAreaName() == null ? "" : locationResponseData.getShipJson().getAddressJson().getAreaName() + ", ");
                    city = (locationResponseData.getShipJson().getAddressJson().getCity() == null ? "" : locationResponseData.getShipJson().getAddressJson().getCity());

                }
                if (locationResponseData.getShipJson().getLatitude() != null && locationResponseData.getShipJson().getLongitude() != null) {
                    deliveryLat = Double.parseDouble(locationResponseData.getShipJson().getLatitude());
                    deliveryLong = Double.parseDouble(locationResponseData.getShipJson().getLongitude());
                } else {
                    deliveryLat = 0.0;
                    deliveryLong = 0.0;
                }
            } else {
                deliveryLat = 0.0;
                deliveryLong = 0.0;
            }
            reciverAdd = floor + houseNo + street + areaName + city;
            restLatitude = Double.parseDouble(locationResponseData.getRestaurant_latitude());
            restLongitude = Double.parseDouble(locationResponseData.getRestaurant_longitude());

            setCustomerData(locationResponseData);
            Utility.showingLog("ORDER ID received IF", "" + locationResponseData.getId());
        } else {
            locationResponseData = new Gson().fromJson(Preference.getAcceptedOrderStatusValue(), LocationResponseData.class);
            orderId = locationResponseData.getId();
            orderIdValue = locationResponseData.getOrder_number();
            amount = locationResponseData.getAmount();
            status = locationResponseData.getStatus();
            restNameGet = locationResponseData.getRestaurant_name();
            restAddress = locationResponseData.getRestaurant_floor() + "," + locationResponseData.getRestaurant_street() + "," + locationResponseData.getRestaurant_area() + " " + locationResponseData.getRestaurant_city();
            deliverToPersonName = locationResponseData.getFirst_name() + " " + (locationResponseData.getLast_name() == null ? "" : locationResponseData.getLast_name());
            if (locationResponseData.getShipJson() != null) {
                if (locationResponseData.getShipJson().getAddressJson() != null) {
                    floor = (locationResponseData.getShipJson().getAddressJson().getFloorUnit() == null ? "" : locationResponseData.getShipJson().getAddressJson().getFloorUnit() + ", ");
                    houseNo = (locationResponseData.getShipJson().getAddressJson().getHouseNo() == null ? "" : locationResponseData.getShipJson().getAddressJson().getHouseNo() + ", ");
                    street = (locationResponseData.getShipJson().getAddressJson().getStreet() == null ? "" : locationResponseData.getShipJson().getAddressJson().getStreet() + ", ");
                    areaName = (locationResponseData.getShipJson().getAddressJson().getAreaName() == null ? "" : locationResponseData.getShipJson().getAddressJson().getAreaName() + ", ");
                    city = (locationResponseData.getShipJson().getAddressJson().getCity() == null ? "" : locationResponseData.getShipJson().getAddressJson().getCity());

                }
                if (locationResponseData.getShipJson().getLatitude() != null && locationResponseData.getShipJson().getLongitude() != null) {
                    deliveryLat = Double.parseDouble(locationResponseData.getShipJson().getLatitude());
                    deliveryLong = Double.parseDouble(locationResponseData.getShipJson().getLongitude());
                } else {
                    deliveryLat = 0.0;
                    deliveryLong = 0.0;
                }
            } else {
                deliveryLat = 0.0;
                deliveryLong = 0.0;
            }

            if (locationResponseData.getRestaurant_latitude() != null) {
                restLatitude = Double.parseDouble(locationResponseData.getRestaurant_latitude());
                restLongitude = Double.parseDouble(locationResponseData.getRestaurant_longitude());
//                deliveryLat = Double.parseDouble(locationResponseData.getRestaurant_latitude());
//                deliveryLong = Double.parseDouble(locationResponseData.getRestaurant_longitude());
            } else {
                restLatitude = 0.0;
                restLongitude = 0.0;
//                deliveryLat = 0.0;
//                deliveryLong = 0.0;
            }
            reciverAdd = floor + houseNo + street + areaName + city;

            setCustomerData(locationResponseData);

            Utility.showingLog("ORDER ID received ELSE", "" + locationResponseData.getId());
        }
        myId = orderId;

        settingUI(view);
        return view;
    }

    private void setCustomerData(LocationResponseData locationResponseData) {
        if (locationResponseData.getProfileImage() != null && !locationResponseData.getProfileImage().equals("")) {
            profileImage = locationResponseData.getProfileImage();
        }

        if (locationResponseData.getFirst_name() == null && locationResponseData.getLast_name() == null) {
            customerName = "NA";
        } else if (locationResponseData.getFirst_name() != null && locationResponseData.getLast_name() != null) {
            customerName = locationResponseData.getFirst_name() + " " + locationResponseData.getLast_name();
        } else if (locationResponseData.getFirst_name() != null) {
            customerName = locationResponseData.getFirst_name();
        } else if (locationResponseData.getLast_name() != null) {
            customerName = locationResponseData.getLast_name();
        } else {
            customerName = "NA";
        }

        customerNumber = locationResponseData.getContactNumber() == null ? "NA" : locationResponseData.getContactNumber();
    }


    private void settingUI(View view) {
        restName = view.findViewById(R.id.restValue);
        deliveryAddr = view.findViewById(R.id.pickUpDeliveryAdd);
        deliveryToName = view.findViewById(R.id.deliverToName);
        receiverAddr = view.findViewById(R.id.deliverToAddr);
        orderVal = view.findViewById(R.id.orderIdVal);
        amountVal = view.findViewById(R.id.amountVal);
        statusVal = view.findViewById(R.id.statusVal);
        frameLayout = view.findViewById(R.id.overlay);
        pickUpCheck = view.findViewById(R.id.pickUpCheck);
        deliveryUncheck = view.findViewById(R.id.deliveryUncheck);

        cancellingOrder = view.findViewById(R.id.cancellingOrder);

        civCustomer = view.findViewById(R.id.civCustomer);
        tvCustomerName = view.findViewById(R.id.tvCustomerName);
        tvCustomerPhone = view.findViewById(R.id.tvCustomerPhone);
        cardCall = view.findViewById(R.id.cardCall);


        pickUpCheck.setOnCheckedChangeListener(this);
        deliveryUncheck.setOnCheckedChangeListener(this);
        cancellingOrder.setOnClickListener(this);
        cardCall.setOnClickListener(this);

        orderStatus = Preference.getPickDeliveryStatus(orderId);
        if (orderStatus == 4) {
            pickUpCheck.setChecked(true);
            deliveryUncheck.setEnabled(true);
            pickUpCheck.setEnabled(false);
        } else
            deliveryUncheck.setEnabled(false);

        if (orderStatus == 3)
            statusVal.setText(getString(R.string.ready));
        else if (orderStatus == 4)
            statusVal.setText(getString(R.string.pickedup));
        else if (orderStatus == 5)
            statusVal.setText(getString(R.string.delvd));
        else if (orderStatus == 6)
            statusVal.setText(getString(R.string.cancelld));

        tvCustomerName.setText(customerName);
        tvCustomerPhone.setText(customerNumber);

        requestOptions = new RequestOptions()
                .placeholder(R.drawable.default_avatar)
                .error(R.drawable.default_avatar);

        if (profileImage != null) {
            Glide.with(getActivity()).
                    setDefaultRequestOptions(requestOptions).
                    load(profileImage).
                    into(civCustomer);
        }


        restName.setText(restNameGet);
        deliveryAddr.setText(restAddress);
        deliveryToName.setText(deliverToPersonName);
        receiverAddr.setText(reciverAdd);


        restImage = view.findViewById(R.id.restImage);
        deliverToImage = view.findViewById(R.id.deliverToImage);
        pickUpLocation = view.findViewById(R.id.pickUpLocation);
        deliveryLocation = view.findViewById(R.id.deliveryLocation);

        markingStatus = view.findViewById(R.id.markingStaus);
        popupShowing = view.findViewById(R.id.popupShowing);

        orderVal.setText(locationResponseData.getOrder_number());
        amountVal.setText(getResources().getString(R.string.currency_symbol) + amount);

        pickUpLocation.setOnClickListener(this);
        deliveryLocation.setOnClickListener(this);
        markingStatus.setOnClickListener(this);
        stopTrackerService();
        startTrackerService(orderIdValue);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.pickUpLocation:
                openingMap(restLatitude, restLongitude);
                break;
            case R.id.deliveryLocation:
                openingMap(deliveryLat, deliveryLong);
                break;
            case R.id.markingStaus:
                frameLayout.setVisibility(View.VISIBLE);
                popupShowing.setVisibility(View.VISIBLE);
                Preference.setFrameVisibilityStatus(true);
                if (Preference.getPickDeliveryStatus(orderId) == 4) {
                    deliveryFlag = true;
                    deliveryUncheck.setEnabled(true);
                }

                enableDisableItems(false);
                break;
            case R.id.cancellingOrder:
                if (Utility.isInternetConnected(getActivity())) {
                    if (Preference.getPickDeliveryStatus(orderId) == 4)
                        Utility.showToast(getActivity(), "You had already mark status Picked Up, so you can't cancel order");
                    else
                        cancelAlert();
                } else
                    Utility.showToast(getActivity(), getString(R.string.no_internet));
                break;
            case R.id.cardCall:
                if (!(customerNumber == null || customerNumber.equals("") || customerNumber.equals("NA"))) {
                    isPermissionGranted();
                }
                break;
            default:
                break;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        // if (orderIdValue.equalsIgnoreCase("4") && orderId != null)
        // startTrackerService(orderIdValue);
        //  stopFloatingService();
    }

    public void enableDisableItems(boolean status) {
        pickUpLocation.setEnabled(status);
        deliveryLocation.setEnabled(status);
        cancellingOrder.setEnabled(status);
        MainActivity.getInstance().activeDeactive.setEnabled(status);
    }


    private void cancelAlert() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity(), R.style.AppCompatAlertDialogStyle);
        builder.setCancelable(false);
        String message = "Please Enter the reason for cancelling the order";
        builder.setMessage(message);

        // Set up the input
        final EditText input = new EditText(getActivity());
        input.getBackground().mutate().setColorFilter(getResources().getColor(R.color.black), PorterDuff.Mode.SRC_ATOP);
        input.setTextColor(getResources().getColor(R.color.bg));
// Specify the type of input expected; this, for example, sets the input as a password, and will mask the text

        builder.setView(input);

        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {


                String inputValue = input.getText().toString();

                if (inputValue.equalsIgnoreCase("")) {
                    Utility.showToast(getActivity(), "Please Enter Reason For Cancelling order");
                } else {
                    makeAcceptRequest(6, inputValue);
                }
            }
        });
        builder.setNegativeButton("No", null);
        builder.show();

    }


    private void paymentAlert() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity(), R.style.AppCompatAlertDialogStyle);
        builder.setCancelable(false);
        String message = "Have you collected given Amount : " + locationResponseData.getAmount();
        builder.setMessage(message);
        builder.setTitle("Amount paid ?");

        // Set up the input
       /* final EditText input = new EditText(getActivity());
        input.getBackground().mutate().setColorFilter(getResources().getColor(R.color.black), PorterDuff.Mode.SRC_ATOP);
        input.setTextColor(getResources().getColor(R.color.bg));
// Specify the type of input expected; this, for example, sets the input as a password, and will mask the text

        builder.setView(input);*/

        builder.setPositiveButton("Collected", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {


               /* String inputValue = input.getText().toString();

                if(inputValue.equalsIgnoreCase(""))
                {
                    Utility.showToast(getActivity(),"Please Enter Reason For Cancelling order");
                }
                else {*/
                if (Utility.isInternetConnected(getActivity()))
                    makeAcceptRequest(5, "");
                else
                    Utility.showToast(getActivity(), getString(R.string.no_internet));
                /*}*/
            }
        });
        builder.setNegativeButton("Not Collected", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //   deliveryUncheck.setChecked(false);
                frameLayout.setVisibility(View.GONE);
                Preference.setFrameVisibilityStatus(false);
                enableDisableItems(true);
                deliveryFlag = false;
                deliveryUncheck.setEnabled(false);
                deliveryUncheck.setChecked(false);

            }
        });
        //   builder.setNegativeButton("No", null);
        builder.show();

    }

    private void openingMap(double lati, double longi) {
        Log.i("lati", "" + lati);
        Log.i("longi", "" + longi);

        try {
            //   startFloating();
            String url = "http://maps.google.com/maps?f=d&daddr=" + lati + "," + longi + "&dirflg=d&layer=t";
            Intent intent = new Intent(android.content.Intent.ACTION_VIEW, Uri.parse(url));
            intent.setClassName("com.google.android.apps.maps", "com.google.android.maps.MapsActivity");
            startActivity(intent);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        switch (buttonView.getId()) {
            case R.id.pickUpCheck:
                /*Preference.setPickDeliveryStatus(orderId, 4);
                    frameLayout.setVisibility(View.GONE);
                    deliveryUncheck.setEnabled(true);*/
                if (Utility.isInternetConnected(getActivity())) {
                    if (Preference.getPickDeliveryStatus(orderId) == 4)
                        Utility.showingLog("cancel fragment", "No request will go");
                    else
                        makeAcceptRequest(4, "");
                } else
                    Utility.showToast(getActivity(), getString(R.string.no_internet));
                break;
            case R.id.deliveryUncheck:
                /*if(orderStatus ==4)
                    frameLayout.setVisibility(View.GONE);*/
                if (Preference.getPickDeliveryStatus(orderId) == 4) {
                    if (Utility.isInternetConnected(getActivity())) {
                        if (deliveryFlag)
                            paymentAlert();
                    } else
                        Utility.showToast(getActivity(), getString(R.string.no_internet));
                }
                break;
            default:
                break;
        }
    }


    private void makeAcceptRequest(int status, String reason) {
        AcceptedOrderRequest acceptedOrderRequest = new AcceptedOrderRequest(Preference.getUserIdCommon(), orderId, String.valueOf(status), reason);
        Gson gson = new GsonBuilder().create();
        String acceptJson = gson.toJson(acceptedOrderRequest);
        orderStatus = status;
        makeServerRequest(acceptJson, status);

    }


    private void makeServerRequest(String loginJson, final int statusGet) {
        pDialog = new MyCustomProgressDialog(getActivity(), R.style.MyTheme);
        //  pDialog.setMessage("Please wait...");

        pDialog.setCancelable(false);
        pDialog.show();
        String url = Constant.ACCEPT_ORDER_METHOD;
        Utility.showingLog("url", "" + url + "request" + loginJson);
        GsonRequest gsonRequest = new GsonRequest<AcceptedOrderResponse>(Request.Method.POST, url,
                AcceptedOrderResponse.class, loginJson, new Response.Listener<AcceptedOrderResponse>() {

            @Override
            public void onResponse(AcceptedOrderResponse acceptedOrderResponse) {
                boolean status = acceptedOrderResponse.isSuccess();

                if (status) {
                    pDialog.dismiss();
                    if (statusGet == 4) {
                        Preference.setPickDeliveryStatus(orderId, 4);
                        Preference.setStatus(myId, 4);
                        frameLayout.setVisibility(View.GONE);
                        pickUpCheck.setEnabled(false);
                        Preference.setFrameVisibilityStatus(false);
                        deliveryUncheck.setEnabled(true);
                        statusVal.setText(getString(R.string.pickedup));
                        // Preference.setAcceptedOrderStatusValue("");
                        enableDisableItems(true);
                        // startTrackerService(locationResponseData.getOrder_number());

                    }
                    if (statusGet == 5 || statusGet == 6) {
                        frameLayout.setVisibility(View.GONE);
                        Preference.setFrameVisibilityStatus(false);
                        enableDisableItems(true);
                        Preference.setAcceptedOrderStatusValue("");
                        Preference.setPickDeliveryStatus(orderId, 0);
                        Preference.setStatus(myId, 5);
                        stopTrackerService();
                        if (AcceptedOrderFragment.getInstance() != null) {
                            if (AcceptedOrderFragment.getInstance().locationResponseDatas != null) {
                                AcceptedOrderFragment.getInstance().locationResponseDatas.clear();
                            }
                        }

                        if (getActivity().getSupportFragmentManager().getBackStackEntryCount() > 0) {
                            FragmentManager fm = getActivity().getSupportFragmentManager();
                            fm.popBackStack();
                        } else {
                            replaceFragment();
                        }
                    }
                } else {
                    Utility.showToast(getActivity(), acceptedOrderResponse.getError());
                    pDialog.dismiss();

                    if (statusGet == 4) {
                        frameLayout.setVisibility(View.GONE);
                        Preference.setFrameVisibilityStatus(false);
                        enableDisableItems(true);
                        pickUpCheck.setChecked(false);
                    } else {
                        frameLayout.setVisibility(View.GONE);
                        Preference.setFrameVisibilityStatus(false);
                        enableDisableItems(true);
                        deliveryUncheck.setChecked(false);
                    }
                }

            }
        },

                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        pDialog.dismiss();
                        if (volleyError != null) {
                            if (volleyError instanceof NetworkError)
                                Utility.showToast(getActivity(), getResources().getString(R.string.slow_net));
                            else if (volleyError.getMessage() == null)
                                Utility.showToast(getActivity(), getResources().getString(R.string.network_error));
                            else
                                Utility.showToast(getActivity(), volleyError.getMessage());


                        }
                    }
                });
        DefaultRetryPolicy policy = new DefaultRetryPolicy(2 * 60 * 1000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        gsonRequest.setRetryPolicy(policy);
        AppController.getInstance().addToRequestQueue(gsonRequest);
    }

    private void replaceFragment() {
        AcceptedOrderFragment acceptedOrderFragment = new AcceptedOrderFragment();
        getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.controllerFragment, acceptedOrderFragment).commit();

    }

    /**
     * update device token to server for firebase
     */
    private void updateFirebaseNotification() {
        if (Preference.getUserIdCommon() != null) {
            if (FirebaseInstanceId.getInstance().getToken() != null && Preference.getUserIdCommon() != null) {
                String firebaseToken = FirebaseInstanceId.getInstance().getToken();
                if (!Preference.getBooleanNotification(Constant.PREFERENCES_UPDATE_NOTIFICATION_STATUS)) {
                    UpdateFirebaseToken.update(getActivity(), firebaseToken);
                }
            }
        }
    }

    private void startTrackerService(String id) {
        Intent intent = new Intent(getActivity(), TrackerService.class);
        LocationObject locationObject = new LocationObject();
        locationObject.setDeliverLatitude(deliveryLat == 0.0 ? 28.4848783 : deliveryLat);
        locationObject.setDeliverLongitude(deliveryLong == 0.0 ? 77.0589723 : deliveryLong);
        locationObject.setPickUpLongitude(restLongitude);
        locationObject.setPickUpLatitude(restLatitude);
        locationObject.setOrderStatus("" + orderStatus);
        locationObject.setOrderIdValue("" + orderId);
        Log.d("OrderStatus", "" + orderStatus);
        locationObject.setOrderId(id);
        String data = new Gson().toJson(locationObject);
        intent.putExtra("data", data);
        getContext().startService(intent);
        //finish();
    }

    private void stopTrackerService() {
        Intent intent = new Intent(getActivity(), TrackerService.class);
        getContext().stopService(intent);
        //finish();
    }

    public void startFloating() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M || Settings.canDrawOverlays(getActivity())) {
            getContext().startService(new Intent(getActivity(), FloatingViewService.class));
        } else {
            errorToast();
        }
    }

    private void stopFloatingService() {
        Intent intent = new Intent(getActivity(), FloatingViewService.class);
        getContext().stopService(intent);
        //finish();
    }

    public boolean isPermissionGranted() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (ContextCompat.checkSelfPermission(getActivity(), android.Manifest.permission.CALL_PHONE)
                    == PackageManager.PERMISSION_GRANTED) {
                Log.v("TAG", "Permission is granted");
                call_action();
                return true;
            } else {

                Log.v("TAG", "Permission is revoked");
                requestPermissions(new String[]{Manifest.permission.CALL_PHONE}, 1);
                return false;
            }
        } else { //permission is automatically granted on sdk<23 upon installation
            Log.v("TAG", "Permission is granted");
            call_action();
            return true;
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {

            case 1: {
//                boolean showRationale = shouldShowRequestPermissionRationale( permission );

                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//                    Toast.makeText(getActivity(), "Permission granted", Toast.LENGTH_SHORT).show();
                    call_action();
                } else {
//                    Toast.makeText(getActivity(), "Permission denied", Toast.LENGTH_SHORT).show();
//                    settingsDialog();

                    // permission was not granted
                    if (getActivity() == null) {
                        return;
                    }
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (shouldShowRequestPermissionRationale(Manifest.permission.CALL_PHONE)) {
    //                        showStoragePermissionRationale();
    //                        boolean showRationale = shouldShowRequestPermissionRationale(permissions);
    //                        showRationale(permissions, R.string.permission_denied_call);
                            Log.e("PHONE_CALL","shouldShowRequestPermissionRationale");
                        } else {
                            Snackbar snackbar = Snackbar.make(getView(), getResources().getString(R.string.message_no_call_permission_snackbar), Snackbar.LENGTH_LONG);
                            snackbar.setAction(getResources().getString(R.string.settings), new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    if (getActivity() == null) {
                                        return;
                                    }
                                    Intent intent = new Intent();
                                    intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                                    Uri uri = Uri.fromParts("package", getActivity().getPackageName(), null);
                                    intent.setData(uri);
                                    getActivity().startActivity(intent);
                                }
                            });
                            snackbar.show();
                        }
                    }
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

    public void call_action() {
        Intent callIntent = new Intent(Intent.ACTION_CALL);
        callIntent.setData(Uri.parse("tel:" + customerNumber));
        startActivity(callIntent);
    }

    public void settingsDialog() {
        new android.app.AlertDialog.Builder(getActivity())
                .setMessage("Call Permossion Required")
                .setCancelable(false)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                        Intent intent = new Intent();
                        intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                        Uri uri = Uri.fromParts("package", getActivity().getPackageName(), null);
                        intent.setData(uri);
                        startActivity(intent);

                    }
                })
                .setTitle("Info")
                .create()
                .show();
    }
}
