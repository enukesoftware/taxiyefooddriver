package com.taxiyefood.driver.fragment;


import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.taxiyefood.driver.R;
import com.taxiyefood.driver.serverrequest.CompletedOrderResponseData;
import com.taxiyefood.driver.utils.Utility;


/**
 * Created by enuke on 19/5/16.
 */
public class CompletedOrderDetail extends Fragment implements OnClickListener{

    //String orderId;
    Bundle bundle;
    CompletedOrderResponseData completedOrderResponseData;

    TextView restName,deliveryAddr,deliveryToName,receiverAddr,orderVal,amountVal;
    ImageView restImage,deliverToImage,pickUpLocation,deliveryLocation;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.e("on fragment","creat");
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Log.e("on detail","activity created");
    }

    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
        Log.e("on detail","view restored");
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        Log.e("on detail","attach");
    }

    @Override
    public void onStart() {
        super.onStart();
        Log.e("on detail","start");
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        Log.e("on detail","deview");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.e("on detail","des");
    }

    @Override
    public void onDetach() {
        super.onDetach();
        Log.e("on detail","detach");
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.e("on detail","pause");
    }

    @Override
    public void onStop() {
        super.onStop();
        Log.e("on detail","stop");
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.e("on detail","resume");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
         super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.completed_order_detail_fragment,container,false);

        Log.e("on detail","createview");
        bundle = getArguments();
        completedOrderResponseData = (CompletedOrderResponseData)bundle.getSerializable("order_id");
        Utility.showingLog("ORDER ID received",""+completedOrderResponseData.getId());

        settingUI(view);
        return view;

    }

    private void settingUI(View view) {
        String floor = "";
        String houseNo = "";
        String street = "";
        String areaName = "";
        String city = "";
        restName = view.findViewById(R.id.restValue);
        deliveryAddr = view.findViewById(R.id.pickUpDeliveryAdd);
        deliveryToName = view.findViewById(R.id.deliverToName);
        receiverAddr = view.findViewById(R.id.deliverToAddr);
        orderVal = view.findViewById(R.id.orderIdVal);
        amountVal = view.findViewById(R.id.amountVal);


        restImage = view.findViewById(R.id.restImage);
        deliverToImage = view.findViewById(R.id.deliverToImage);
        pickUpLocation = view.findViewById(R.id.pickUpLocation);
        deliveryLocation = view.findViewById(R.id.deliveryLocation);

        orderVal.setText(completedOrderResponseData.getOrder_number());
        amountVal.setText(getResources().getString(R.string.currency_symbol)+completedOrderResponseData.getAmount());
        restName.setText(completedOrderResponseData.getRestaurant_name());
        deliveryAddr.setText(completedOrderResponseData.getRestaurant_floor()+","+completedOrderResponseData.getRestaurant_street()+","+completedOrderResponseData.getRestaurant_area()+" "+completedOrderResponseData.getRestaurant_city());
        deliveryToName.setText(completedOrderResponseData.getFirst_name()+" "+(completedOrderResponseData.getLast_name() == null ? "" : completedOrderResponseData.getLast_name()));
        if (completedOrderResponseData.getShipJson() != null) {
            if (completedOrderResponseData.getShipJson().getAddressJson() != null) {
                floor = (completedOrderResponseData.getShipJson().getAddressJson().getFloorUnit() == null ? "" : completedOrderResponseData.getShipJson().getAddressJson().getFloorUnit() + ", ");
                houseNo = (completedOrderResponseData.getShipJson().getAddressJson().getHouseNo() == null ? "" : completedOrderResponseData.getShipJson().getAddressJson().getHouseNo() + ", ");
                street = (completedOrderResponseData.getShipJson().getAddressJson().getStreet() == null ? "" : completedOrderResponseData.getShipJson().getAddressJson().getStreet() + ", ");
                areaName = (completedOrderResponseData.getShipJson().getAddressJson().getAreaName() == null ? "" : completedOrderResponseData.getShipJson().getAddressJson().getAreaName() + ", ");
                city = (completedOrderResponseData.getShipJson().getAddressJson().getCity() == null ? "" : completedOrderResponseData.getShipJson().getAddressJson().getCity());
            }
        }
        receiverAddr.setText(floor + houseNo + street + areaName + city);


        pickUpLocation.setOnClickListener(this);
        deliveryLocation.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.pickUpLocation:
                openingMap(Double.parseDouble(completedOrderResponseData.getRestaurant_latitude()),Double.parseDouble(completedOrderResponseData.getRestaurant_longitude()));
                break;
            case R.id.deliveryLocation:
                String shippingLat =completedOrderResponseData.getShipJson().getLatitude().equalsIgnoreCase("") ? "0.0" :completedOrderResponseData.getShipJson().getLatitude();
                String shippingLong =completedOrderResponseData.getShipJson().getLongitude().equalsIgnoreCase("") ? "0.0" :completedOrderResponseData.getShipJson().getLongitude();

                openingMap(Double.parseDouble(shippingLat),Double.parseDouble(shippingLong));
                break;
            default:
                break;
        }
    }

    private void openingMap(double lati,double longi) {
        Log.i("lati",""+lati);
        Log.i("longi",""+longi);

        try {
            String url = "http://maps.google.com/maps?f=d&daddr=" + lati + "," + longi + "&dirflg=d&layer=t";
            Intent intent = new Intent(android.content.Intent.ACTION_VIEW, Uri.parse(url));
            intent.setClassName("com.google.android.apps.maps", "com.google.android.maps.MapsActivity");
            startActivity(intent);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }
}
