package com.taxiyefood.driver.fragment;


import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.PorterDuff;
import android.os.Build;
import android.os.Bundle;
import android.text.InputType;
import android.text.TextUtils;
import android.util.Log;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowInsets;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.taxiyefood.driver.R;
import com.taxiyefood.driver.adapter.CompletedOrderAdapter;
import com.taxiyefood.driver.data.AppController;
import com.taxiyefood.driver.firebasenotification.UpdateFirebaseToken;
import com.taxiyefood.driver.interfaces.CompletedOrderFragmentUpdation;
import com.taxiyefood.driver.serverrequest.CompletedOrderRequest;
import com.taxiyefood.driver.serverrequest.CompletedOrderResponse;
import com.taxiyefood.driver.serverrequest.CompletedOrderResponseData;
import com.taxiyefood.driver.ui.MainActivity;
import com.taxiyefood.driver.utils.Constant;
import com.taxiyefood.driver.utils.GsonRequest;
import com.taxiyefood.driver.utils.MyCustomProgressDialog;
import com.taxiyefood.driver.utils.NewSelectDateFragment;
import com.taxiyefood.driver.utils.Preference;
import com.taxiyefood.driver.utils.Utility;

import java.util.ArrayList;
import java.util.Calendar;


/**
 * Created by enuke on 19/5/16.
 */
public class CompleteOrderFragment extends Fragment implements View.OnClickListener {

    public static ArrayList<CompletedOrderResponseData> acceptedOrderBeen;
    TextView noData;
    DrawerLayout drawer;
    CompletedOrderFragmentUpdation completeOrderFragment;
    DatePickerDialog.OnDateSetListener ondate1 = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            // dateTo.setText((monthOfYear+1)+"/"+dayOfMonth+"/"+year);
            dateTo.setText(year + "-" + (monthOfYear + 1) + "-" + dayOfMonth);
        }
    };
    private RecyclerView myRecyclerView;
    private LinearLayoutManager linearLayoutManager;
    private CompletedOrderAdapter myRecyclerViewAdapter;
    private ProgressDialog pDialog;
    private EditText dateFrom, dateTo;
    DatePickerDialog.OnDateSetListener ondate2 = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            //     Utility.showToast(getActivity(),String.valueOf(year) + "-" + String.valueOf(monthOfYear)+ "-" + String.valueOf(dayOfMonth));
            //  dateFrom.setText((monthOfYear+1)+"/"+dayOfMonth+"/"+year);
            dateFrom.setText(year + "-" + (monthOfYear + 1) + "-" + dayOfMonth);
        }
    };
    private Button apply, cancel;

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Log.e("on complete", "activity created");
    }

    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
        Log.e("on complete", "view restored");
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        Log.e("on complete", "attach");
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        Log.e("on complete", "deview");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.e("on complete", "des");
    }

    @Override
    public void onDetach() {
        super.onDetach();
        Log.e("on complete", "detach");
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.e("on complete", "pause");
    }

    @Override
    public void onStop() {
        super.onStop();
        Log.e("on complete", "stop");
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        acceptedOrderBeen = new ArrayList<>();
        Log.e("on complete", "creatview");


        if (!(getActivity() instanceof CompletedOrderFragmentUpdation)) {
            throw new ClassCastException("Hosting activity must implement BackHandlerInterface");
        } else {
            completeOrderFragment = (CompletedOrderFragmentUpdation) getActivity();
        }
    }

    public void openingDrawer() {
        drawer.openDrawer(GravityCompat.END);
    }

    @Override
    public void onStart() {
        super.onStart();
        Log.e("on complete", "start");

        // Mark this fragment as the selected Fragment.
        completeOrderFragment.selectedFragment(this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.completed_order_fragment_main, container, false);
        settingLayout(view);
        updateFirebaseNotification();
        Log.e("on complete", "createview");

        if (acceptedOrderBeen.size() > 0) {
            Utility.showingLog("getActivity()", "no request will go");
            myRecyclerViewAdapter = new CompletedOrderAdapter(getActivity(), acceptedOrderBeen);
            myRecyclerView.setAdapter(myRecyclerViewAdapter);
            checkVisibility();
        } else {
            if (Utility.isInternetConnected(getActivity()))
                makeCompleteOrderRequest("5", Preference.getUserIdCommon());
            else
                Utility.showToast(getActivity(), getString(R.string.no_internet));
        }

        myRecyclerView.addOnItemTouchListener(new RecycleTouchEvent(getActivity(), myRecyclerView, new ClickListener() {
            @Override
            public void onClick(View v, int position) {
                // String orderId = acceptedOrderBeen.get(position).getId();
                CompletedOrderResponseData completedOrderResponseData = acceptedOrderBeen.get(position);
                replaceFragment(completedOrderResponseData);
            }

            @Override
            public void onClickLong(View v, int position) {
            }
        }));

        return view;
    }

    private void replaceFragment(CompletedOrderResponseData completedOrderResponseData) {

        CompletedOrderDetail completedOrderDetail = new CompletedOrderDetail();
        Bundle bundle = new Bundle();
        bundle.putSerializable("order_id", completedOrderResponseData);
        completedOrderDetail.setArguments(bundle);
        MainActivity.getInstance().activeDeactive.setVisibility(View.VISIBLE);
        MainActivity.getInstance().filtering.setVisibility(View.GONE);
        MainActivity.getInstance().title.setText(getString(R.string.completed_order_detail));
        getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.controllerFragment, completedOrderDetail).addToBackStack(null).commit();
    }

    /*private void settingUI() {
        *//*Utility.showingLog("current milli",""+System.currentTimeMillis());
        acceptedOrderBeen.add(new AcceptedOrderBean("Subway Japanese Resturant","Sector 18,Udyog Vihar Gurgaon","Mr. Raj Kapoor","Sector 18,Udyog Vihar Gurgaon","5 FEB 2016","232565462","Ready","$300","","","",""+30000));
        acceptedOrderBeen.add(new AcceptedOrderBean("Rajdhani Rajsthali Resturant","MG Road, Iffco Chowk Gurgaon","Mr. Shyam Taneja","MG Road, Iffco Chowk Gurgaon","3 FEB 2016","232243545","Ready","$500","","","",""+60000));//1463570598700*//*
    }*/
    private void makeCompleteOrderRequest(String status, String driver_id) {
        CompletedOrderRequest loginData = new CompletedOrderRequest(status, driver_id);
        Gson gson = new GsonBuilder().create();
        String loginJson = gson.toJson(loginData);
        makeServerRequest(loginJson);

    }

    private void makeCompleteOrderFilterRequest(String status, String driver_id, String date_from, String date_to) {
        CompletedOrderRequest loginData = new CompletedOrderRequest(status, driver_id, date_from, date_to);
        Gson gson = new GsonBuilder().create();
        String loginJson = gson.toJson(loginData);
        makeServerRequest(loginJson);

    }

    private void makeServerRequest(String loginJson) {
        pDialog = new MyCustomProgressDialog(getActivity(), R.style.MyTheme);
        // pDialog.setMessage("Please wait...");

        pDialog.setCancelable(false);
        pDialog.show();
        String url = Constant.COMPLETED_ORDERS;
        Utility.showingLog("url", "" + url + "request" + loginJson);
        GsonRequest gsonRequest = new GsonRequest<CompletedOrderResponse>(Request.Method.POST, url,
                CompletedOrderResponse.class, loginJson, new Response.Listener<CompletedOrderResponse>() {

            @Override
            public void onResponse(CompletedOrderResponse completedOrderResponse) {
                boolean status = completedOrderResponse.isSuccess();

                if (status) {
                    pDialog.dismiss();
                    ArrayList<CompletedOrderResponseData> completedOrderResponseDatas = completedOrderResponse.getData();
                    try {
                        if (completedOrderResponseDatas.size() > 0) {
                            acceptedOrderBeen.clear();
                            acceptedOrderBeen = completedOrderResponseDatas;
                            myRecyclerViewAdapter = new CompletedOrderAdapter(getActivity(), acceptedOrderBeen);
                            myRecyclerView.setAdapter(myRecyclerViewAdapter);
                            checkVisibility();
                        } else {
                            acceptedOrderBeen.clear();
                            myRecyclerViewAdapter.notifyDataSetChanged();
                            checkVisibility();

                            // Utility.showToast(getActivity(), completedOrderResponse.getError());

                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                } else {
                    Utility.showToast(getActivity(), completedOrderResponse.getError());
                    pDialog.dismiss();
                }

            }
        },

                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        pDialog.dismiss();
                        if (volleyError != null) {
                            if (volleyError instanceof NetworkError)
                                Utility.showToast(getActivity(), getResources().getString(R.string.slow_net));
                            else if (volleyError.getMessage() == null)
                                Utility.showToast(getActivity(), getResources().getString(R.string.network_error));
                            else
                                Utility.showToast(getActivity(), volleyError.getMessage());


                        }
                    }
                });
        DefaultRetryPolicy policy = new DefaultRetryPolicy(2 * 60 * 1000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        gsonRequest.setRetryPolicy(policy);
        AppController.getInstance().addToRequestQueue(gsonRequest);
    }

    private void settingLayout(View view) {
        noData = view.findViewById(R.id.no_data_there);

        dateFrom = view.findViewById(R.id.filterDateFrom);
        dateTo = view.findViewById(R.id.filterDateTo);


        apply = view.findViewById(R.id.filter_apply);
        cancel = view.findViewById(R.id.filter_reset);

        drawer = view.findViewById(R.id.drawer_layout_completed);

        dateFrom.setOnClickListener(this);
        dateTo.setOnClickListener(this);
        apply.setOnClickListener(this);
        cancel.setOnClickListener(this);

        dateFrom.setInputType(InputType.TYPE_NULL);
        dateFrom.setFocusableInTouchMode(false);

        dateTo.setInputType(InputType.TYPE_NULL);
        dateTo.setFocusableInTouchMode(false);

        dateFrom.getBackground().setColorFilter(getResources().getColor(R.color.colorPrimary), PorterDuff.Mode.SRC_ATOP);
        dateTo.getBackground().setColorFilter(getResources().getColor(R.color.colorPrimary), PorterDuff.Mode.SRC_ATOP);

        myRecyclerView = view.findViewById(R.id.completedOrderRecycler);
        myRecyclerView.setHasFixedSize(true);

        linearLayoutManager =
                new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);


        myRecyclerView.setLayoutManager(linearLayoutManager);

        NavigationView navigationView = view.findViewById(R.id.nav_view_completed);
        if (navigationView != null && Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT_WATCH) {
            navigationView.setOnApplyWindowInsetsListener(new View.OnApplyWindowInsetsListener() {
                @Override
                public WindowInsets onApplyWindowInsets(View v, WindowInsets insets) {
                    return insets;
                }
            });
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        Log.e("on complete", "resume");
        if (myRecyclerViewAdapter != null) {
            if (myRecyclerViewAdapter.getItemCount() > 0)
                noData.setVisibility(View.GONE);
            else
                noData.setVisibility(View.VISIBLE);
        }

        MainActivity.getInstance().activeDeactive.setVisibility(View.GONE);
        MainActivity.getInstance().filtering.setVisibility(View.VISIBLE);
        MainActivity.getInstance().title.setText(getString(R.string.completed_orders));
    }

    private boolean isValid() {
        if (TextUtils.isEmpty(dateFrom.getText())) {
            dateFrom.requestFocus();
            dateFrom.setError("Please select date");
            return false;
        } else
            dateFrom.setError(null);

        if (TextUtils.isEmpty(dateTo.getText())) {
            dateTo.requestFocus();
            dateTo.setError("Please select date");
            return false;
        } else
            dateTo.setError(null);

        return true;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.filterDateFrom:
                /*DialogFragment newFragment = new SelectDateFragment(dateFrom);
                newFragment.show(getFragmentManager(), "DatePicker");*/
                String dateSetting2 = dateFrom.getText().toString();
                if (dateSetting2.equalsIgnoreCase("")) {
                    NewSelectDateFragment newFragment2 = new NewSelectDateFragment();
                    Calendar calender = Calendar.getInstance();
                    Bundle args2 = new Bundle();
                    args2.putInt("year", calender.get(Calendar.YEAR));
                    args2.putInt("month", calender.get(Calendar.MONTH));
                    args2.putInt("day", calender.get(Calendar.DAY_OF_MONTH));
                    args2.putString("edit_text_value", dateSetting2);
                    newFragment2.setArguments(args2);

                    newFragment2.setCallBack2(ondate2);
                    newFragment2.show(getFragmentManager(), "Date Picker");
                } else {

                    NewSelectDateFragment newFragment2 = new NewSelectDateFragment();
                    Bundle args1 = new Bundle();
                    args1.putInt("year", Integer.parseInt(dateSetting2.split("-", 3)[0]));
                    args1.putInt("month", (Integer.parseInt(dateSetting2.split("-", 3)[1]) - 1));
                    args1.putInt("day", Integer.parseInt(dateSetting2.split("-", 3)[2]));
                    args1.putString("edit_text_value", dateSetting2);

                    newFragment2.setArguments(args1);

                    newFragment2.setCallBack2(ondate2);
                    newFragment2.show(getFragmentManager(), "Date Picker");
                }
                break;
            case R.id.filterDateTo:
                /*DialogFragment newFragment1 = new SelectDateFragment(dateTo);
                newFragment1.show(getFragmentManager(), "DatePicker");*/
                String dateSetting = dateTo.getText().toString();
                if (dateSetting.equalsIgnoreCase("")) {
                    NewSelectDateFragment newFragment2 = new NewSelectDateFragment();
                    Calendar calender = Calendar.getInstance();
                    Bundle args2 = new Bundle();
                    args2.putInt("year", calender.get(Calendar.YEAR));
                    args2.putInt("month", calender.get(Calendar.MONTH));
                    args2.putInt("day", calender.get(Calendar.DAY_OF_MONTH));
                    args2.putString("edit_text_value", dateSetting);
                    newFragment2.setArguments(args2);

                    newFragment2.setCallBack2(ondate1);
                    newFragment2.show(getFragmentManager(), "Date Picker");
                } else {

                    NewSelectDateFragment newFragment2 = new NewSelectDateFragment();
                    Bundle args2 = new Bundle();
                    args2.putInt("year", Integer.parseInt(dateSetting.split("-", 3)[0]));
                    args2.putInt("month", (Integer.parseInt(dateSetting.split("-", 3)[1]) - 1));
                    args2.putInt("day", Integer.parseInt(dateSetting.split("-", 3)[2]));
                    args2.putString("edit_text_value", dateSetting);

                    newFragment2.setArguments(args2);

                    newFragment2.setCallBack(ondate1);
                    newFragment2.show(getFragmentManager(), "Date Picker");
                }
                break;
            case R.id.filter_apply:
                if (Utility.isInternetConnected(getActivity())) {
                    if (isValid()) {
                        try {
                            String fromDateValue = dateFrom.getText().toString() + " 00:00:00";
                            String toDateValue = dateTo.getText().toString() + " 23:59:59";
                            Log.i("date from", "" + fromDateValue + "date to" + toDateValue);
                            cancel.setVisibility(View.VISIBLE);
                            makeCompleteOrderFilterRequest("5", Preference.getUserIdCommon(), dateFrom.getText().toString(), dateTo.getText().toString());
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        drawer.closeDrawer(GravityCompat.END);
                    }
                } else
                    Utility.showToast(getActivity(), getString(R.string.no_internet));
                break;
            case R.id.filter_reset:
                if (Utility.isInternetConnected(getActivity())) {
                    try {
                        dateFrom.setHint("Date from");
                        dateFrom.setText("");
                        dateTo.setText("");
                        dateTo.setHint("Date to");
                        cancel.setVisibility(View.GONE);

                        makeCompleteOrderRequest("5", Preference.getUserIdCommon());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    drawer.closeDrawer(GravityCompat.END);
                } else
                    Utility.showToast(getActivity(), getString(R.string.no_internet));
                break;
            default:
                break;
        }
    }

    public void checkVisibility() {
        if (myRecyclerViewAdapter != null) {
            if (myRecyclerViewAdapter.getItemCount() > 0)
                noData.setVisibility(View.GONE);
            else
                noData.setVisibility(View.VISIBLE);
        }
    }


    public interface ClickListener {
        void onClick(View v, int position);

        void onClickLong(View v, int position);

    }

    class RecycleTouchEvent implements RecyclerView.OnItemTouchListener {
        private GestureDetector gestureDetector;
        private ClickListener clk;

        public RecycleTouchEvent(Context context, final RecyclerView recyclerView, final ClickListener clicklistener) {
            this.clk = clicklistener;
            gestureDetector = new GestureDetector(context, new GestureDetector.SimpleOnGestureListener() {

                @Override
                public boolean onSingleTapUp(MotionEvent e) {
                    View child = recyclerView.findChildViewUnder(e.getX(), e.getY());
                    child.bringToFront();

                    clicklistener.onClick(child, recyclerView.getChildPosition(child));
                    //     clicklistener.onClicklong(child, recyclerView.getChildPosition(child));

                    return super.onSingleTapUp(e);


                }

                @Override
                public void onLongPress(MotionEvent e) {
                    View child = recyclerView.findChildViewUnder(e.getX(), e.getY());
                    child.bringToFront();

                    if (child != null && clicklistener != null) {
                        //   clicklistener.onClick(child, recyclerView.getChildPosition(child));
                        //                                                  clicklistener.onClickLong(child, recyclerView.getChildPosition(child));
                    }

                }
            });
        }

        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
            View child = rv.findChildViewUnder(e.getX(), e.getY());
            if (child != null && clk != null && gestureDetector.onTouchEvent(e)) {
                clk.onClick(child, rv.getChildPosition(child));
            }
            return false;
        }

        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e) {

        }

        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean b) {

        }
    }
    /**
     * update device token to server for firebase
     */
    private void updateFirebaseNotification() {
        if (Preference.getUserIdCommon() != null) {
            if (FirebaseInstanceId.getInstance().getToken() != null && Preference.getUserIdCommon() != null) {
                String firebaseToken = FirebaseInstanceId.getInstance().getToken();
                if (!Preference.getBooleanNotification(Constant.PREFERENCES_UPDATE_NOTIFICATION_STATUS)) {
                    UpdateFirebaseToken.update(getActivity(), firebaseToken);
                }
            }
        }
    }
}
