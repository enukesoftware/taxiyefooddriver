package com.taxiyefood.driver.serverrequest;


import com.google.gson.annotations.SerializedName;


/**
 * Created by enuke on 13/6/16.
 */
public class CompletedOrderRequest {


    @SerializedName("status")
    String status;

    @SerializedName("driver_id")
    String driver_id;

    @SerializedName("date_from")
    String date_from;

    @SerializedName("date_to")
    String date_to;

    public CompletedOrderRequest(String status, String driver_id) {
        this.status = status;
        this.driver_id = driver_id;
    }

    public CompletedOrderRequest(String status, String driver_id, String date_from, String date_to) {
        this.status = status;
        this.driver_id = driver_id;
        this.date_from = date_from;
        this.date_to = date_to;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDriver_id() {
        return driver_id;
    }

    public void setDriver_id(String driver_id) {
        this.driver_id = driver_id;
    }

    public String getDate_from() {
        return date_from;
    }

    public void setDate_from(String date_from) {
        this.date_from = date_from;
    }

    public String getDate_to() {
        return date_to;
    }

    public void setDate_to(String date_to) {
        this.date_to = date_to;
    }
}
