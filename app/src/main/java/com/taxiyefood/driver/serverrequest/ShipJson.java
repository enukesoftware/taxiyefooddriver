package com.taxiyefood.driver.serverrequest;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by amrit on 17/8/17.
 */

public class ShipJson implements Serializable{
    @SerializedName("address_json")
    @Expose
    private AddressJson addressJson;
    @SerializedName("latitude")
    @Expose
    private String latitude;
    @SerializedName("longitude")
    @Expose
    private String longitude;

    public ShipJson(AddressJson addressJson, String latitude, String longitude) {
        this.addressJson = addressJson;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public AddressJson getAddressJson() {
        return addressJson;
    }

    public void setAddressJson(AddressJson addressJson) {
        this.addressJson = addressJson;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }
}