package com.taxiyefood.driver.serverrequest;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by amrit on 17/8/17.
 */

public class AddressJson implements Serializable {

    @SerializedName("area_name")
    @Expose
    private String areaName;
    @SerializedName("city")
    @Expose
    private String city;
    @SerializedName("floor_unit")
    @Expose
    private String floorUnit;
    @SerializedName("house_no")
    @Expose
    private String houseNo;
    @SerializedName("street")
    @Expose
    private String street;


    public AddressJson(String areaName, String city, String floorUnit, String houseNo, String street) {
        this.areaName = areaName;
        this.city = city;
        this.floorUnit = floorUnit;
        this.houseNo = houseNo;
        this.street = street;
    }

    public String getAreaName() {
        return areaName;
    }

    public void setAreaName(String areaName) {
        this.areaName = areaName;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getFloorUnit() {
        return floorUnit;
    }

    public void setFloorUnit(String floorUnit) {
        this.floorUnit = floorUnit;
    }

    public String getHouseNo() {
        return houseNo;
    }

    public void setHouseNo(String houseNo) {
        this.houseNo = houseNo;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }
}
