package com.taxiyefood.driver.serverrequest;


import com.google.gson.annotations.SerializedName;


/**
 * Created by enuke on 31/5/16.
 */
public class AcceptedOrderResponse {

    @SerializedName("success")
    boolean success;

    @SerializedName("message")
    String message = null;

    @SerializedName("error")
    String error = null;

    public AcceptedOrderResponse(boolean success, String message, String error) {
        this.success = success;
        this.message = message;
        this.error = error;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }
}
