package com.taxiyefood.driver.serverrequest;


import com.google.gson.annotations.SerializedName;


/**
 * Created by enuke on 6/1/16.
 */
public class LoginResponseData {

    @SerializedName("success")
    boolean status;

    @SerializedName("message")
    String message;

    @SerializedName("data")
    LoginResponseDataValue loginResponseDataValue;

    @SerializedName("token")
    String token;

    public LoginResponseData(boolean status, String message, LoginResponseDataValue loginResponseDataValue, String token) {
        this.status = status;
        this.message = message;
        this.loginResponseDataValue = loginResponseDataValue;
        this.token = token;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }



    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public LoginResponseDataValue getLoginResponseDataValue() {
        return loginResponseDataValue;
    }

    public void setLoginResponseDataValue(LoginResponseDataValue loginResponseDataValue) {
        this.loginResponseDataValue = loginResponseDataValue;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
