package com.taxiyefood.driver.serverrequest;


import com.google.gson.annotations.SerializedName;


/**
 * Created by enuke on 30/5/16.
 */
public class DriverActivationResponse {

    @SerializedName("success")
    boolean success;

    @SerializedName("data")
    DriverActivationResponseData data;

    @SerializedName("message")
    String message;

    public DriverActivationResponse(boolean success, DriverActivationResponseData data, String message) {
        this.success = success;
        this.data = data;
        this.message = message;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public DriverActivationResponseData getData() {
        return data;
    }

    public void setData(DriverActivationResponseData data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
