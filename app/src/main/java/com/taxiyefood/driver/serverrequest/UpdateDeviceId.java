package com.taxiyefood.driver.serverrequest;


import com.google.gson.annotations.SerializedName;


/**
 * Created by enuke on 28/4/16.
 */
public class UpdateDeviceId {

    @SerializedName("update")
    UpdateDeviceData updateDeviceData;

    public UpdateDeviceId(UpdateDeviceData updateDeviceData) {
        this.updateDeviceData = updateDeviceData;
    }

    public UpdateDeviceData getUpdateDeviceData() {
        return updateDeviceData;
    }

    public void setUpdateDeviceData(UpdateDeviceData updateDeviceData) {
        this.updateDeviceData = updateDeviceData;
    }
}

