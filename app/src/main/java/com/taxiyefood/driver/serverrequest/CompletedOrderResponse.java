package com.taxiyefood.driver.serverrequest;


import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;


/**
 * Created by enuke on 31/5/16.
 */
public class CompletedOrderResponse {

    @SerializedName("success")
    boolean success;

    @SerializedName("data")
    ArrayList<CompletedOrderResponseData> data = null;

    @SerializedName("error")
    String error = null;

    public CompletedOrderResponse(boolean success, ArrayList<CompletedOrderResponseData> data, String error) {
        this.success = success;
        this.data = data;
        this.error = error;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public ArrayList<CompletedOrderResponseData> getData() {
        return data;
    }

    public void setData(ArrayList<CompletedOrderResponseData> data) {
        this.data = data;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }
}
