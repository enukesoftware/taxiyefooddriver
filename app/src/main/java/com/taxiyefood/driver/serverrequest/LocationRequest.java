package com.taxiyefood.driver.serverrequest;


import com.google.gson.annotations.Expose;


/**
 * Created by enuke on 4/3/16.
 */
public class LocationRequest {

    @Expose

    LocationData location_update;

    public LocationRequest(LocationData location_update) {
        this.location_update = location_update;
    }

    public LocationData getLocation_update() {
        return location_update;
    }

    public void setLocation_update(LocationData location_update) {
        this.location_update = location_update;
    }
}
