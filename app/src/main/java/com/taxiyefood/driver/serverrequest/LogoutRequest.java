package com.taxiyefood.driver.serverrequest;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by enuke on 9/5/18.
 */
public class LogoutRequest {
    @Expose
    @SerializedName("driver_id")
    String driverId;

    public LogoutRequest(String driverId) {
        this.driverId = driverId;
    }

    public String getDriverId() {
        return driverId;
    }

    public void setDriverId(String driverId) {
        this.driverId = driverId;
    }
}
