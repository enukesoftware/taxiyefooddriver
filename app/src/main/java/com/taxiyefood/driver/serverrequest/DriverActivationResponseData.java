package com.taxiyefood.driver.serverrequest;


import com.google.gson.annotations.SerializedName;


/**
 * Created by enuke on 30/5/16.
 */
public class DriverActivationResponseData {

    @SerializedName("id")
    String id;

    @SerializedName("role_id")
    String role_id;


    @SerializedName("first_name")
    String first_name;


    @SerializedName("last_name")
    String last_name;

    @SerializedName("status")
    String status;

    public DriverActivationResponseData(String id, String role_id, String first_name, String last_name, String status) {
        this.id = id;
        this.role_id = role_id;
        this.first_name = first_name;
        this.last_name = last_name;
        this.status = status;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getRole_id() {
        return role_id;
    }

    public void setRole_id(String role_id) {
        this.role_id = role_id;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
