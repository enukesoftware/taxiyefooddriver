package com.taxiyefood.driver.serverrequest;


import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;


/**
 * Created by enuke on 4/3/16.
 */
public class LocationResponse {

    @SerializedName("success")
    boolean success;

    @SerializedName("data")
    ArrayList<LocationResponseData> data;

    public LocationResponse(boolean success, ArrayList<LocationResponseData> data) {
        this.success = success;
        this.data = data;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public ArrayList<LocationResponseData> getData() {
        return data;
    }

    public void setData(ArrayList<LocationResponseData> data) {
        this.data = data;
    }
}
