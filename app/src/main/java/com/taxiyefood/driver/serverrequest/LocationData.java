package com.taxiyefood.driver.serverrequest;


import com.google.gson.annotations.SerializedName;


/**
 * Created by enuke on 6/1/16.
 */
public class LocationData {

    @SerializedName("driver_id")
    String driver_id;

    @SerializedName("lat")
    String lati;

    @SerializedName("long")
    String longi;

    @SerializedName("status")
    String Status;

    public LocationData(String driver_id, String lati, String longi, String status) {
        this.driver_id = driver_id;
        this.lati = lati;
        this.longi = longi;
        Status = status;
    }

    public String getDriver_id() {
        return driver_id;
    }

    public void setDriver_id(String driver_id) {
        this.driver_id = driver_id;
    }

    public String getLati() {
        return lati;
    }

    public void setLati(String lati) {
        this.lati = lati;
    }

    public String getLongi() {
        return longi;
    }

    public void setLongi(String longi) {
        this.longi = longi;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }
}