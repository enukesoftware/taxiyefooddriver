package com.taxiyefood.driver.serverrequest;


import com.google.gson.annotations.Expose;


/**
 * Created by enuke on 6/1/16.
 */
public class LoginData {

    @Expose
    String email;

    @Expose
    String password;

    public LoginData(String email, String password) {
        this.email = email;
        this.password = password;
    }

    public String getEmailAddress() {
        return email;
    }

    public void setEmailAddress(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
