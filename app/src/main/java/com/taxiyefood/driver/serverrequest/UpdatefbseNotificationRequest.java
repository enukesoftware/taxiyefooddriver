package com.taxiyefood.driver.serverrequest;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by enuke on 9/5/18.
 */
public class UpdatefbseNotificationRequest {
    @Expose
    @SerializedName("id")
    String userId;
    @Expose
    @SerializedName("device_token")
    String deviceToken;

    public UpdatefbseNotificationRequest() {
    }

    public UpdatefbseNotificationRequest(String userId, String deviceToken) {
        this.userId = userId;
        this.deviceToken = deviceToken;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getDeviceToken() {
        return deviceToken;
    }

    public void setDeviceToken(String deviceToken) {
        this.deviceToken = deviceToken;
    }
}
