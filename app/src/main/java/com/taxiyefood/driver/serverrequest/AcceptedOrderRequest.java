package com.taxiyefood.driver.serverrequest;


import com.google.gson.annotations.SerializedName;


/**
 * Created by enuke on 31/5/16.
 */
public class AcceptedOrderRequest {

    @SerializedName("driver_id")
    String driver_id;

    @SerializedName("order_id")
    String order_id;

    @SerializedName("status")
    String status;

    @SerializedName("remark")
    String remark;

    public AcceptedOrderRequest(String driver_id, String order_id, String status, String remark) {
        this.driver_id = driver_id;
        this.order_id = order_id;
        this.status = status;
        this.remark = remark;
    }

    public String getDriver_id() {
        return driver_id;
    }

    public void setDriver_id(String driver_id) {
        this.driver_id = driver_id;
    }

    public String getOrder_id() {
        return order_id;
    }

    public void setOrder_id(String order_id) {
        this.order_id = order_id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }
}
