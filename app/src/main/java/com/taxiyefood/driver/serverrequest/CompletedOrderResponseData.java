package com.taxiyefood.driver.serverrequest;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;


/**
 * Created by enuke on 6/1/16.
 */
public class CompletedOrderResponseData implements Serializable{

    @SerializedName("id")
    String id;

    @SerializedName("date")
    String date;

    @SerializedName("time")
    String time;

    @SerializedName("asap")
    String asap;

    @SerializedName("amount")
    String amount;

    @SerializedName("discount")
    String discount;

    @SerializedName("order_type")
    String order_type;

    @SerializedName("shipping_charge")
    String shipping_charge;

    @SerializedName("order_number")
    String order_number;

    @SerializedName("status")
    String status;

    @SerializedName("remark")
    String remark;

    @SerializedName("created_at")
    String created_at;

    @SerializedName("ship_add1")
    String ship_add1;

    @SerializedName("ship_add2")
    String ship_add2;

    @SerializedName("ship_city")
    String ship_city;

    @SerializedName("ship_zip")
    String ship_zip;

    @SerializedName("ship_mobile")
    String ship_mobile;

    @SerializedName("ship_lat")
    String ship_lat;

    @SerializedName("ship_long")
    String ship_long;

    @SerializedName("first_name")
    String first_name;

    @SerializedName("last_name")
    String last_name;

    @SerializedName("coupon_code")
    String coupon_code;

    @SerializedName("coupon_value")
    String coupon_value;

    @SerializedName("restaurant_name")
    String restaurant_name;

    @SerializedName("restaurant_longitude")
    String restaurant_longitude;

    @SerializedName("restaurant_latitude")
    String restaurant_latitude;

    @SerializedName("restaurant_rating")
    String restaurant_rating;

    @SerializedName("restaurant_floor")
    String restaurant_floor;

    @SerializedName("restaurant_street")
    String restaurant_street;

    @SerializedName("restaurant_company")
    String restaurant_company;

    @SerializedName("restaurant_area")
    String restaurant_area;

    @SerializedName("restaurant_city")
    String restaurant_city;

    @SerializedName("ship_json")
    @Expose
    private ShipJson shipJson;

    public CompletedOrderResponseData(String id, String date, String time, String asap, String amount, String discount, String order_type, String shipping_charge, String order_number, String status, String remark, String created_at, String ship_add1, String ship_add2, String ship_city, String ship_zip, String ship_mobile, String ship_lat, String ship_long, String first_name, String last_name, String coupon_code, String coupon_value, String restaurant_name, String restaurant_longitude, String restaurant_latitude, String restaurant_rating, String restaurant_floor, String restaurant_street, String restaurant_company, String restaurant_area, String restaurant_city, ShipJson shipJson) {
        this.id = id;
        this.date = date;
        this.time = time;
        this.asap = asap;
        this.amount = amount;
        this.discount = discount;
        this.order_type = order_type;
        this.shipping_charge = shipping_charge;
        this.order_number = order_number;
        this.status = status;
        this.remark = remark;
        this.created_at = created_at;
        this.ship_add1 = ship_add1;
        this.ship_add2 = ship_add2;
        this.ship_city = ship_city;
        this.ship_zip = ship_zip;
        this.ship_mobile = ship_mobile;
        this.ship_lat = ship_lat;
        this.ship_long = ship_long;
        this.first_name = first_name;
        this.last_name = last_name;
        this.coupon_code = coupon_code;
        this.coupon_value = coupon_value;
        this.restaurant_name = restaurant_name;
        this.restaurant_longitude = restaurant_longitude;
        this.restaurant_latitude = restaurant_latitude;
        this.restaurant_rating = restaurant_rating;
        this.restaurant_floor = restaurant_floor;
        this.restaurant_street = restaurant_street;
        this.restaurant_company = restaurant_company;
        this.restaurant_area = restaurant_area;
        this.restaurant_city = restaurant_city;
        this.shipJson = shipJson;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getAsap() {
        return asap;
    }

    public void setAsap(String asap) {
        this.asap = asap;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public String getOrder_type() {
        return order_type;
    }

    public void setOrder_type(String order_type) {
        this.order_type = order_type;
    }

    public String getShipping_charge() {
        return shipping_charge;
    }

    public void setShipping_charge(String shipping_charge) {
        this.shipping_charge = shipping_charge;
    }

    public String getOrder_number() {
        return order_number;
    }

    public void setOrder_number(String order_number) {
        this.order_number = order_number;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getShip_add1() {
        return ship_add1;
    }

    public void setShip_add1(String ship_add1) {
        this.ship_add1 = ship_add1;
    }

    public String getShip_add2() {
        return ship_add2;
    }

    public void setShip_add2(String ship_add2) {
        this.ship_add2 = ship_add2;
    }

    public String getShip_city() {
        return ship_city;
    }

    public void setShip_city(String ship_city) {
        this.ship_city = ship_city;
    }

    public String getShip_zip() {
        return ship_zip;
    }

    public void setShip_zip(String ship_zip) {
        this.ship_zip = ship_zip;
    }

    public String getShip_mobile() {
        return ship_mobile;
    }

    public void setShip_mobile(String ship_mobile) {
        this.ship_mobile = ship_mobile;
    }

    public String getShip_lat() {
        return ship_lat;
    }

    public void setShip_lat(String ship_lat) {
        this.ship_lat = ship_lat;
    }

    public String getShip_long() {
        return ship_long;
    }

    public void setShip_long(String ship_long) {
        this.ship_long = ship_long;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getCoupon_code() {
        return coupon_code;
    }

    public void setCoupon_code(String coupon_code) {
        this.coupon_code = coupon_code;
    }

    public String getCoupon_value() {
        return coupon_value;
    }

    public void setCoupon_value(String coupon_value) {
        this.coupon_value = coupon_value;
    }

    public String getRestaurant_name() {
        return restaurant_name;
    }

    public void setRestaurant_name(String restaurant_name) {
        this.restaurant_name = restaurant_name;
    }

    public String getRestaurant_longitude() {
        return restaurant_longitude;
    }

    public void setRestaurant_longitude(String restaurant_longitude) {
        this.restaurant_longitude = restaurant_longitude;
    }

    public String getRestaurant_latitude() {
        return restaurant_latitude;
    }

    public void setRestaurant_latitude(String restaurant_latitude) {
        this.restaurant_latitude = restaurant_latitude;
    }

    public String getRestaurant_rating() {
        return restaurant_rating;
    }

    public void setRestaurant_rating(String restaurant_rating) {
        this.restaurant_rating = restaurant_rating;
    }

    public String getRestaurant_floor() {
        return restaurant_floor;
    }

    public void setRestaurant_floor(String restaurant_floor) {
        this.restaurant_floor = restaurant_floor;
    }

    public String getRestaurant_street() {
        return restaurant_street;
    }

    public void setRestaurant_street(String restaurant_street) {
        this.restaurant_street = restaurant_street;
    }

    public String getRestaurant_company() {
        return restaurant_company;
    }

    public void setRestaurant_company(String restaurant_company) {
        this.restaurant_company = restaurant_company;
    }

    public String getRestaurant_area() {
        return restaurant_area;
    }

    public void setRestaurant_area(String restaurant_area) {
        this.restaurant_area = restaurant_area;
    }

    public String getRestaurant_city() {
        return restaurant_city;
    }

    public void setRestaurant_city(String restaurant_city) {
        this.restaurant_city = restaurant_city;
    }

    public ShipJson getShipJson() {
        return shipJson;
    }

    public void setShipJson(ShipJson shipJson) {
        this.shipJson = shipJson;
    }
}
