package com.taxiyefood.driver.serverrequest;


import com.google.gson.annotations.SerializedName;


/**
 * Created by enuke on 30/5/16.
 */
public class DriverActivationRequest {

    @SerializedName("driver_id")
    String driver_id;

    @SerializedName("status")
    String status;

    public DriverActivationRequest(String driver_id, String status) {
        this.driver_id = driver_id;
        this.status = status;
    }

    public String getDriver_id() {
        return driver_id;
    }

    public void setDriver_id(String driver_id) {
        this.driver_id = driver_id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
