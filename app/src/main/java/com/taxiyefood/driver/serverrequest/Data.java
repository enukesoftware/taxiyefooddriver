
package com.taxiyefood.driver.serverrequest;

import com.google.gson.annotations.SerializedName;

public class Data {

    @SerializedName("contact_number")
    private String mContactNumber;
    @SerializedName("countrycode")
    private String mCountrycode;
    @SerializedName("created_at")
    private String mCreatedAt;
    @SerializedName("deleted_at")
    private String mDeletedAt;
    @SerializedName("device_token")
    private String mDeviceToken;
    @SerializedName("email")
    private String mEmail;
    @SerializedName("first_name")
    private String mFirstName;
    @SerializedName("id")
    private String mId;
    @SerializedName("last_lat")
    private Object mLastLat;
    @SerializedName("last_long")
    private Object mLastLong;
    @SerializedName("last_name")
    private String mLastName;
    @SerializedName("login_type")
    private String mLoginType;
    @SerializedName("newsletter")
    private String mNewsletter;
    @SerializedName("profile_image")
    private String mProfileImage;
    @SerializedName("role_id")
    private String mRoleId;
    @SerializedName("status")
    private String mStatus;
    @SerializedName("token")
    private String mToken;
    @SerializedName("updated_at")
    private String mUpdatedAt;
    @SerializedName("verified")
    private Object mVerified;

    public String getContactNumber() {
        return mContactNumber;
    }

    public void setContactNumber(String contactNumber) {
        mContactNumber = contactNumber;
    }

    public String getCountrycode() {
        return mCountrycode;
    }

    public void setCountrycode(String countrycode) {
        mCountrycode = countrycode;
    }

    public String getCreatedAt() {
        return mCreatedAt;
    }

    public void setCreatedAt(String createdAt) {
        mCreatedAt = createdAt;
    }

    public String getDeletedAt() {
        return mDeletedAt;
    }

    public void setDeletedAt(String deletedAt) {
        mDeletedAt = deletedAt;
    }

    public String getDeviceToken() {
        return mDeviceToken;
    }

    public void setDeviceToken(String deviceToken) {
        mDeviceToken = deviceToken;
    }

    public String getEmail() {
        return mEmail;
    }

    public void setEmail(String email) {
        mEmail = email;
    }

    public String getFirstName() {
        return mFirstName;
    }

    public void setFirstName(String firstName) {
        mFirstName = firstName;
    }

    public String getId() {
        return mId;
    }

    public void setId(String id) {
        mId = id;
    }

    public Object getLastLat() {
        return mLastLat;
    }

    public void setLastLat(Object lastLat) {
        mLastLat = lastLat;
    }

    public Object getLastLong() {
        return mLastLong;
    }

    public void setLastLong(Object lastLong) {
        mLastLong = lastLong;
    }

    public String getLastName() {
        return mLastName;
    }

    public void setLastName(String lastName) {
        mLastName = lastName;
    }

    public String getLoginType() {
        return mLoginType;
    }

    public void setLoginType(String loginType) {
        mLoginType = loginType;
    }

    public String getNewsletter() {
        return mNewsletter;
    }

    public void setNewsletter(String newsletter) {
        mNewsletter = newsletter;
    }

    public String getProfileImage() {
        return mProfileImage;
    }

    public void setProfileImage(String profileImage) {
        mProfileImage = profileImage;
    }

    public String getRoleId() {
        return mRoleId;
    }

    public void setRoleId(String roleId) {
        mRoleId = roleId;
    }

    public String getStatus() {
        return mStatus;
    }

    public void setStatus(String status) {
        mStatus = status;
    }

    public String getToken() {
        return mToken;
    }

    public void setToken(String token) {
        mToken = token;
    }

    public String getUpdatedAt() {
        return mUpdatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        mUpdatedAt = updatedAt;
    }

    public Object getVerified() {
        return mVerified;
    }

    public void setVerified(Object verified) {
        mVerified = verified;
    }

}
